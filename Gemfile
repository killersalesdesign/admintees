source 'http://rubygems.org'

# App
gem 'rails', '3.2.13'
gem 'mysql2'
gem 'figaro'
gem 'devise', '2.2.4'
gem 'rabl'
gem 'ledermann-rails-settings', :require => 'rails-settings'
gem 'paperclip', '~> 3.4.2'
gem 'aws-sdk'
gem 'fog'
gem 'friendly_id'
gem 'groupdate'
gem 'mail_form'
gem 'net-ssh', '~> 2.9.1'
gem 'kaminari'
gem 'meta_search'
gem 'rails_admin'
gem 'json'
gem 'sendgrid-api'
gem 'algoliasearch-rails'
# gem 'will_paginate', '~> 3.0.6'
gem 'capistrano-slack', :git => 'https://github.com/j-mcnally/capistrano-slack.git'
gem 'awesome_print'

# CSV
gem 'roo'

# PDF's
gem 'pdfkit', '0.6.2'
gem 'wkhtmltopdf-binary'


# Worker
gem 'sidekiq'
gem 'sidetiq'
gem 'sinatra', :require => nil

# Google
gem 'faraday', '~> 0.9.0'
gem 'google-api-client', '~> 0.7'

# Assets
gem 'gon'
gem 'eco'
gem 'haml-rails'
gem 'jquery-rails'
gem 'js-routes'
gem 'toastr-rails'
gem 'angularjs-rails'
gem 'angular-ui-rails'
gem 'ng-rails-csrf'

# Cron Jobs
gem 'whenever', :require => false

# Server
gem 'unicorn'
gem "airbrake"

gem 'barby'
gem 'chunky_png'

# used by elasticsearch
gem 'searchkick'

# Payment gateway
# gem 'activemerchant', :git => 'git@github.com:arcknine/active_merchant.git'
gem 'activemerchant'
gem 'active_utils'

group :assets do
  gem 'coffee-rails', '~> 3.2.1'
  gem 'therubyracer', :platforms => :ruby
  gem 'uglifier', '>= 1.0.3'
  gem 'haml_coffee_assets'
  gem 'execjs'
  gem 'asset_sync'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'thin'
  gem "letter_opener"

  gem 'capistrano', '~> 2.15.5'
  gem 'capistrano_colors'
  gem 'capistrano-ext'
  gem 'rvm-capistrano', :require => false
  gem 'capistrano-rbenv', '1.0.1'
  gem 'capistrano-sidekiq', '~> 0.3.0'
end

group :production do
  gem 'newrelic_rpm'
end