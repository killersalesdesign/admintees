class ProductsEmailWorker
  include Sidekiq::Worker
  def perform(user_id, email_id, product_id, order_id)
    email_obj       = Email.find(email_id)
    user            = User.find(user_id)
    emails_received = User.find(user_id).settings(:options).emails_received

    exist = false
    new_emails_received = []
    emails_received.each do |item|
      if item[:product].to_i == product_id
        exist = true
        item[:emails].push email_id
      end
      new_emails_received.push item
    end

    unless exist
      new_mail_entry = {product: product_id, emails: [email_id]}
      new_emails_received.push new_mail_entry
    end

    # emails_received.push email_id
    # user.settings(:options).update_attributes! :emails_received => emails_received
    user.settings(:options).update_attributes! :emails_received => new_emails_received

    product = Product.find(product_id)
    from    = product.website_id == 1 ? "FanTees Family <support@fantees.com.au>" : "support@jobtees.com.au"
    subject = email_obj.subject

    case email_obj.email_type
    when "packing"
      UserMailer.product_email_packing(user, from, subject).deliver
    when "printing"
      UserMailer.product_email_printing(user, from, subject).deliver
    when "tracking"
      UserMailer.product_email_tracking(user, from, subject).deliver
    when "arrived"
      UserMailer.product_email_arrived(user, from, subject, product.title).deliver
    when "campaign_ended"
      UserMailer.product_email_campaign_ended(user, from, subject, product.title).deliver
    when "howd_we_do"
      UserMailer.product_email_how_we_do(user, from, subject, product.title).deliver
    when "new_designs"
      UserMailer.product_email_new_design(user, from, subject, product.title).deliver
    end

    SentEmail.create order_id: order_id, email_id: email_id
  end
end
