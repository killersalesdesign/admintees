class AlgoliaIndexProductWorker
  include Sidekiq::Worker
  include AlgoliaSearch

  def perform
    # p = Product.find(id)
    # remove ? p.remove_from_index! : p.index!
    Time.zone = "Brisbane"
    products = Product.where(:show => true).where("active = ? AND (date >= ? OR status != ?)", true, Time.zone.now.beginning_of_day, 0)
    products.each do |p|
      p.index!
      sleep 0.5
    end
  end
end