class DailyBatchWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(23).minute_of_hour(59) }
  # recurrence { hourly.minute_of_hour(0, 15, 30, 45) }

  def perform(paramDate = nil)
    Time.zone = "Brisbane"

    begin
      paramDate = paramDate.to_date
    rescue Exception => e
      paramDate = Time.zone.now
    end

    results   = OrderedProduct.where("created_at >= ? AND created_at <= ?", paramDate.to_date.beginning_of_day, paramDate.to_date.end_of_day)
    if results
      # create batch here
      last_batch    = Batch.last
      new_batch_key = 1
      if last_batch
        new_batch_key = last_batch.batch_key + 1

        # check if batch key exist
        loop do
          res = Batch.where(:batch_key => new_batch_key)
          if res.blank?
            break
          else
            new_batch_key += 1
          end
        end
      end

      results.group_by(&:product_id).each do |pid, op|
        if pid
          the_product = op.first.product
          if the_product.status == 1    # status 1 - PRINTING
            stats       = get_stats(results, pid)
            brand       = the_product.get_brand
            color       = the_product.get_item_color
            Batch.create batch_key: new_batch_key, product_id: pid, batch_stats: stats, brand: brand, colour: color, product_name: op.first.title, is_auto: true

            Note.create description:"Batch Key: #{new_batch_key} created automatically", order_id: op.first.order.id, note_type: 1
          end
        end
      end

      puts 'batch created: '
      puts new_batch_key
    end
  end

  def get_stats(records, pid)
    stats = []
    products = records.where(:product_id => pid)
    this_product = products.first.product
    this_product.settings(:options).prices.each do |p|
      p[:total_sold]  = products.where(:category_id => p[:category_id]).sum(:quantity)
      category        = Category.find(p[:category_id].to_i)
      all_sizes       = category.settings(:options).sizes
      price           = this_product.price_and_profit_of_style(p[:category_id].to_i)
      product_sizes   = []
      all_sizes.each do |s|
        option          = {:name => s, :sold => products.where(:size => s, :category_id => p[:category_id]).sum(:quantity)}
        option[:extra]  = 0
        price[:sizes].each { |p| option[:extra] = p[:extra].nil? ? 0 : p[:extra] if s == p[:name] } unless price[:sizes].blank?
        product_sizes << option
      end
      p[:sizes] = product_sizes
      stats << p
    end
    stats.to_json
  end
end