module ApplicationHelper
  def socail_smart_layer
    js = <<-eos
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5320264f0ac93ecd"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent',
    'share' : {
      'position' : 'left',
      'numPreferredServices' : 5
    }
  });
</script>
<!-- AddThis Smart Layers END -->
    eos
    js.html_safe
  end

  def recommend4u_js
    js = <<-eos
<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/531fae09073bf7de80000006.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();

</script>
    eos
    js.html_safe
  end

  def olark_js
    js = <<-eos
<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7964-580-10-9213');/*]] > */</script><noscript><a href="https://www.olark.com/site/7964-580-10-9213/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
    eos
    js.html_safe
  end

  def fb_conversion_js
    js = <<-eos
<!-- Facebook Conversion Code for JobTeesAU Sale -->
<script>
  var fb_param = {};
  fb_param.pixel_id = '6013441219299';
  fb_param.value = '0.00';
  fb_param.currency = 'USD';
  (function(){
    var fpw = document.createElement('script');
    fpw.async = true;
    fpw.src = '//connect.facebook.net/en_US/fp.js';
    var ref = document.getElementsByTagName('script')[0];
    ref.parentNode.insertBefore(fpw, ref);
  })();
</script>
<noscript>
  <img alt='' height='1' src='https://www.facebook.com/offsite_event.php?id=6013441219299&amp;amp;value=0&amp;amp;currency=USD' style='display:none' width='1'>
</noscript>
<!-- end Facebook Conversion Code for JobTeesAU Sale -->
    eos
    js.html_safe
  end

  def ga_js
    js = <<-eos
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48898816-1', 'jobtees.com.au');
  ga('send', 'pageview');
  ga('require', 'ecommerce', 'ecommerce.js');
</script>
    eos
    js.html_safe
  end

  def ge_ecommerce_js(order)
    items   = order.ordered_products
    product = order.product
    js = []
    items.each do |i|
      js << "ga('ecommerce:addItem', {
    'id': '#{order.order_id}',
    'name': '#{product.title}',
    'sku': '#{product.id}',
    'category': '#{Category.find(i.category_id).name}',
    'price': '#{product.price_and_profit_of_style(i.category_id)[:amount]}',
    'quantity': '#{i.quantity}'
  });"
    end
    js = <<-eos
<script>
  #{js.join("\n")}
  ga('ecommerce:send');
</script>
    eos
    js.html_safe
  end

  def perfectaudience_js
    js = <<-eos
<script>
  (function() {
    window._pa = window._pa || {};
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5354c7e0150899988d000027.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
    eos
    js.html_safe
  end

  def fb_share(page_url)
    uniq = "#.#{SecureRandom.hex[0..7]}.facebook"
    url  = "https://www.facebook.com/login.php?next=https://www.facebook.com/sharer/sharer.php?u=#{page_url}&display=popup"
    return URI::escape(url)
  end

  def twitter_share(page_url)
    url  = "https://twitter.com/intent/tweet?text=Jobtees&url=#{page_url}&related="
    return URI::escape(url)
  end

  def gplus_share(page_url)
    url = "https://accounts.google.com/ServiceLogin?service=oz&passive=1209600&continue=https://plus.google.com/share?url=#{page_url}&t=Jobtees&gpsrc=frameless&btmpl=popup"
    return URI::escape(url)
  end

  def pinterest_share(product)
    # http://www.pinterest.com/pin/create/button/
    #     ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
    #     &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
    #     &description=Next%20stop%3A%20Pinterest
    page_url = "#{root_url}#{product.slug}"
    img      = product.product_images.first.product_image(:medium)
    img_url  = "#{root_url}#{img[1..-1]}"
    url      = "http://www.pinterest.com/pin/create/button/?url=#{page_url}&media=#{img_url}&description=#{product.title}"
    return URI::escape(url)
  end
end
