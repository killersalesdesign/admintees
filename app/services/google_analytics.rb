class GoogleAnalytics
  def initialize(params)
    @startDate = params[:start_date]
    @endDate = params[:end_date]

    @client = Google::APIClient.new(
      :application_name => 'AdminTees',
      :application_version => '1.0.0'
    )

    # analytics = client.discovered_api('analytics', 'v3')
    # puts File.read('./client_secrets.json')
    # client_secrets = Google::APIClient::ClientSecrets.load('./client_secrets.json')

    @client.authorization = Signet::OAuth2::Client.new(
    :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
    :audience => 'https://accounts.google.com/o/oauth2/token',
    :scope => 'https://www.googleapis.com/auth/analytics.readonly',
    :issuer => GA_ISSUER,
    :signing_key => GA_API_KEY)
    @client.authorization.fetch_access_token!
    
    @client
  end

  def product_views(id)
    product = Product.find id
    puts product.slug
    items = []
    hours = ['12AM', '1AM', '2AM', '3AM', '4AM', '5AM', '6AM', '7AM', '8AM', '9AM', '10AM', '11AM', '12PM', '1PM', '2PM', '3PM', '4PM', '5PM', '6PM', '7PM', '8PM', '9PM', '10PM', '11PM']
    analytics = @client.discovered_api('analytics', 'v3')
    if File.exists? GA_CACHED_API_FILE
      File.open(GA_CACHED_API_FILE) do |file|
        analytics = Marshal.load(file)
      end
    else
      analytics = @client.discovered_api('analytics', GA_API_VERSION)
      File.open(GA_CACHED_API_FILE, 'w') do |file|
        Marshal.dump(analytics, file)
      end
    end
    viewsToday = get_hourly_stats(product, analytics, @startDate, @endDate)

    viewsToday.data.rows.each do |y|
      items.push y[1]
    end
    return {datasets: [data: items], labels: hours}
  end

  def product_campaigns(id)
    product = Product.find id
    puts product.slug
    items = []
    analytics = @client.discovered_api('analytics', 'v3')
    if File.exists? GA_CACHED_API_FILE
      File.open(GA_CACHED_API_FILE) do |file|
        analytics = Marshal.load(file)
      end
    else
      analytics = @client.discovered_api('analytics', GA_API_VERSION)
      File.open(GA_CACHED_API_FILE, 'w') do |file|
        Marshal.dump(analytics, file)
      end
    end
    campaignsToday = get_campaign_stats(product, analytics, @startDate, @endDate)

    campaignsToday.data.rows.each do |y|
      items.push ({
        campaign: y[0],
        sessions: y[1],
        bounces: y[2],
        uniquePageviews: y[3],
        pageviewsPerSession: number_to_percentage(y[4], precision: 2),
        avgTimeOnPage: y[5],
        transactions: y[6],
        transactionRevenue: y[7],
        revenuePerTransaction: number_to_percentage(y[8], precision: 2),
        newUsers: y[9]
      })
    end
    items
  end

  def product_referrals(id)
    product = Product.find id
    items = []
    analytics = @client.discovered_api('analytics', 'v3')
    if File.exists? GA_CACHED_API_FILE
      File.open(GA_CACHED_API_FILE) do |file|
        analytics = Marshal.load(file)
      end
    else
      analytics = @client.discovered_api('analytics', GA_API_VERSION)
      File.open(GA_CACHED_API_FILE, 'w') do |file|
        Marshal.dump(analytics, file)
      end
    end
    visitCount = get_visitor_stats(product, analytics, product.created_at.strftime("%Y-%m-%d"), @endDate)
    statsToday = get_visitor_stats(product, analytics, DateTime.now.yesterday.strftime("%Y-%m-%d"), DateTime.now.strftime("%Y-%m-%d"))
    c_views, c_total_orders, c_orders_count, c_conversion_rate, c_views_today, c_orders_today, c_orders_count_today, c_conversion_rate_today, c_total_count = 0,0,0,0,0,0,0,0,1

    statsToday.data.rows.each do |x|
      visitCount.data.rows.each do |y|
        if x[0] == y[0]
          items.push ({
            referrer: y[0],
            views: y[2],
            orders: y[3],
            orders_count: y[4],
            conversion_rate: number_to_percentage(y[5], precision: 2),
            views_today: x[2],
            orders_today: x[3],
            orders_count_today: x[4],
            conversion_rate_today: number_to_percentage(x[5], precision: 2)
          })
          c_views += y[2].to_i
          c_total_orders += y[3].to_i
          c_orders_count += y[4].to_i
          c_conversion_rate += y[5].to_i
          c_views_today += x[2].to_i
          c_orders_today += x[3].to_i
          c_orders_count_today += x[4].to_i
          c_conversion_rate_today += x[5].to_i
          c_total_count += 1
        else
          items.push ({
            referrer: y[0],
            views: y[2],
            orders: y[3],
            orders_count: y[4],
            conversion_rate: number_to_percentage(y[5], precision: 2),
            views_today: "0",
            orders_today: "0",
            orders_count_today: "0",
            conversion_rate_today: "0.00%"
          })
          c_views += y[2].to_i
          c_total_orders += y[3].to_i
          c_orders_count += y[4].to_i
          c_conversion_rate += y[5].to_i
          c_total_count += 1
        end
      end
    end
    items.push({
      referrer: 'Total',
      views: c_views,
      orders: c_total_orders,
      orders_count: c_orders_count,
      conversion_rate: "#{number_to_percentage(c_conversion_rate / c_total_count, precision: 2)} (ave)",
      views_today: c_views_today,
      orders_today: c_orders_today,
      orders_count_today: c_orders_count_today,
      conversion_rate_today: "#{number_to_percentage(c_conversion_rate_today / c_total_count, precision: 2)} (ave)"
    })
    items
  end

  def get_visits_of_page(page, startDate, endDate)
    analytics = @client.discovered_api('analytics', 'v3')
    if File.exists? GA_CACHED_API_FILE
      File.open(GA_CACHED_API_FILE) do |file|
        analytics = Marshal.load(file)
      end
    else
      analytics = @client.discovered_api('analytics', GA_API_VERSION)
      File.open(GA_CACHED_API_FILE, 'w') do |file|
        Marshal.dump(analytics, file)
      end
    end
    
    visitCount = @client.execute(:api_method => analytics.data.ga.get, :parameters => { 
      'ids' => "ga:" + GA_PROFILE_ID, 
      'start-date' => startDate,
      'end-date' => endDate,
      # 'dimensions' => "ga:source,ga:landingPagePath",
      'dimensions' => "ga:pagePath",
      # 'metrics' => "ga:sessions,ga:transactions,ga:itemQuantity,ga:transactionsPerSession",
      'metrics' => "ga:pageviews",
      # 'segment' => "dynamic::ga:landingPagePath=~#{page}"
      'segment' => "dynamic::ga:pagePath=~#{page}"
    })
  end

  private

  def get_visitor_stats(product, analytics, startDate, endDate)
    visitCount = @client.execute(:api_method => analytics.data.ga.get, :parameters => { 
      'ids' => "ga:" + GA_PROFILE_ID, 
      'start-date' => startDate,
      'end-date' => endDate,
      'dimensions' => "ga:source,ga:landingPagePath",
      'metrics' => "ga:sessions,ga:transactions,ga:itemQuantity,ga:transactionsPerSession",
      'segment' => "dynamic::ga:landingPagePath=~/#{product.slug}"
    })
  end

  def get_campaign_stats(product, analytics, startDate, endDate)
    campaignCount = @client.execute(:api_method => analytics.data.ga.get, :parameters => { 
      'ids' => "ga:" + GA_PROFILE_ID, 
      'start-date' => startDate,
      'end-date' => endDate,
      'dimensions' => "ga:campaign",
      'metrics' => "ga:sessions,ga:bounces,ga:uniquePageviews,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:transactions,ga:transactionRevenue,ga:revenuePerTransaction,ga:newUsers",
      'segment' => "dynamic::ga:landingPagePath=~/#{product.slug}"
    })
  end

  def get_hourly_stats(product, analytics, startDate, endDate)
    campaignCount = @client.execute(:api_method => analytics.data.ga.get, :parameters => { 
      'ids' => "ga:" + GA_PROFILE_ID, 
      'start-date' => startDate,
      'end-date' => endDate,
      'dimensions' => "ga:hour",
      'metrics' => "ga:sessions",
      'segment' => "dynamic::ga:landingPagePath=~/#{product.slug}"
    })
  end
end