class ParcelBarcode
  def initialize(params)
    @merchant_id = "SJZ"
    @merchant_code = "340"

    @consignment_id = params[:consignment_id].to_s

    @article_count = "01"
    @product_code = "00061"
    @service_code = "02"
    @post_paid_indicator = "0"
  end

  def stringify_codes
    @merchant_code + @consignment_id + @article_count + @product_code + @service_code + @post_paid_indicator 
  end

  def get_human_readable_check_digit
    odd = 0
    even = 0
    stringify_codes.reverse.chars.each_with_index do |item, index| 
      if index.odd?
        odd += item.to_i
      end
      if index.even?
        even += item.to_i
      end
    end

    even = even * 3

    check_digit = odd + even
    
    puts "total: #{check_digit}"

    next_divisible = ((check_digit.to_f / 10.to_f) + 1).to_i
    current = (check_digit.to_f / 10.to_f)
    
    check_digit = (((next_divisible - current)).round(1) * 10).to_i
    if check_digit == 10
      check_digit = 0
    end    
    check_digit
  end

  def get_eye_readable_article_no
    @merchant_id + @consignment_id + @article_count + @product_code + @service_code + @post_paid_indicator + get_human_readable_check_digit.to_s
  end
end

p = ParcelBarcode.new(consignment_id: 1234557)
puts "\n#{p.get_eye_readable_article_no}"