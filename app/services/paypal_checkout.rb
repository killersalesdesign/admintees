class PaypalCheckout
  def initialize(params, orders)
    @params  = params
    @orders  = orders
  end

  def orders
    @orders
  end

  def params
    @params
  end

  def user
    @user
  end

  def purchase?
    response = EXPRESS_GATEWAY.purchase(price_in_cents, express_purchase_options)
    response.success?
  end

  def create_order
    @details = EXPRESS_GATEWAY.details_for(@params[:token])

    if @details.params["CheckoutStatus"] == "PaymentActionCompleted"
      create_user

      @order = @user.orders.new(
        :ip_address => @params[:ip],
        :product_id => @params[:id],
        :token      => @details.params["token"],
        :payer_id   => @details.params["payer_id"],
        :shipping_info => @details.params["PaymentDetails"]["ShipToAddress"].to_json,
        :order_id   => SecureRandom.hex[0..7].upcase
      )

      if @order.save
        # orders_session = session[:orders]
        # session.delete(:orders)

        @orders.each do |o|
          @order.ordered_products.new(
            :quantity => o[:quantity],
            :size => o[:size],
            :category_id => o[:category_id]
          ).save
        end
      end

      @order.send_order_mail(@password)
    else
      @order = false
    end

    @order
  end

  private

  def create_user
    @user     = User.find_or_initialize_by_email(@details.params["payer"])
    @password = nil

    if @user.id.nil?
      @password                   = Devise.friendly_token[0,8]
      @user.password              = @password
      @user.password_confirmation = @password
      @user.first_name            = @details.params["first_name"]
      @user.last_name             = @details.params["last_name"]
      address                     = []

      address << @details.params["street1"] unless @details.params["street1"].blank?
      address << @details.params["street2"] unless @details.params["street2"].blank?
      address << @details.params["city_name"] unless @details.params["city_name"].blank?
      address << @details.params["country"] unless @details.params["country"].blank?
      @user.address = address.join(", ")

      @user.save
    end

    @user
  end


  def price_in_cents
    total   = 0
    product = Product.find(@params[:id])
    @orders.each do |p|
      total += p[:quantity] * product.price_and_profit_of_style(p[:category_id])[:amount]
    end

    (total * 100).round
  end

  def express_purchase_options
    {
      :ip => @params[:ip],
      :currency => 'AUD',
      :token => @params["token"],
      :payer_id => @params["PayerID"]
    }
  end
end