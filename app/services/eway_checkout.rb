class EwayCheckout
  def initialize(params)
    @params = params
    # last_name  = params[:name].split(" ").last
    # first_name = params[:name].gsub(last_name, "").strip
    @creditcard = ActiveMerchant::Billing::CreditCard.new(
      :number     => params[:cc_number],
      :month      => params[:month],
      :year       => params[:year],
      :first_name => params[:first_name],
      :last_name  => params[:last_name],
      :verification_value => params[:cvn]
    )

    @product   = Product.find(params[:product_id])
    order_id   = SecureRandom.hex[0..7].upcase

    @total = 0
    key    = 0
    @items = []
    desc   = []
    params[:orders][:qty].each do |o|
      category_id = params[:orders][:styles][key]
      size        = params[:orders][:sizes][key]
      style       = Category.find(category_id)
      info        = @product.price_and_profit_of_style(category_id)
      @total      += o.to_i * info[:amount].to_f

      @items << {:category_id => style.id, :quantity => o.to_i, :size => size, :price => info[:amount]}
      desc << "#{o} #{size} #{style.name}"
      key += 1
    end

    @total   = @total * 100
    @options = {
      :order_id => order_id,
      :email    => params[:email],
      :address => {
        :address1 => params[:address],
        :city     => params[:city],
        :state    => params[:state],
        :country  => params[:country],
        :zip      => params[:zip]
      },
      :shipping_address => {
        :address1 => params[:address],
        :city     => params[:city],
        :state    => params[:state],
        :country  => params[:country],
        :zip      => params[:zip]
      },
      :description => desc.join(", ")
    }
  end

  def paypal_shipping_format
    pp_format = {
      :Name            => "#{@params[:first_name].humanize} #{@params[:last_name].humanize}",
      :Street1         => @params[:address],
      :CityName        => @params[:city],
      :StateOrProvince => @params[:state],
      :Country         => @params[:country],
      :CountryName     => @params[:country],
      :PostalCode      => @params[:zip],
      :Phone           => ""
    }
    return pp_format
  end

  def get_order_id
    @options[:order_id]
  end

  def get_product
    @product
  end

  def get_items
    @items
  end

  def checkout(customer_id)
    gateway  = ActiveMerchant::Billing::EwayGateway.new(:login => customer_id)
    response = gateway.purchase(@total.to_i, @creditcard, @options)

    return response
  end
end