@CheckoutCtrl = [ "$scope", "$http", "geolocation", "COUNTRIES", ($scope, $http, geolocation, COUNTRIES) ->
  $scope.countries = COUNTRIES
  $scope.country   = COUNTRIES[0]
  $scope.checkout  =
    address:    gon.info.address
    city:       gon.info.city
    state:      gon.info.state
    country:    gon.info.country || $scope.country.iso
    zip:        gon.info.zip
    first_name: gon.info.first_name
    last_name:  gon.info.last_name
    email:      gon.info.email
    cc_number:  gon.info.cc_number
    month:      gon.info.month
    year:       gon.info.year
    cvn:        gon.info.cvn

  geolocation.getLocation().then (data) ->
    coords =
      lat:  data.coords.latitude
      long: data.coords.longitude

    $.getJSON "//maps.googleapis.com/maps/api/geocode/json?latlng=#{coords.lat},#{coords.long}&sensor=true", (data) ->
      if data && data.status == "OK"
        results = data.results.pop()
        try
          country = results.address_components[0].short_name
          $.each COUNTRIES, (key, value) ->
            $scope.country = value if value.iso == country

          $scope.checkout.country = gon.info.country || $scope.country.iso
          $scope.$apply()
      else
        $scope.country = COUNTRIES[0]

    coords

  $scope.selectCountry = (e) ->
    $scope.checkout.country = e.country.iso

  $(document).on "submit", "#new_order", ->
    error = false
    regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    $.each $scope.checkout, (key, value) ->
      unless value
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else if ["month", "year", "zip"].indexOf(key) isnt -1 and isNaN(value)
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else if key is "email" and !regex.test(value)
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else
        $("input[name=#{key}]").removeClass("txtbox-error")

    if error
      return false
    else
      return true

]