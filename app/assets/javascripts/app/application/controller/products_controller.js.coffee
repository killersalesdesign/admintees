@ProductShowCtrl = [ "$scope", "$http", "$location", "$modal", "flashr", ($scope, $http, $location, $modal, flashr) ->
  county = $('#my-count-down').county
    endDateTime: new Date(gon.product.date)
    reflection: false
    animation: 'scroll'
    theme: 'white'
    timezone: +10

  # $scope.categories = []
  # $.each gon.categories, (key, value) ->
  #   value.name = "#{value.name} - $#{gon.product.price.toFixed(2)}"
  #   $scope.categories.push value
  $scope.prices = []
  $.each gon.prices, (key, value) ->
    value.name = "#{value.type} - $#{value.amount.toFixed(2)}"
    $scope.prices.push value

  $scope.price      = $scope.prices[0]
  $scope.product    = gon.product
  # $scope.selected   = $scope.categories[0]

  $scope.images     = gon.gallery
  $scope.currentImg = $scope.images[0]
  currentIndex      = 0
  $scope.showNextPrev = (if $scope.images.length > 1 then true else false)

  $scope.selectCategory = (category) ->
    $scope.selected = category.price
    setTimeout(->
      $(".select2-chosen").text(category.price.name)
    , 50)

  $scope.current = (id) ->
    if id is $scope.currentImg.id
      return "current"

  $scope.selecteImage = (image, index) ->
    currentIndex = index
    $scope.currentImg = image

  $scope.next = ->
    currentIndex++
    currentIndex = $scope.images.length - 1 if currentIndex >= $scope.images.length - 1
    $scope.currentImg = $scope.images[currentIndex]

  $scope.back = ->
    currentIndex--
    currentIndex = 0 if currentIndex <= 0
    $scope.currentImg = $scope.images[currentIndex]

  # console.log gon.gallery
  $scope.openModal = ->
    $modal.open
      template: JST["app/application/views/products/modal"]
      backdrop: true
      windowClass: 'modal'
      controller: ["$scope", "$modalInstance", "Category", "product", "price", "COUNTRIES", ($scope, $modalInstance, Category, product, price, countries) ->
        $scope.prices = gon.prices

        Category.query product_id: gon.product.id, (categories) ->
          $scope.categories = []
          angular.forEach categories, (value, key) ->
            angular.forEach $scope.prices, (price, k) ->
              value.price = price.amount if price.category_id is value.id

            $scope.categories.push value

            if value.id is price.category_id
              $scope.selected = value
              # $scope.sizes   = value.sizes
              $scope.product  = product
              $scope.qty      = 1
              # $scope.selected.total_price = value.price * $scope.qty

        # init modal labels
        $scope.title  = "Select Your Size & Style"
        $scope.action = "/products/paypal_checkout"

        $scope.countries = countries
        $scope.country   = ""
        $scope.addMore   = []
        $scope.csrf      = $('meta[name=csrf-token]').attr('content')
        $scope.checkout  = 'order'
        $scope.gon_user  = gon.current_user
        $scope.user      = gon.current_user

        # $(document).on "submit", "#modal-form", ->
        #   console.log $scope.action
        #   return false

        $scope.paypal = ->
          $scope.action = "/products/paypal_checkout"
          # $modalInstance.dismiss('cancel')

        $scope.eway = ->
          $scope.action = "/products/set_checkout_session"
          # $modalInstance.dismiss('cancel')

        $scope.next = ->
          $scope.action   = "/products/eway_checkout"
          # $scope.title    = "Checkout via Credit Card"
          # $scope.checkout = 'cc'

        $scope.back = ->
          $scope.action   = "/products/paypal_checkout"
          $scope.title    = "Select Your Size"
          $scope.checkout = 'order'

        $scope.selectType = (e) ->
          $.each gon.prices, (key, value) ->
            e.selected.price = value.amount if value.category_id is e.selected.id

        $scope.selectType2 = (e) ->
          $.each gon.prices, (key, value) ->
            if value.category_id is e.c.id
              e.c.price = value.amount
              e.c.qty   = e.c.qty || 1

        $scope.submit = ->
          $modalInstance.dismiss('cancel')

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

        $scope.moreCheckOut = ->
          add_more     = $scope.categories[0]
          add_more.qty = 1
          $scope.addMore.push add_more

        $scope.removeStyle = ->
          $scope.addMore.pop()
      ]
      resolve:
        product: ->
          $scope.product
        price: ->
          $scope.price

  # fix for select2 dropdown
  setTimeout(->
    $(".select2-chosen").text($scope.price.name)
  , 50)
]