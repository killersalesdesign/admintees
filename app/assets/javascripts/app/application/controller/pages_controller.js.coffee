@PagesCtrl = [ "$scope", ($scope) ->
  # 355
  # $scope.height = "auto"
  wrapper = $("#content-wrapper").height()
  page    = $( document ).height() - 355

  $scope.height = (if page > wrapper then "#{page}px" else "auto")
]