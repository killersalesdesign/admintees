# Product list
@ProductsListCtrl = [ "$scope", "$http", "$location", "Product", "Website", "Brand", ($scope, $http, $location, Product, Website, Brand) ->
  $scope.selected_status = 0
  $scope.inactive        = false
  $scope.keyword         = ""
  $scope.website         = "all"
  $scope.paginate        = {}
  $scope.paginate.page   = 1
  $scope.paginate.limit  = 16
  $scope.currentPage     = 1
  $scope.numPerPage      = $scope.paginate.limit
  $scope.maxSize         = 5

  $scope.filters = ->
    filters = []
    filters.push "inactive=#{$scope.inactive}"
    filters.push "status=#{$scope.selected_status}" if $scope.selected_status < 4
    filters.push "keyword=#{$scope.keyword}"
    filters.push "website=#{$scope.website}"
    filters.push "page=#{$scope.paginate.page}"
    filters.push "limit=#{$scope.paginate.limit}"

    return filters.join("&")

  $scope.query_products = ->
    NProgress.start()
    $(".page-navigation").css("visibility", "hidden")
    $http.get "/admin/products/products_info?#{$scope.filters()}"
      .success (data, status) ->
        $scope.products_info = data
        $scope.paginate.total_pages = Math.ceil(data.total / $scope.paginate.limit)
        $scope.numPages             = Math.ceil data.total / $scope.numPerPage

        $http.get "#{Routes.all_products_admin_products_path()}?#{$scope.filters()}"
        .success (data, status) ->
          $scope.products = data
          NProgress.done()
          $(".page-navigation").css("visibility", "block")


  $scope.query_products()

  # $scope.products = Product.query (products) ->
  #   NProgress.done()
  websites               = Website.query()

  $scope.productUrl = (product) ->
    website = {}
    $.each websites, (key, value) ->
      website = value if value.id is product.website_id

    website

  $scope.show = (id) ->
    $location.path("/product/#{id}/edit")

  $scope.goToAddressLabel = (id) ->
    $location.path("/addresslabels/#{id}")

  $scope.delete = (id) ->
    if confirm("Delete Product?")
      Product.delete id: id, (success) ->
        $scope.products = $scope.products.filter (item) ->
          return item if item.id isnt id

  $scope.showStatus = (status) ->
    switch status
      when 0 then return "<span class='label label-primary'>Campaign</span>"
      when 1 then return "<span class='label label-success'>Printing</span>"
      when 2 then return "<span class='label label-warning'>In Stock</span>"
      else return ""

  $scope.selectStatus = (status) ->
    if status == 4
      $scope.inactive = true
    else
      $scope.inactive = false

    $scope.selected_status = status
    $scope.paginate.page   = 1
    $scope.currentPage     = 1
    $scope.query_products()

  $scope.prevPage = ->
    $scope.paginate.page -= 1
    $scope.query_products()

  $scope.nextPage = ->
    $scope.paginate.page += 1
    console.log $scope.paginate.page
    $scope.query_products()

  $scope.selectPage = (page) ->
    console.log "test"
    console.log page


  $scope.$watch 'currentPage + numPerPage', ->
    begin = ($scope.currentPage - 1) * $scope.numPerPage
    end = begin + $scope.numPerPage

    console.log $scope.currentPage, $scope.currentPage
    $scope.paginate.page = $scope.currentPage
    $scope.query_products()
    # $scope.filteredTodos = $scope.todos.slice(begin, end)
    # console.log $scope.currentPage, begin, end
    return

]

# Product new
@ProductsNewCtrl = [ "$scope", "$http", "$location", "$upload", "$timeout", "flashr", "Category", "Product", "Website", "Brand", "ItemColor", ($scope, $http, $location, $upload, $timeout, flashr, Category, Product, Website, Brand, ItemColor) ->
  NProgress.start()
  $scope.product    = new Product()
  $scope.productTags = []
  # initialize default value
  $scope.product.print_colors = 1

  $scope.categories = Category.query()
  $scope.websites   = Website.query (websites) ->
    $scope.selected_website = websites[0]
    $scope.product.website_id = $scope.selected_website.id

  $scope.brands = Brand.query (brands) ->
    $scope.selected_brand = brands[0]
    $scope.product.brand_id = $scope.selected_brand.id

  $scope.item_colors = ItemColor.query (item_colors) ->
    $scope.selected_item_color = item_colors[0]
    $scope.product.item_color_id = $scope.selected_item_color.id

  # $scope.categories = Category.query (categories) ->
  #   $scope.product.categories.push categories[0].id
  $scope.product.categories = []
  $scope.product.active     = true
  $scope.product.show       = true
  $scope.product.date       = moment().format("YYYY-MM-DD")
  $scope.product.prices     = []
  $scope.product.price      = 0
  $scope.product.profit     = 0
  $scope.product.postage    = 0
  $scope.product.bundle     = false
  $scope.product.gift_card     = false

  # default values
  $scope.product.international_cutoff = 0
  $scope.product.additional_item_cost = 0

  # $scope.product.website_id = $scope.selected_website.id

  uploaded_files    = []
  $scope.images     = []

  $(document).on "click", ".cbox-category", (e) ->
    val   = $(e.currentTarget).val()
    val   = parseInt(val)
    # index = (if $scope.product.categories.lenght > 0 then $scope.product.categories.indexOf(val) else -1)
    type  = $.trim $(e.currentTarget).parent().text()

    if typeof $scope.product.prices is "undefined"
      $scope.product.prices = []

    if $(e.currentTarget).is(":checked")
      $scope.product.prices.push {category_id: val, type: type}
      # $scope.product.categories.push val if index == -1
    else
      # $scope.product.categories.splice(index, 1) if index > -1
      $scope.product.prices = $scope.product.prices.filter (item) ->
        return item if item.category_id isnt val

    $scope.product.categories = []
    $.each $scope.product.prices, (key, value) ->
      $scope.product.categories.push value.category_id
      switch value.type.trim()
        when "Gift Cards"
          $scope.product.gift_card = true
        when "Mens Tee"
          value.brand     = (if value.brand then value.brand else $scope.brands[7])

        when "Womens Tee"
          value.brand     = (if value.brand then value.brand else $scope.brands[4])

        when "Hoodie"
          value.amount    = (if value.amount then value.amount else 55)
          value.profit    = (if value.profit then value.profit else 28)
          value.postage   = (if value.postage then value.postage else 9.15)
          value.i_postage = (if value.i_postage then value.i_postage else 10)
          value.brand        = (if value.brand then value.brand else $scope.brands[1])

        when "Womens Hoodie"
          value.amount    = (if value.amount then value.amount else 55)
          value.profit    = (if value.profit then value.profit else 28)
          value.postage   = (if value.postage then value.postage else 9.15)
          value.i_postage = (if value.i_postage then value.i_postage else 10)
          value.brand        = (if value.brand then value.brand else $scope.brands[1])

        when "Women's V-Neck"
          value.brand     = (if value.brand then value.brand else $scope.brands[5])

        when "Women's V Neck"
          value.brand     = (if value.brand then value.brand else $scope.brands[5])

        when "Women's Racerback Singlet"
          value.amount    = (if value.amount then value.amount else 30)
          value.brand     = (if value.brand then value.brand else $scope.brands[12])

        when "Men's Singlet"
          value.amount    = (if value.amount then value.amount else 30)
          value.brand     = (if value.brand then value.brand else $scope.brands[11])
        else
          $scope.product.gift_card = false


      # default values
      value.amount       = (if value.amount then value.amount else 32)
      value.profit       = (if value.profit then value.profit else 18)
      value.postage      = (if value.postage then value.postage else 7.10)
      value.i_postage    = (if value.i_postage then value.i_postage else 7.10)
      value.brand        = (if value.brand then value.brand else $scope.brands[0])
      value.item_color   = (if value.item_color then value.item_color else  $scope.item_colors[0])
      value.print_colors = (if value.print_colors then value.print_colors else 1)

    $scope.$apply()

  # $scope.selectCategory = (e) ->
  #   console.log e

  $scope.generateGiftCode = ->
    text = ""
    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    i = 0

    while i < 10
      text += possible.charAt(Math.floor(Math.random() * possible.length))
      i++
    text
  $scope.onSelect = (website) ->
    $scope.product.website_id = website.selected_website.id

  $scope.brandSelect = (brand) ->
    $scope.product.brand_id = brand.selected_brand.id

  $scope.colorSelect = (item_color) ->
    $scope.product.item_color_id = item_color.selected_item_color.id

  $scope.cboxModel = (cat) ->
    index = product.categories.indexOf(cat)
    if index > -1
      return $scope.product.categories[index]
    else
      return ""

  $scope.onFileSelect = ($files, image) ->
    console.log image
    $.each $files, (key, file) ->
      last_file        = uploaded_files[uploaded_files.length - 1]
      file.index       = (if last_file then last_file.index + 1 else 1)
      file.category_id = image.category_id
      uploaded_files.push file

      fileReader = new FileReader()
      fileReader.readAsDataURL(file)
      fileReader.onload = (e) ->
        $timeout ->
          $scope.images.push {id: null, src: e.target.result, index: file.index, type: image.type}
          $scope.last_upload = {id: null, src: e.target.result, index: file.index, type: image.type}

  $scope.view_image = (e) ->
    $scope.last_upload = e.image

  $scope.selected_image = (e) ->
    if $scope.last_upload.src is e.image.src
      return "current"

  $scope.removeImage = (image) ->
    if confirm("Are you sure you want to remove this image?")
      uploaded_files = uploaded_files.filter (item) ->
        return item if item.index isnt image.index
      $scope.images  = $scope.images.filter (item) ->
        return item if item.index isnt image.index

      $scope.last_upload = $scope.images[$scope.images.length - 1]
      flashr.now.success('Image removed!')

  $scope.save = (product) ->
    NProgress.start()
    product.tags = JSON.stringify($scope.productTags)
    product.$save (savedProduct, putResponseHeaders) ->
      if uploaded_files.length > 0
        $.each uploaded_files, (key, file) ->
          $scope.upload = $upload.upload(
            url: Routes.product_image_admin_products_path()
            data: { product_id: savedProduct.id, category_id: file.category_id }
            file: file
          ).progress((evt) ->
            # console.log "percent: " + parseInt(100.0 * evt.loaded / evt.total)
            return
          ).success((data, status, headers, config) ->
            # file is uploaded successfully
            if uploaded_files.length == (key + 1)
              flashr.now.success('Added a new product!')
              $location.path("/products")
              NProgress.done()

            return
          )
      else
        flashr.now.success('Added a new product!')
        $location.path("/products")
        NProgress.done()

  $scope.tags = [
    {
      text: "just"
    }
    {
      text: "some"
    }
    {
      text: "cool"
    }
    {
      text: "tags"
    }
  ]
  NProgress.done()
]

# Product Edit
@ProductsEditCtrl = [ "$scope", "$http", "$location", "$upload", "$timeout", "$routeParams", "flashr", "Category", "Product", "Website", "Brand", "ItemColor", ($scope, $http, $location, $upload, $timeout, $routeParams, flashr, Category, Product, Website, Brand, ItemColor) ->
  NProgress.start()

  $scope.categories = Category.query (categories) ->
    $scope.product = Product.get id: $routeParams.id, (product) ->
      $scope.productTags = angular.fromJson(product.tags)
      $.each product.gallery, (key, value) ->
        $scope.images.push {id: value.id, src: value.medium, type: value.type}
        $scope.last_upload = {id: value.id, src: value.medium, type: value.type}
      $scope.product.date = moment(product.date).format("YYYY-MM-DD")

      $scope.websites   = Website.query (websites) ->
        $.each websites, (key, value) ->
          $scope.selected_website = value if value.id == product.website_id

        $scope.product.website_id = $scope.selected_website.id

      $scope.brands = Brand.query (brands) ->
        $.each brands, (key, value) ->
          $scope.selected_brand = value if value.id == product.brand_id

        $scope.product.brand_id = $scope.selected_brand.id

      $scope.item_colors = ItemColor.query (item_colors) ->
        $.each item_colors, (key, value) ->
          $scope.selected_item_color = value if value.id == product.item_color_id

        $scope.product.item_color_id = $scope.selected_item_color.id

      if product.stocks.length is 0
        $scope.product.stocks = []
        $.each product.prices, (index, price) ->
          stock =
            category_id: price.category_id
            type:        price.type
            sizes:       []

          $.each categories, (key, value) ->
            if value.id is price.category_id
              sizes      = value.sizes.split(", ")
              stock.sizes = []
              $.each sizes, (index, size) ->
                stock.sizes.push {label: size, value: 0}

          $scope.product.stocks.push stock

      else
        $scope.product.stocks = product.stocks

      NProgress.done()

  $scope.generateGiftCode = ->
    text = ""
    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    i = 0

    while i < 10
      text += possible.charAt(Math.floor(Math.random() * possible.length))
      i++
    $scope.product.gift_card_code = text

  $scope.testFunc = (e) ->
    $scope.stocks

  $scope.images     = []
  uploaded_files    = []

  $(document).on "click", "input[type=checkbox]", (e) ->
    val   = $(e.currentTarget).val()
    val   = parseInt(val)
    index = (if $scope.product.categories.lenght > 0 then $scope.product.categories.indexOf(val) else -1)
    type  = $.trim $(e.currentTarget).parent().text()

    if $(e.currentTarget).is(":checked")
      $scope.product.prices.push {category_id: val, type: type}
      # $scope.product.categories.push val if index == -1

      console.log "checked", val
      stock =
        category_id: val
        type:        type
        sizes:       []

      $.each $scope.categories, (key, value) ->
        if value.id is val
          sizes       = value.sizes.split(", ")
          stock.sizes = []
          $.each sizes, (index, size) ->
            stock.sizes.push {label: size, value: 0}

      $scope.product.stocks.push stock
    else
      # $scope.product.categories.splice(index, 1) if index > -1
      $scope.product.prices = $scope.product.prices.filter (item) ->
        return item if item.category_id isnt val

      $scope.product.stocks = $scope.product.stocks.filter (item) ->
        return item unless item.category_id is val

    $scope.product.categories = []
    $.each $scope.product.prices, (key, value) ->
      $scope.product.categories.push value.category_id

    $scope.$apply()

  $scope.productStatus = (status) ->
    $scope.product.status = status

  $scope.onSelect = (website) ->
    $scope.product.website_id = website.selected_website.id

  $scope.brandSelect = (brand) ->
    $scope.product.brand_id = brand.selected_brand.id

  $scope.colorSelect = (item_color) ->
    $scope.product.item_color_id = item_color.selected_item_color.id

  $scope.view_image = (e) ->
    $scope.last_upload = e.image

  $scope.selected_image = (e) ->
    if $scope.last_upload.src is e.image.src
      return "current"

  $scope.removeImage = (image) ->
    if confirm("Are you sure you want to remove this image?")
      NProgress.start()
      if image.id
        $http.delete(Routes.delete_product_image_admin_product_path(image.id)
        ).success((data, status) ->
          flashr.now.success('Image removed!')
          $scope.images  = $scope.images.filter (item) ->
            return item if item.id isnt image.id
          $scope.last_upload = $scope.images[$scope.images.length - 1]
          NProgress.done()
        ).error (data, status) ->
          flashr.now.error(data.errors)
          NProgress.done()
      else
        uploaded_files = uploaded_files.filter (item) ->
          return item if item.index isnt image.index
        $scope.images  = $scope.images.filter (item) ->
          return item if item.index isnt image.index

        $scope.last_upload = $scope.images[$scope.images.length - 1]
        flashr.now.success('Image removed!')
        NProgress.done()

  $scope.onFileSelect = ($files, image) ->
    $.each $files, (key, file) ->
      last_file  = uploaded_files[uploaded_files.length - 1]
      file.index = (if last_file then last_file.index + 1 else 1)
      file.category_id = image.category_id
      uploaded_files.push file

      fileReader = new FileReader()
      fileReader.readAsDataURL(file)
      fileReader.onload = (e) ->
        $timeout ->
          $scope.images.push {id: null, src: e.target.result, index: file.index, type: image.type}
          $scope.last_upload = {id: null, src: e.target.result, index: file.index, type: image.type}

  $scope.update = (product) ->
    NProgress.start()
    product.tags = JSON.stringify($scope.productTags)
    product.$update (updatedProduct, putResponseHeaders) ->
      $scope.product.date = moment(updatedProduct.date).format("YYYY-MM-DD")
      if uploaded_files.length > 0
        $.each uploaded_files, (key, file) ->
          $scope.upload = $upload.upload(
            url: Routes.product_image_admin_products_path()
            data: { product_id: updatedProduct.id, category_id: file.category_id }
            file: file
          ).progress((evt) ->
            # console.log "percent: " + parseInt(100.0 * evt.loaded / evt.total)
            return
          ).success((data, status, headers, config) ->
            # file is uploaded successfully
            $scope.images  = $scope.images.filter (item) ->
              if item.index is file.index
                item.id    = data.id
                item.index = null
                $scope.last_upload = {id: data.id, src: item.src}

              return item

            if uploaded_files.length == (key + 1)
              flashr.now.success('Updated product!')
              NProgress.done()
              uploaded_files = []

            return
          )
      else
        flashr.now.success('Updated product!')
        NProgress.done()
]

# Product Stats
@ProductsStatsCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", ($scope, $http, $location, Product, $routeParams) ->
  colors = ["#acc270", "#45acd0", "#f7b532", "#f07c00", "#2d4154", "#c0392b"]
  sizeclasses = ["light-blue-square", "light-blue-square", "yellow-orange-square", "dark-orange-square", "dark-red-square", "dark-red-square", "dark-green-square"]
  barclasses = ["pink-square", "yellow-square", "green-square"]
  bcolors = ["rgba(241,132,98,0.8", "rgba(250,212,107,0.8)", "rgba(172,194,112,0.8)"]
  progress = 0

  $scope.colors = []
  $scope.sizes = []
  $scope.barcolors = []
  $scope.referrers = []
  $scope.campaigns = []
  $scope.product = {}
  $scope.sales_today = 0
  $scope.sales_yesterday = 0

  $scope.get_sales_today = (id)->
    $http.get "/admin/statistics/" + id + "/sales_today"
      .success (data, status) ->
        $("#todays-profit").html("$" + data.profit)
        $("#todays-customers").html(data.customers)
        $("#todays-products").html(data.products)
        $("#todays-orders").html(data.orders)
        today = parseFloat($scope.sales_today)
        yesterday = parseFloat($scope.sales_yesterday)
        if(yesterday!=0)
          diff = ((today - yesterday)/yesterday) * 100
          $("#todays-profit-diff").html(diff + " %")
        else
          $("#todays-profit-diff").html(0+" %")
        NProgress.set(progress += 0.1)


  $scope.get_sales_yesterday = (id)->
    $http.get "/admin/statistics/" + id + "/sales_yesterday"
      .success (data, status) ->
        $scope.sales_yesterday = data.profit
        $("#yesterdays-sale").html("$" + data.profit)
        $("#yesterdays-customers").html(data.customers)
        $("#yesterdays-products").html(data.products)
        $("#yesterdays-orders").html(data.orders)
        today = parseFloat($scope.sales_today)
        yesterday = parseFloat($scope.sales_yesterday)
        if(yesterday!=0)
          diff = ((today - yesterday)/yesterday) * 100
          $("#todays-profit-diff").html(diff + " %")
        else
          $("#todays-profit-diff").html(0+" %")
        NProgress.set(progress += 0.1)

  $scope.get_sales_all_time = (id)->
    $http.get "/admin/statistics/" + id + "/sales_all_time"
      .success (data, status) ->
        $("#alltime-sale").html("$" + data.profit)
        $("#alltime-customers").html(data.customers)
        $("#alltime-products").html(data.products)
        $("#alltime-orders").html(data.orders)
        NProgress.set(progress += 0.1)

  $scope.get_product = (id)->
    $http.get "/admin/statistics/" + id + "/product"
      .success (data, status) ->
        $scope.product = data
        NProgress.set(progress += 0.1)

  $scope.get_sizes = (id, start_date="")->
    $http.get "/admin/statistics/" + id + "/sizes?start_date=" + start_date
      .success (data, status) ->
        $scope.sizes = data.sizes
        if (start_date != "")
          NProgress.set(1)
        else
          NProgress.set(progress += 0.1)

  $scope.get_ordered = (id)->
    $http.get "/admin/statistics/" + id + "/ordered"
      .success (data, status) ->
        count = 0
        data_sets = []
        for key, value of data.datasets
          data_sets.push
            fillColor: bcolors[count]
            strokeColor: bcolors[count]
            data: value.data
          count += 1

        count = 0
        for key, value of data.categories
          $scope.barcolors.push
            divclass: barclasses[count]
            txt: value.name
          count += 1

        barChartData =
          labels : data.labels
          datasets : data_sets

        myBar = new Chart(document.getElementById("bar-chart").getContext("2d")).Bar(barChartData);
        NProgress.set(progress += 0.1)

  $scope.get_hourly_orders = (id, start_date="")->
    $http.get "/admin/statistics/" + id + "/hourly_orders?start_date=" + start_date
      .success (data, status) ->
        count = 0
        data_sets = []
        for key, value of data.datasets
          data_sets.push
            fillColor: bcolors[count]
            strokeColor: bcolors[count]
            data: value.data
          count += 1

        lineChartData =
          labels : data.labels
          datasets : data_sets

        myLine = new Chart(document.getElementById("line-chart").getContext("2d")).Line(lineChartData)
        if (start_date != "")
          NProgress.set(1)
        else
          NProgress.set(progress += 0.1)

  $scope.get_hourly_views = (id, start_date="")->
    $http.get "/admin/statistics/" + id + "/views?start_date=" + start_date
      .success (data, status) ->
        count = 0
        data_sets = []
        for key, value of data.datasets
          data_sets.push
            fillColor: bcolors[count]
            strokeColor: bcolors[count]
            data: value.data
          count += 1

        lineChartData =
          labels : data.labels
          datasets : data_sets

        myLine = new Chart(document.getElementById("line-chart-views").getContext("2d")).Line(lineChartData)
        if (start_date != "")
          NProgress.set(1)
        else
          NProgress.set(progress += 0.2)

  $scope.get_analytics = (id)->
    $http.get "/admin/statistics/" + id + "/analytics"
      .success (data, status) ->
        count = 0
        data_sets = []

        $scope.referrers = data

        $http.get "/admin/statistics/" + id + "/campaigns"
          .success (data, status) ->
            count = 0
            data_sets = []

            $scope.campaigns = data

      NProgress.set(progress += 0.2)

  $scope.get_stats = (id)->
    NProgress.start()
    $(".page-navigation").css("visibility", "hidden")

  $scope.get_stats( $routeParams.id )
  $scope.get_sales_today( $routeParams.id )
  $scope.get_sales_yesterday( $routeParams.id )
  $scope.get_sales_all_time( $routeParams.id )
  $scope.get_product( $routeParams.id )
  $scope.get_sizes( $routeParams.id )
  $scope.get_ordered( $routeParams.id )
  $scope.get_hourly_orders( $routeParams.id )
  $scope.get_hourly_views( $routeParams.id )
  $scope.get_analytics( $routeParams.id )

  $(document).on "change", "#size-date-selector", (e)->
    NProgress.start()
    $scope.get_sizes( $routeParams.id, $(this).val())

  $(document).on "change", "#hourly-view-date-selector", (e)->
    NProgress.start()
    $scope.get_hourly_views( $routeParams.id, $(this).val())
    $scope.get_hourly_orders( $routeParams.id, $(this).val())

  $(document).on "click", "a.show-views", (e)->
    e.preventDefault()
    $('a.show-views').addClass("btn-primary").removeClass("btn-default")
    $('a.show-orders').removeClass("btn-primary")
    $(".line-chart-container").addClass('hide')
    $(".line-chart-views-container").removeClass('hide')
  $(document).on "click", "a.show-orders", (e)->
    e.preventDefault()
    $('a.show-orders').addClass("btn-primary").removeClass("btn-default")
    $('a.show-views').removeClass("btn-primary")
    $(".line-chart-container").removeClass('hide')
    $(".line-chart-views-container").addClass('hide')
]


@ProductsMailingCtrl = [ "$scope", "$http", "$location", "flashr", "Product", "Email", "$routeParams", ($scope, $http, $location, flashr, Product, Email, $routeParams) ->
  NProgress.start()

  $scope.product = Product.get id: $routeParams.id, (product) ->
    $scope.emails = Email.query (emails) ->
      $scope.mailing = []
      $scope.sent = []

      for email in emails
        result = $.grep product.mailing, (value, index) ->
          return value.mail_id is email.id

        if result.length > 0
          $scope.mailing[email.id] = 1
          $scope.sent[email.id] = moment(result[0].sent).format("MMMM D, YYYY")
        else
          $scope.mailing[email.id] = 0
          $scope.sent[email.id] = ""

      NProgress.done()

  $scope.setReadyMail = (elem, radio_elem) ->
    $scope.ready_mail = elem
    $scope.selected_radio = radio_elem

  $scope.cancelSend = () ->
    elem = $($scope.selected_radio.target)
    elem.prop("checked", false)

    elem.parent("td").next("td").find("input[type='radio']").prop("checked", true)
    true

  $scope.mailingUpdate = (elem, product) ->
    success_msg = "Request succeeded"
    if elem is ""
      elem = $scope.ready_mail
      success_msg = "Emails sent for processing"

    value       = $scope.mailing[elem.email.id]
    found       = false
    new_mailing = []

    for email in product.mailing
      if email.mail_id is elem.email.id
        if parseInt(value) is 1
          email.sent = moment().toDate()
          product.to_mail_id = elem.email.id
          found = true
          new_mailing.push email
      else
        new_mailing.push email

    if found is false and parseInt(value) is 1
      new_mailing.push {"mail_id": elem.email.id, "sent": moment().toDate()}
      product.to_mail_id = elem.email.id

    product.mailing = new_mailing


    product.$update (updatedProduct, putResponseHeaders) ->
      $('#sendModal').modal('hide')
      flashr.now.success(success_msg)

]