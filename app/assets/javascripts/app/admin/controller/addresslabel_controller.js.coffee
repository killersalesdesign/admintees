@AddressLabelCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", "Order", ($scope, $http, $location, Product, $routeParams, Order) ->
  NProgress.start()
  $scope.product = Product.get(id: $routeParams.pid)
  $scope.orders = Order.query(product_id: $routeParams.pid)

  $scope.root_path = Routes.root_path()
  $scope.showAddress = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Name + "<br/>"
    shtml += shippingInfo.Street1 + "<br/>"
    if shippingInfo.Street2 isnt null and shippingInfo.Street2 isnt undefined
      shtml += shippingInfo.Street2 + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.CityName isnt null and shippingInfo.CityName isnt undefined
      shtml += shippingInfo.CityName + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.StateOrProvince isnt null and shippingInfo.StateOrProvince isnt undefined
      shtml += shippingInfo.StateOrProvince + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.Country isnt null and shippingInfo.Country isnt undefined
      shtml += shippingInfo.Country + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.Phone isnt null and shippingInfo.Phone isnt undefined
      shtml += shippingInfo.Phone + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.PostalCode isnt null and shippingInfo.PostalCode isnt undefined
      shtml += shippingInfo.PostalCode + "<br/>"
    else
      shtml += "<br/>"


    return shtml

  $scope.range = ->
    n = Math.ceil($scope.orders.length / 100)
    new Array(n)

  $scope.showStatus = (status) ->
    switch status
      when 0 then return "<span class='label label-primary'>Campaign</span>"
      when 1 then return "<span class='label label-success'>Printing</span>"
      when 2 then return "<span class='label label-warning'>In Stock</span>"
      else return ""

  NProgress.done()
]
