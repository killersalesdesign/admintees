@OrdersV2ListCtrl = [ "$scope", "$http", "$location", "V2Order", "flashr", "$routeParams", ($scope, $http, $location, Order, flashr, $routeParams) ->
  $scope.currentPage = 1
  $scope.numPerPage  = 100
  $scope.maxSize     = 5
  $scope.numPages    = 1
  $scope.filters     =
    keyword: ''
    shipped: ''
    printed: ''
    from:    ''
    to:      ''
    page:    $scope.currentPage

  get_orders = (callback) ->
    params = []
    $.each $scope.filters, (k, v) ->
      params.push "#{k}=#{v}"

    $http.get "#{Routes.total_orders_admin_v2_orders_path()}?#{params.join("&")}"
      .success (data) ->
        $scope.total_orders = data
        $scope.orders       = Order.query $scope.filters
        callback()

  $scope.$watch 'currentPage + numPerPage', ->
    NProgress.start()
    begin = ($scope.currentPage - 1) * $scope.numPerPage
    end   = begin + $scope.numPerPage
    $scope.filters.page = $scope.currentPage

    get_orders ->
      $scope.numPages = Math.ceil $scope.total_orders / $scope.numPerPage
      NProgress.done()

  $scope.searchOrders = ->
    NProgress.start()
    get_orders ->
      $scope.numPages = Math.ceil $scope.total_orders / $scope.numPerPage
      NProgress.done()

  $scope.show_product = (id) ->
    $location.path "/product/#{id}/edit"

  $scope.show_order = (id) ->
    $location.path "/orders/#{id}/show"

  $scope.show_customer = (id) ->
    $location.path "/customers/#{id}/show"
]