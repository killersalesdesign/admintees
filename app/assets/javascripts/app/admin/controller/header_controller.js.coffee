@HeaderController = [ "$scope", "$http", "$location", ($scope, $http, $location) ->
  $scope.admin = gon.current_admin

  $scope.isActive = (viewLocation) ->
    if viewLocation is $location.path()
      return "active"

  $scope.getNotifications = ->
    $http.get "/admin/notifications"
      .success (data, status) ->
        $scope.notifications = data

  $scope.getNotifications()

  $scope.showNotifications = (e)->
    $('#notificationsModel').modal('show')
    return ''

  $scope.hideNotifications = (e)->
    $('.notification-icon').hide()
    $http.get "/admin/notifications/clear"
    return ''
    
]