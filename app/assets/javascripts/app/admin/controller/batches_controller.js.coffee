
@BatchListCtrl = [ "$scope", "$http", "Batch", ($scope, $http, Batch) ->
  # $scope.batches = Batch.query()
  indexedTeams = []
  $scope.currentPage = 1
  $scope.totalPages = 5
  $scope.pageLimit = 10
  $scope.numPages = 5
  $scope.maxSize = 5

  $scope.dataformsToFilter = ->
    indexedTeams = []
    $scope.batches

  $scope.filterTeams = (data) ->
    teamIsNew = indexedTeams.indexOf(data.batch_key) is -1
    indexedTeams.push data.batch_key  if teamIsNew
    teamIsNew

  $scope.prevPage = () ->
    console.log 'prevPage'
    $scope.currentPage -= 1
    $scope.getBatches()

  $scope.nextPage = () ->
    console.log 'nextPage'
    $scope.currentPage += 1
    $scope.getBatches()

  $scope.$watch 'currentPage + numPerPage', ->
    begin = ($scope.currentPage - 1) * $scope.numPerPage
    end = begin + $scope.numPerPage
    console.log 'currentPage: ', $scope.currentPage
    $scope.getBatches()

  $scope.getBatches = () ->
    NProgress.start()
    $http.get "/admin/batches?page=#{$scope.currentPage}&limit=#{$scope.pageLimit}"
      .success (data, status) ->
        $scope.batches = data.batches

        $scope.totalPages = Math.ceil(data.total / $scope.pageLimit)
        $scope.numPages = Math.ceil(data.total / $scope.pageLimit)

        NProgress.done()

  $scope.getBatches()

]

@BatchShowCtrl = [ "$scope", "Batch", "$routeParams" ,($scope, Batch, $routeParams) ->
  $scope.batch_id = $routeParams.id
  $scope.categories = []
  $scope.boxCategory = []

  $scope.batches = Batch.query(batch_key: $routeParams.id
    , (data)->
      angular.forEach $scope.batches, (val, key) ->
        val.stats = angular.fromJson(val.batch_stats)
        angular.forEach val.stats, (stat, skey) ->

          teamIsNew = $scope.categories.indexOf(stat.category_id) is -1
          $scope.categories.push stat.category_id  if teamIsNew
          if teamIsNew
            $scope.boxCategory.push {category_id: stat.category_id, category_name: stat.type, stats: [{product_name: val.product_name, product_id: val.product_id, sizes: stat.sizes}]}
          else
            angular.forEach $scope.boxCategory, (boxCat, bkey) ->
              if boxCat.category_id is stat.category_id
                boxCat.stats.push {product_name: val.product_name, product_id: val.product_id, sizes: stat.sizes}

      console.log data
      angular.forEach $scope.boxCategory, (boxCat, bkey) ->
        allSizes  = []
        allExtras = []
        angular.forEach boxCat.stats, (stats, skey) ->

          angular.forEach stats.sizes, (size, sizekey) ->
            allSizes.push size.name
            allExtras.push size.extra
        uniqueArray = allSizes.filter((item, pos) ->
          allSizes.indexOf(item) is pos
        )
        console.log allExtras
        boxCat.all_sizes = uniqueArray
      console.log "awa ni bai!", $scope.boxCategory


    )

  $scope.showTotal = (sizes) ->
    total = 0
    angular.forEach sizes, (val, key) ->
      total += parseInt(val.sold)
    return total

  $scope.getSold = (sizeName, sizes) ->
    total = 0
    angular.forEach sizes, (val, key) ->
      if val.name is sizeName
        if val.extra isnt undefined
          total = parseInt val.sold + parseInt val.extra
        else
          total = parseInt val.sold
    return total

  $scope.getTotal = (sizeName, box) ->
    total = 0
    angular.forEach box, (val, key) ->
      angular.forEach val.sizes, (sval, skey) ->
        if sval.name is sizeName
          if sval.extra isnt undefined
            total += parseInt(sval.sold + sval.extra)
          else
            total += parseInt sval.sold
    return total


  $scope.showAllTotal = (box) ->
    total = 0
    angular.forEach box, (val, key) ->
      angular.forEach val.sizes, (sval, skey) ->
        if sval.extra isnt undefined
          total += parseInt(sval.sold + sval.extra)
        else
          total += parseInt(sval.sold)

    return total

]




