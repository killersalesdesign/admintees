@StatisticsListCtrl = [ "$scope", "$http", "$location", "Product", "$filter",  ($scope, $http, $location, Product, $filter) ->
  # NProgress.start()
  # $scope.orders = Order.query()
  # $scope.show = (id) ->
  #   $location.path("/orders/#{id}/show")

  # NProgress.done()

  $scope.chartType = "bar"
  $scope.chartType2 = "bar"
  $scope.chartType3 = "bar"
  $scope.chartType4 = "bar"

  $scope.data =
    series: []
    data: []
  $scope.data2 =
    series: []
    data: []

  $scope.data3 =
    series: []
    data: []

  $scope.data4 =
    series: []
    data: []

  $scope.config =
    labels: false
    title: "Every Day Sales"
    legend:
      display: true
      position: "left"

  $scope.config2 =
    labels: false
    title: "Profits"
    legend:
      display: true
      position: "left"

  $scope.config3 =
    labels: false
    title: "Product Types"
    legend:
      display: true
      position: "left"

  $scope.config4 =
    labels: false
    title: "Product Sizes"
    legend:
      display: true
      position: "left"

  # $scope.products = Product.query(
  #  (data) ->
  #   angular.forEach data, (v, k) ->
  #     $scope.data2.series.push v.title
  #     tempData =
  #       x: v.title
  #       y: [v.profit]
  #     $scope.data2.data.push tempData
  #   $scope.chartType2 = "bar"
  # )

  $scope.productSelect = 0
  $http.get(Routes.all_products_admin_statistics_path()
  ).success((data, status) ->
    $scope.products = data
    angular.forEach data, (v, k) ->
      $scope.data2.series.push v.title
      $scope.productSelect = v.id
      tempData =
        x: v.title
        y: [v.profit]
      $scope.data2.data.push tempData
    $scope.chartType2 = "bar"
  )

  $http.get(Routes.all_products_list_admin_statistics_path()
  ).success((newdata, status) ->
    $scope.productList = newdata
  )

  $scope.setSelectedProduct = ->
    $http.get(Routes.all_products_admin_statistics_path()+'?id='+$scope.productSelect
    ).success((data, status) ->
      $scope.products = data
      angular.forEach data, (v, k) ->
        $scope.data2.series.push v.title
        $scope.productSelect = v.id
        tempData =
          x: v.title
          y: [v.profit]
        $scope.data2.data.push tempData
      $scope.chartType2 = "bar"
    )

  $scope.setSelectedProductB = (id)->
    $http.get(Routes.all_products_admin_statistics_path()+'?id='+id
    ).success((data, status) ->
      $scope.products = data
      angular.forEach data, (v, k) ->
        $scope.data2.series.push v.title
        $scope.productSelect = v.id
        tempData =
          x: v.title
          y: [v.profit]
        $scope.data2.data.push tempData
      $scope.chartType2 = "bar"
    )

  $ ->

    products = new Bloodhound
      datumTokenizer: (d) ->
        return Bloodhound.tokenizers.whitespace d.title
      queryTokenizer: Bloodhound.tokenizers.whitespace
      limit: 10
      remote:
        url: "/admin/products/autocomplete?query=%QUERY"

    products.initialize()

    $("#product-search").typeahead(null,{
        name: "product",
        source: products.ttAdapter(),
        displayKey: "title",
        templates:
          suggestion: Handlebars.compile('<strong>{{title}}</strong>')
      }
    ).bind("typeahead:selected", (obj, datum, name) ->
      console.log 'datummmmmmmmmmmm: ', datum
      $scope.setSelectedProductB(datum.id)
    )

  # $http.get(Routes.daily_sales_admin_statistics_path()
  # ).success((newdata, status) ->
  #   angular.forEach newdata, (v, k) ->
  #     $scope.data.series.push k
  #     tempData =
  #       x: $filter('date')(k,'medium')
  #       y: [v.length]
  #     $scope.data.data.push tempData
  #   $scope.chartType = "line"

  # )

  # $http.get(Routes.daily_product_types_admin_statistics_path()
  # ).success((newdata, status) ->
  #   angular.forEach newdata, (v, k) ->
  #     $scope.data3.series.push k
  #     tempData =
  #       x: k
  #       y: [v.length]
  #     $scope.data3.data.push tempData
  #   $scope.chartType3 = "pie"

  # )

  # $http.get(Routes.daily_product_sizes_admin_statistics_path()
  # ).success((newdata, status) ->
  #   angular.forEach newdata, (v, k) ->
  #     $scope.data4.series.push k
  #     tempData =
  #       x: k
  #       y: [v.length]
  #     $scope.data4.data.push tempData
  #   $scope.chartType4 = "pie"

  # )


  # $scope.data2 =
  #   series: [
  #     "Sales"
  #     "Income"
  #     "Expense"
  #     "Laptops"
  #     "Keyboards"
  #   ]
  #   data: [
  #     {
  #       x: "Sales"
  #       y: [
  #         100
  #         500
  #         0
  #       ]
  #       tooltip: "this is tooltip"
  #     }
  #     {
  #       x: "Not Sales"
  #       y: [
  #         300
  #         100
  #         100
  #       ]
  #     }
  #     {
  #       x: "Tax"
  #       y: [351]
  #     }
  #     {
  #       x: "Not Tax"
  #       y: [
  #         54
  #         0
  #         879
  #       ]
  #     }
  #   ]




  $scope.showRevenue = (stats) ->
    totalRevenue = 0
    angular.forEach stats, (v, k) ->
      totalRevenue = totalRevenue + (v.total_sold * v.profit)
    totalRevenue = totalRevenue.toFixed(2)
    return totalRevenue

  $scope.showProfit = (adcost, stats) ->
    totalProfit = 0
    totalRevenue = 0
    angular.forEach stats, (v, k) ->
      if v.total_sold isnt 0
        totalRevenue = totalRevenue + ((v.total_sold) * v.profit)

    totalProfit = totalRevenue - adcost
    totalProfit = totalProfit.toFixed(2)
    return totalProfit

  $scope.showTotalAds = () ->
    adSpend = 0
    angular.forEach $scope.products, (value, key) ->
      adSpend = adSpend + (value.ads_spend * 1)

    adSpend = adSpend.toFixed(2)
    return adSpend

  $scope.showTotalSales = () ->
    sales = 0
    angular.forEach $scope.products, (value, key) ->
      angular.forEach value.stats, (v, k) ->
        sales = sales + v.total_sold
    return sales

  $scope.showTotalRevenue = () ->
    revenue = 0
    angular.forEach $scope.products, (value, key) ->
      angular.forEach value.stats, (v, k) ->
        totalRevenue = 0
        totalRevenue = v.total_sold * v.profit
        revenue = revenue + totalRevenue
    return revenue.toFixed(2)

  $scope.showTotalProfit = () ->
    profit = 0
    angular.forEach $scope.products, (value, key) ->
      angular.forEach value.stats, (v, k) ->
        if v.total_sold isnt 0
          totalProfit = 0
          revenue = ((v.total_sold) * v.profit)
          totalProfit = revenue - value.ads_spend
          profit = profit + totalProfit

    return profit.toFixed(2)

  $scope.autoUpdate = (product) ->
    newProject = new Product
      id: product.id
      ads_spend: product.ads_spend

    success = ->
      console.log "updated na"
    failure = ->
      console.log "wala na update"
    newProject.$update({}, success, failure)
]



