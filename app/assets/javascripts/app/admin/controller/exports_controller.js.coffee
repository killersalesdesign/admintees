@ExportsNewlistCtrl = [ "$scope", "$http", "$location", "Product", "ExtraOrder", "flashr", "Category" , ($scope, $http, $location, Product, ExtraOrder, flashr, Category) ->
  NProgress.start()
  $scope.user = name: "0"
  $scope.categorySizes = []
  $scope.extraNewData = []
  $scope.showLoader = false

  total = 100
  range = []
  i = 1
  while i < total
    range.push i
    i++

  $scope.batchIDs = range
  $scope.categories = []
  $scope.products = []

  $scope.searchExports = (searchTxt) ->
    $scope.products = Product.query(search: searchTxt,
      (data) ->
        $scope.setSwtich = 2
        angular.forEach data, (pval, pkey) ->
          nCategory = []
          angular.forEach $scope.categorySizes, (cval, ckey) ->
            total = 0
            angular.forEach pval.extra_orders, (eoval, ekey) ->
              if cval.category_name is eoval.category
                switch cval.name
                  when "XS"
                    if eoval.XS isnt null and eoval.XS isnt undefined
                      total = eoval.XS
                  when "XXS"
                    if eoval.XXS isnt null and eoval.XXS isnt undefined
                      total = eoval.XXS
                  when "S"
                    if eoval.S isnt null and eoval.S isnt undefined
                      total = eoval.S
                  when "M"
                    if eoval.M isnt null and eoval.M isnt undefined
                      total = eoval.M
                  when "L"
                    if eoval.L isnt null and eoval.L isnt undefined
                      total = eoval.L
                  when "XL"
                    if eoval.XL isnt null and eoval.XL isnt undefined
                      total = eoval.XL
                  when "XXL"
                    if eoval.XXL isnt null and eoval.XXL isnt undefined
                      total = eoval.XXL
                  when "XXXL"
                    if eoval.XXXL isnt null and eoval.XXXL isnt undefined
                      total = eoval.XXXL
                  else
                    total = 0
            total = parseInt(total)
            nCategory.push {name: cval.name, category_name: cval.category_name, sold: 0, extra: total}
          $scope.extraNewData.push nCategory
    )

  $scope.categories = Category.query(
    (data) ->
      angular.forEach data, (value, key) ->
        arraySize = value.sizes.split(',')
        angular.forEach arraySize, (v, k) ->
          $scope.categorySizes.push {name: v, category_name: value.name, sold: 0, extra: 0, id: value.id}



  )

  $scope.showCountCols = (str) ->
    arrayStr = str.sizes.split(',')
    total = arrayStr.length
    console.log "counter", total
    return total
  $scope.itemCode = (website) ->
    itemCode = ''
    if website is "FanTees"
      itemCode = 'FAN0'
    else
      itemCode = 'JOB0'

  $scope.showTotalBySize = (catId, nsize, dataStats, csize) ->
    total = 0
    nsize = nsize.trim()
    keepGoingA = true

    angular.forEach dataStats, (value, key) ->
      if keepGoingA is true
        if value.category_id == catId
          keepGoingA = false
          keepGoingB = true
          angular.forEach value.sizes, (v, k) ->
            if keepGoingB is true
              if nsize == v.name.trim()
                total = v.sold
                keepGoingB = false
    return total

  $scope.showTotalHoodie = (data) ->
    total = 0
    angular.forEach data.stats, (value, key) ->
      n = value.type.search("Hoodie")
      if n isnt -1
        total = total + value.total_sold
    return total

  $scope.showTotalShirt = (data) ->
    total = 0
    angular.forEach data.stats, (value, key) ->
      n = value.type.search("Hoodie")
      if n is -1
        total = total + value.total_sold
    return total

  $scope.showTotalHoodieShirt = (data) ->
    total = 0
    angular.forEach data.stats, (value, key) ->
      total = total + value.total_sold
    return total

  $scope.showExtraTotalHoodies = (data) ->
    total = 0
    angular.forEach data, (value, key) ->
      n = value.category_name.search("Hoodie")
      if n is -1
        total += parseInt(value.extra)
    return total

  $scope.showExtraTotalShirts = (data) ->
    total = 0
    angular.forEach data, (value, key) ->
      n = value.category_name.search("Hoodie")
      if n isnt -1
        total += parseInt(value.extra)
    return total

  $scope.showTotalColumn = (type, nsize, dataStats, extra) ->
    total = 0
    nsize = nsize.trim()
    keepGoingA = true

    angular.forEach dataStats, (value, key) ->
      if keepGoingA is true
        if value.type == type
          keepGoingA = false
          keepGoingB = true
          angular.forEach value.sizes, (v, k) ->
            if keepGoingB is true
              if nsize == v.name.trim()
                total = v.sold
                keepGoingB = false

    coltotal = parseInt(total + parseInt(extra))
    return coltotal

  $scope.updateExtraData = (data, newVal, product) ->
    console.log
    newSizes = []
    angular.forEach $scope.categories, (cval, ckey) ->
      tempSizes =
        category: cval.name
        XS: 0
        XXS: 0
        S: 0
        M: 0
        L: 0
        XL: 0
        XXL: 0
        XXXL: 0
      angular.forEach data, (exval, ekey) ->
        if cval.name is exval.category_name
          switch exval.name
            when "XS"
              tempSizes.XS = parseInt(exval.extra)
            when "XXS"
              tempSizes.XXS = parseInt(exval.extra)
            when "S"
              tempSizes.S = parseInt(exval.extra)
            when "M"
              tempSizes.M = parseInt(exval.extra)
            when "L"
              tempSizes.L = parseInt(exval.extra)
            when "XL"
              tempSizes.XL = parseInt(exval.extra)
            when "XXL"
              tempSizes.XXL = parseInt(exval.extra)
            when "XXXL"
              tempSizes.XXXL = parseInt(exval.extra)
            else
      newSizes.push tempSizes






    newExstraOrder = new ExtraOrder
      id: product.extra_order_id,
      design: product.extra_order_design,
      batch_id: product.extra_order_batch_id,
      sizes: newSizes
    success = ->
      flashr.now.success('Data has been updated!')
    failure = ->
      flashr.now.error('Something went wrong!')
    newExstraOrder.$update({}, success, failure)
    return true

  $scope.exportData = ->
    blob = new Blob([document.getElementById("exportorders").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "ExportsOrders.xls"
    return

  NProgress.done()
]

@ExportsListCtrl = [ "$scope", "$http", "$location", "Product", "ExtraOrder", "flashr", "Category" , ($scope, $http, $location, Product, ExtraOrder, flashr, Category) ->
  NProgress.start()





  total = 100
  range = []
  i = 1
  while i < total
    range.push i
    i++

  $scope.batchIDs = range
  $scope.products = Product.query(print: 'strict')
  $scope.exportData = ->
    blob = new Blob([document.getElementById("newexportable").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "NewExports.xls"
    return

  $scope.showTotalBySize = (catId, nsize, dataStats, csize) ->
    total = 0
    nsize = nsize.trim()
    keepGoingA = true

    angular.forEach dataStats, (value, key) ->
      if keepGoingA is true
        if value.category_id == catId
          keepGoingA = false
          keepGoingB = true
          angular.forEach value.sizes, (v, k) ->
            if keepGoingB is true
              if nsize == v.name.trim()
                total = v.sold
                keepGoingB = false
    return total


  $scope.showTotalByCategory = (type, data) ->
    total = 0
    angular.forEach data.stats, (value, key) ->
      if value.type is type
        total = value.total_sold

    return total

  $scope.showExtraTotalByCategory = (type, data) ->
    total = 0
    angular.forEach data.extra_orders, (value, key) ->
      if value.category is type
        if value.XS isnt null and value.XS isnt undefined
          total += value.XS
        if value.XXS isnt null and value.XXS isnt undefined
          total += value.XXS
        total += value.S
        total += value.M
        total += value.L
        total += value.XL
        total += value.XXL
        total += value.XXXL

    return total

  $scope.showExtraTotalByWomensTee = (data) ->
    total = 0
    total += data.S
    total += data.M
    total += data.L
    total += data.XL
    total += data.XXL
    total += data.XXXL

    return total

  $scope.updateData = (product) ->
    newExstraOrder = new ExtraOrder
      id: product.extra_order_id,
      design: product.extra_order_design,
      batch_id: product.extra_order_batch_id,
      sizes: product.extra_orders
    success = ->
      flashr.now.success('Data has been updated!')
    failure = ->
      flashr.now.error('Something went wrong!')
    newExstraOrder.$update({}, success, failure)


  $scope.showTotalByProduct = (product) ->

  $scope.showConsole = ->
    console.log "test", $scope.filter.setProduct

  $scope.itemCode = (website) ->
    itemCode = ''
    if website is "FanTees"
      itemCode = 'FAN0'
    else
      itemCode = 'JOB0'

  $scope.setPrinted = ->
    $scope.products = Product.query(print: $scope.filter.printed)


  NProgress.done()
]



@ExportsSingleProductCtrl = [ "$scope", "$http", "$location", "Product", "ExtraOrder", "flashr", "Category" , ($scope, $http, $location, Product, ExtraOrder, flashr, Category) ->
  $scope.searchExports = (searchTxt) ->
    $scope.products = Product.query(search: searchTxt)

  $scope.showTotal = (sizes) ->
    total = 0
    angular.forEach sizes, (val, key) ->
      total += parseInt(val.sold)
    return total
]

@SingleReportProductCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", "Batch", "flashr", ($scope, $http, $location, Product, $routeParams, Batch, flashr) ->

  $scope.product = Product.get(id: $routeParams.id)
  $scope.product.batch_key = 0

  $scope.current_batch = Batch.get(product_id: $routeParams.id)
  $scope.showTotal = (sizes) ->
    total = 0
    angular.forEach sizes, (val, key) ->
      total += parseInt(val.sold)
    return total

  $scope.saveToBatch = ->
    batch = new Batch
      batch_key: $scope.current_batch.batch_key
      product_id: $scope.product.id
      batch_stats: JSON.stringify($scope.product.stats)
      brand: $scope.product.product_brand
      colour: $scope.product.product_item_color
      product_name: $scope.product.title

    success = (data)->
      $scope.current_batch = Batch.get(product_id: $routeParams.id)
      flashr.now.success('Current data has been save to batch.!')
    failure = ->
      flashr.now.error('Something went wrong!')
    batch.$save({}, success, failure)

  $scope.updateToBatch = ->
    success = ->
      flashr.now.success('Current data has been save to batch.!')
    failure = ->
      flashr.now.error('Something went wrong!')
    $scope.current_batch.$update({}, success, failure)

  $scope.showExtraTotal = (sizes) ->
    total = 0
    angular.forEach sizes, (val, key) ->
      total += parseInt(val.extra)

    total

  $scope.updateExtra = (product) ->
    NProgress.start()
    product.prices = product.stats
    $http.put(Routes.update_single_report_extra_admin_product_path($routeParams.id), product
    ).success((data, status) ->
      $scope.current_batch.batch_stats = JSON.stringify product.stats if $scope.current_batch
      NProgress.done()
    ).error (data, status) ->
      NProgress.done()
]