@OrdersListCtrl = [ "$scope", "$http", "$location", "Order", "flashr", "$routeParams", ($scope, $http, $location, Order, flashr, $routeParams) ->
  NProgress.start()

  $scope.currentPage = 1
  $scope.perPage = 100

  if $routeParams.page isnt undefined
    $scope.currentPage = parseInt($routeParams.page)

  $scope.BaseRowNum = ( $scope.currentPage - 1) * $scope.perPage

  if $routeParams.newdate isnt undefined
    $scope.newDate = moment($routeParams.newdate).format("YYYY-MM-DD")
    $scope.orders = Order.query({newdate: $scope.newDate, page: $scope.currentPage})

    $http(
      method: "GET"
      url: "/admin/orders?total=1&newdate=#{$scope.newDate}"
    ).success (data) ->
      $scope.pageButtons = Math.ceil(data / $scope.perPage)
      # $scope.front =
      $scope.pageTotalOrders = data
  else
    $scope.orders = Order.query({page: $scope.currentPage})
    $http(
      method: "GET"
      url: "/admin/orders?total=1"
    ).success (data) ->
      $scope.pageButtons = Math.ceil(data / $scope.perPage)
      $scope.pageTotalOrders = data

  $scope.getNumber = (num) ->
    new Array(num)

  $scope.show = (id) ->
    $location.path("/orders/#{id}/show")

  $scope.updateOrder = (order) ->
    success = ->
      flashr.now.success('Order has been updated!')
    failure = (data) ->
      flashr.now.error('Something went wrong!')
    order.$update({}, success, failure)

  $scope.today = ->
    $scope.dt = new Date()
    return

  $scope.today()
  $scope.showWeeks = true
  $scope.toggleWeeks = ->
    $scope.showWeeks = not $scope.showWeeks
    return

  $scope.clear = ->
    $scope.dt = null
    return

  # Disable weekend selection
  # $scope.disabled = (date, mode) ->
  #   mode is "day" and (date.getDay() is 0 or date.getDay() is 6)

  $scope.toggleMin = ->
    $scope.minDate = (if ($scope.minDate) then null else new Date())
    return

  $scope.toggleMin()
  $scope.open = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.opened = true
    return

  $scope.dateOptions =
    "year-format": "'yy'"
    "starting-day": 1

  $scope.formats = [
    "dd-MMMM-yyyy"
    "yyyy/MM/dd"
    "shortDate"
  ]
  $scope.format = $scope.formats[0]

  $scope.exportData = ->
    blob = new Blob([document.getElementById("exportable").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "Orders.xls"
    return

  $scope.exportDataSendgrid = ->
    blob = new Blob([document.getElementById("exportablesendgrid").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "OrdersSengrid.xls"
    return

  $scope.showAddress = (data) ->
    if data isnt ''
      shippingInfo = angular.fromJson(data)
      shtml = ''
      shtml += 'Name: '+ shippingInfo.Name + "<br/>"
      shtml += 'Street1: '+ shippingInfo.Street1 + "<br/>"
      shtml += 'Street2: '+ shippingInfo.Street2 + "<br/>"
      shtml += 'City: '+ shippingInfo.CityName + "<br/>"
      shtml += 'State/Province: '+ shippingInfo.StateOrProvince + "<br/>"
      shtml += 'Country: '+ shippingInfo.Country + "<br/>"
      shtml += 'Phone: '+ shippingInfo.Phone + "<br/>"
      shtml += 'Postal: '+ shippingInfo.PostalCode + "<br/>"
    return shtml

  $scope.byDate = ->
    if $scope.dt isnt undefined
      $location.path("/order/1/#{$scope.dt}")
    else
      $location.path("/orders")



  $scope.searchOrders = (k) ->
    $scope.orders = Order.query({type: 'search',k: k})
    $scope.currentPage = 1
    $http(
      method: "GET"
      url: "/admin/orders?type=search&total=1&k=#{k}"
    ).success (data) ->
      $scope.pageButtons = 0
      $scope.pageTotalOrders = data

  $scope.searchOrdersByDate = (k) ->
    $scope.orders = Order.query({type: 'search_by_date',k: k})
    $scope.currentPage = 1
    $http(
      method: "GET"
      url: "/admin/orders?type=search_by_date&total=1&k=#{k}"
    ).success (data) ->
      $scope.pageButtons = 0
      $scope.pageTotalOrders = data


  $scope.showRefund = (status) ->
    switch status
      when false then return "<span class='label label-primary'>No</span>"
      when true then return "<span class='label label-success'>Refunded</span>"
      else return "tae"

  NProgress.done()
]

@OrdersShowCtrl = [ "$scope", "$http", "$location", "Order", "$routeParams", "OrdredProduct", "flashr", "Category", "Customer", "Product", "Note", "BatchOrder", ($scope, $http, $location, Order, $routeParams, OrdredProduct, flashr, Category, Customer, Product, Note, BatchOrder) ->
  NProgress.start()
  # $scope.categories = Category.query()

  $scope.setProductSizes = (category) ->
    $scope.sizes = []
    if $scope.product.status != 0
      angular.forEach $scope.product.stocks, (stock) ->
        if stock.category_id is category.id

          angular.forEach stock.sizes, (size) ->
            if size.value > 0
              $scope.sizes.push size.label
    else
      $scope.sizes = category.sizes.replace(" ","").split(",")

  $scope.setOrderSize = () ->
    angular.forEach $scope.sizes, (size) ->
      if size.trim() is $scope.currentSize
        $scope.size = size


  $scope.editData = null
  $scope.showForm = false
  $scope.note     = ""
  $scope.order    = Order.get(id: $routeParams.id
    ,(success) ->
      $scope.order.notes = JSON.parse($scope.order.notes).reverse()

      Category.query product_id: $scope.order.product_id, (categories) ->
        # console.log 'this order: ', $scope.order
        # console.log 'categoriessss: ', categories
        $scope.categories = []
        $scope.sizes = []

        # get product stocks
        $scope.product = Product.get id: $scope.order.product_id, (product) ->

          if product.status != 0
            angular.forEach categories, (category) ->
              angular.forEach product.stocks, (stock) ->
                if stock.category_id is category.id
                  $scope.categories.push category

                  # fill sizes here
                  angular.forEach stock.sizes, (size) ->
                    if size.value > 0
                      $scope.sizes.push size.label
          else
            $scope.categories = categories
            $scope.sizes = categories[0].sizes.replace(" ","").split(",")


      $scope.shippingInfo = angular.fromJson($scope.order.shipping_info)

      $scope.customer = Customer.get(id: $scope.order.user_id)
  )

  $http(method: "GET", url: "#{Routes.show_by_order_admin_batch_orders_path()}?id=#{$routeParams.id}").success (data) ->
    $("#batch_value").val(data.batch_key) if data != null
    $scope.batch_order = data


  $scope.ordered_products = OrdredProduct.query(order_id: $routeParams.id)

  $scope.saveNote = () ->
    if $scope.note.length > 0
      note = new Note
        description: $scope.note
        order_id: $scope.order.id
        note_type: 1
      success = (e) ->
        $scope.order.notes.unshift note
        $scope.note = ""
      failure = ->
        console.log 'something went wrong while saving note'
      note.$save({}, success, failure)

  $scope.toggleForm = (flag) ->
    if flag is 1
      $scope.showForm = true
    else
      $scope.showForm = false

  $scope.updateOrder = ->
    $scope.order.shipping_info = JSON.stringify($scope.shippingInfo)

    success = ->
      $scope.shippingInfo = angular.fromJson($scope.order.shipping_info)
      flashr.now.success('Order has been updated!')

    $scope.order.$update({},success)

  $scope.showEditOrder = (orderData) ->
    $scope.currentSize = orderData.size.trim()

    $('#editModal').modal('show')
    $scope.editData = orderData

    angular.forEach $scope.categories, (value, key) ->
      if orderData.category_id is value.id
        $scope.category = value

        $scope.setProductSizes value
        $scope.setOrderSize()

    $scope.setCategory = (data) ->
      # console.log 'set category data: ', data
      $scope.category = data
      $scope.sizes = []

      $scope.setProductSizes data

      $scope.setOrderSize()


    $scope.update = (data)->
      orderedProduct = new OrdredProduct
        id: data.id,
        quantity: data.quantity
        category_id: $scope.category.id
        size: $scope.size
      success = ->
        $scope.ordered_products = OrdredProduct.query(order_id: $routeParams.id)
        $('#editModal').modal('hide')
        flashr.now.success('Ordered data has been updated!')

      failure = (object) ->
        flashr.now.error('Error data. Something went wrong!')
      orderedProduct.$update({}, success, failure)

  $scope.deleteOrder = (order) ->
    success = ->
      $('#deleteModal').modal('hide')
      flashr.now.success('Ordered data has been deleted!')
      $location.path("/orders/#{order.id}/show")
    failure = (object) ->
      flashr.now.error('Error data. Something went wrong!')
    order.$delete({}, success, failure)

  $scope.refundOrder = (order) ->
    order.refund = true
    success = ->
      $('#refundModal').modal('hide')
      flashr.now.success('Ordered data has been deleted!')
      $location.path("/orders/#{order.id}/show")
    failure = (object) ->
      flashr.now.error('Error data. Something went wrong!')
    order.$update({}, success, failure)

  $scope.goToInvoice = (id) ->
    $location.path("/invoice/#{id}/show")

  $scope.saveBatch = ->
    NProgress.start()
    batch_key = $("#batch_value").val()
    batch = new BatchOrder
      batch_key: batch_key
      order_id:  $routeParams.id

    batch.$save (batchOrder, responseHeaders) ->
      $scope.batch_order = batchOrder
      NProgress.done()

  NProgress.done()
]

@OrdersEditCtrl = [ "$scope", "$http", "$location", ($scope, $http, $location) ->
  NProgress.start()
  NProgress.done()
]

@OrdersNewCtrl = [ "$scope", "$http", "$location", "flashr", "Order", "Product", "Customer", "Website", "Category", "COUNTRIES", ($scope, $http, $location, flashr, Order, Product, Customer, Website, Category, COUNTRIES) ->
  NProgress.start()

  $scope.countries        = COUNTRIES
  $scope.country          = COUNTRIES[13]

  $scope.customer         = new Customer()
  $scope.order            = new Order()

  $scope.user_email       = ""
  $scope.user_result      = ""
  $scope.order_result     = ""
  $scope.address_street   = ""
  $scope.address_city     = ""
  $scope.address_state    = ""
  $scope.address_zip      = ""

  $scope.details          = []
  $scope.sizes            = []
  $scope.categories       = []

  $scope.typed_product    = ""
  $scope.product_list     = []

  $scope.websites = Website.query (website) ->
    $scope.selected_website = website[0]
    $scope.order.website_id = $scope.selected_website.id

  $scope.trimArrayElements = (array_param) ->
    angular.forEach array_param, (value, index) ->
      array_param[index] = value.trim()
    return array_param

  $scope.selectWebsite = (website) ->
    $scope.order.website_id = website.selected_website.id

  $scope.selectCountry = (country) ->
    $scope.country = country.country

  $scope.searchUser = (e) ->
    if e.user_email is ''
      flashr.now.error('User email is required.')
    else
      $scope.user_result = Customer.query(email: e.user_email, (res) ->
        unless typeof res[0] is 'undefined'
          # flashr.now.error('Error! User does not exist!')
        # else
          flashr.now.success('User found.')
          user = $scope.user_result[0]

          $scope.order.user_id = user.id

          $scope.customer.id = user.id
          $scope.customer.email = $scope.user_email
          $scope.customer.first_name = user.first_name
          $scope.customer.last_name = user.last_name
          $scope.customer.address = user.address
          $scope.customer.phone = user.phone
      )

  $scope.addDetails = ->
    $scope.sizes.push $scope.availableSizes
    $scope.details.push {qty: 1,size:$scope.sizes[0][0],category:$scope.categories[0]}

  $scope.removeDetail = ->
    $scope.details.pop()
    $scope.sizes.pop()

  $scope.selectCategory = (e,i) ->
    $scope.sizes[i] = e.d.category.sizes.replace(" ","").split(",")
    $scope.details[i].size = $scope.sizes[i][0]
    $scope.details[i].category = e.d.category

  $scope.selectSize = (e,i) ->
    $scope.details[i].size = e.d.size

  $scope.afterProductSelect = (product) ->
    $scope.details          = []
    $scope.sizes            = []
    $scope.categories       = []
    $scope.availableSizes   = []

    stocks                  = product.stocks
    prices                  = product.prices

    $scope.order.product_id = product.id

    Category.query product_id: product.id, (categories) ->
      angular.forEach categories, (category) ->
        availableSizes = []
        if product.status != 0 #printing or in-stock
          angular.forEach stocks, (stock) ->
            if stock.category_id is category.id and stock.amount > 0
              $scope.availableSizes.push stock.size.trim()
              $scope.categories.push category

        else #campaign
          $scope.availableSizes = $scope.trimArrayElements(category.sizes.replace(" ","").split(","))
          $scope.categories.push category

        if $scope.availableSizes.length > 0
          if $scope.sizes.length < 1
            $scope.sizes.push $scope.availableSizes
            $scope.details.push {qty: 1,size:$scope.sizes[0][0],category:category}

  $scope.save = (order, customer) ->
    $scope.order.details = $scope.details

    total_qty = 0
    angular.forEach $scope.details, (item) ->
      total_qty = parseInt(total_qty) + parseInt(item.qty)

    $scope.order.quantity = total_qty

    # set ip address to localhost to determine manually added orders
    $scope.order.ip_address = "127.0.0.1"

    # build shipping info
    $scope.order.shipping_info =
      Name: "#{$scope.customer.first_name} #{$scope.customer.last_name}"
      Street1: "#{$scope.address_street}"
      CityName: "#{$scope.address_city}"
      StateOrProvince: "#{$scope.address_state}"
      Country: $scope.country.iso
      CountryName: $scope.country.name
      Phone: "#{$scope.customer.phone}"
      PostalCode: "#{$scope.address_zip}"
      AddressID: null
      AddressOwner: 'PayPal'
      ExternalAddressID: null
      AddressStatus:'Unconfirmed'

    NProgress.start()

    saveOrder = ->
      order.$save (savedOrder, responseHeaders) ->
        flashr.now.success('Added a new order!')
        $location.path("/orders")
        NProgress.done()


    $scope.customer.email = $scope.user_email
    if typeof $scope.customer.id is 'undefined'
      customer.$save (savedCustomer, responseHeaders) ->
        $scope.order.user_id = savedCustomer.id
        flashr.now.success('Added a new customer.')

        saveOrder()
    else
      customer.$update (savedCustomer, responseHeaders) ->
        flashr.now.success('Customer information updated.')

        saveOrder()


  $ ->

    products = new Bloodhound
      datumTokenizer: (d) ->
        return Bloodhound.tokenizers.whitespace d.title
      queryTokenizer: Bloodhound.tokenizers.whitespace
      limit: 10
      remote:
        url: "/admin/products/autocomplete?query=%QUERY"

    products.initialize()

    $("#product-search").typeahead(null,{
        name: "product",
        source: products.ttAdapter(),
        displayKey: "title",
        templates:
          suggestion: Handlebars.compile('<strong>{{title}}</strong>')
      }
    ).bind("typeahead:selected", (obj, datum, name) ->
      # console.log 'datummmmmmmmmmmm: ', datum
      $scope.afterProductSelect datum
    )

  NProgress.done()
]

@OrdersEparcelCtrl = [ "$scope", "$http", "flashr", "Order", "Product", ($scope, $http, flashr, Order, Product) ->
  NProgress.start()

  $scope.order_ids  = ""
  $scope.orders     = []
  $scope.csv_data   = []
  $scope.isShift    = false

  $scope.inv_batch_id = ""
  $scope.shipit_batch_id = ""

  $scope.generateInvoices = () ->
    if $scope.inv_batch_id isnt ""
      NProgress.start()
      window.location.assign("/admin/invoices/batch/#{$scope.inv_batch_id}.pdf")

  $scope.createCsv = (data) ->
    csvContent  = ""
    filename    = "orders_eparcel.csv"
    if $scope.isShift
      filename        = "orders_eparcel_shiftIT.csv"

      currDate        = new Date()
      currDate_day    = currDate.getDate()
      currDate_month  = currDate.getMonth() + 1
      currDate_year   = currDate.getFullYear()

      if currDate_day < 10
        currDate_day = '0' + currDate_day

      if currDate_month < 10
        currDate_month = '0' + currDate_month

      today = currDate_day + '/' + currDate_month + '/' + currDate_year

    data.forEach (d) ->
      address1 = $scope.showAddress1(d.shipping_info)
      if address1.indexOf(",") > -1
        address1 = address1.replace(/,/g, ".")

      address2 = $scope.showAddress2(d.shipping_info)
      if address2.indexOf(",") > -1
        address2 = address2.replace(/,/g, ".")

      if $scope.isShift
        dataString  = "#{d.order_id},#{today},#{$scope.showName(d.shipping_info)},,#{address1},,"
        dataString += "#{$scope.showCity(d.shipping_info)},#{$scope.showPostal(d.shipping_info)},#{$scope.showState(d.shipping_info)},"
        dataString += "#{$scope.showCountry(d.shipping_info)},#{d.user_email},#{$scope.showPhone(d.shipping_info)},Apparel,"
        dataString += "#{d.order_total},,#{d.weight},#{d.order_id}\n"
      else
        if $scope.isInternational d.shipping_info
          dataString  = "C,,,AIR8,,#{$scope.showName(d.shipping_info)},,#{address1},"
          dataString += "#{address2},,,#{$scope.showCity(d.shipping_info)},"
          dataString += "#{$scope.showState(d.shipping_info)},#{$scope.showPostal(d.shipping_info)},#{$scope.showCountry(d.shipping_info)},"
          dataString += "#{$scope.showPhone(d.shipping_info)},,,,Y,,,Y,,#{d.order_id},,,,,,,,,,,,,,,,,,,,,,,,,,#{d.user_email},\n"

          dataString += "A,#{d.weight},,,,1,,,,,#{$scope.showPaid(d.shipping_info, d.order_total)},#{$scope.showSale(d.shipping_info)},,,,,,,"
          dataString += "OTHER,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n"

          dataString += "G,,,Apparel,,,1,,#{d.order_total},#{d.order_total},,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n"
        else
          dataString  = "C,,,3J35,,#{$scope.showName(d.shipping_info)},,#{address1},"
          dataString += "#{address2},,,#{$scope.showCity(d.shipping_info)},"
          dataString += "#{$scope.showState(d.shipping_info)},#{$scope.showPostal(d.shipping_info)},#{$scope.showCountry(d.shipping_info)},"
          dataString += "#{$scope.showPhone(d.shipping_info)},,,,Y,,,Y,,#{d.order_id},,,,,,,,,,,,,,,,,,,,,,,,,,#{d.user_email},TRACKADV\n"

          dataString += "A,#{d.weight},,,,1,,,,,#{$scope.showPaid(d.shipping_info, d.order_total)},#{$scope.showSale(d.shipping_info)},,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n"

      csvContent += dataString

    blob = new Blob [csvContent], {type:"text/csv;charset=utf-8;"}
    saveAs blob, filename

  $scope.generateEparcel = (byBatch = false) ->
    NProgress.start()
    $scope.orders = []
    reqUrl        = ""

    if byBatch is true and $scope.shipit_batch_id? and $scope.shipit_batch_id
      reqUrl = "/admin/orders/get_order_by_batch?batch_key=#{$scope.shipit_batch_id}"
    else
      # get all campaigns here using api call
      if $scope.order_ids? and $scope.order_ids != ""
        reqUrl = "/admin/orders/get_order_ids?order_ids=#{$scope.order_ids}"

    if reqUrl
      $http(
        method: "GET"
        url: reqUrl
      ).success (data) ->
        $scope.orders = data
        NProgress.done()
    else
      NProgress.done()

  $scope.exportParcelData = (csv_param) ->
    if csv_param is "shift"
      $scope.isShift = true
    else
      $scope.isShift = false
    $scope.createCsv $scope.orders

  $scope.isInternational = (data) ->
    signal = false
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        signal = false
      else
        signal = true
    else
      signal = false
    return signal

  $scope.showName = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Name
    return shtml

  $scope.showAddress1 = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Street1
    return shtml

  $scope.showAddress2 = (data) ->
    shippingInfo = angular.fromJson(data)
    if shippingInfo.Street2 isnt null and shippingInfo.Street2 isnt undefined
      shtml = shippingInfo.Street2
    else
      shtml = ""
    return shtml

  $scope.showCity = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.CityName
    return shtml

  $scope.showState = (data) ->
    shippingInfo = angular.fromJson(data)
    state = shippingInfo.StateOrProvince
    switch state
        when 'South Australia'
          shtml = 'SA'
        when 'Victoria'
          shtml = 'VIC'
        when 'New South Wales'
          shtml = 'NSW'
        when 'Queensland'
          shtml = 'QLD'
        when 'South Australia'
          shtml = 'SA'
        when 'Western Australia'
          shtml = 'WA'
        when 'Tasmania'
          shtml = 'TAS'
        when 'Australian Capital Territory'
          shtml = 'ACT'
        when 'Northern Territory'
          shtml = 'NT'
        else
          shtml = shippingInfo.StateOrProvince
    return shtml

  $scope.showPostal = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.PostalCode
    return shtml

  $scope.showCountry = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Country
    return shtml

  $scope.showPhone = (data) ->
    shippingInfo = angular.fromJson(data)
    if shippingInfo.Phone isnt null and shippingInfo.Phone isnt undefined
      shtml = shippingInfo.Phone
    else
      shtml = ""
    return shtml

  $scope.showPaid = (data, total) ->
    shtml = ""
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        shtml = ''
      else
        shtml = total
    return shtml

  $scope.showSale = (data) ->
    shtml = ""
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        shtml = ''
      else
        shtml = "SALE VIA INTERNET"
    return shtml

  NProgress.done()
]