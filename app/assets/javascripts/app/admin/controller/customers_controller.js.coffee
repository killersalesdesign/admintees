@CustomersListCtrl = [ "$scope", "$http", "$location", "Customer",  ($scope, $http, $location, Customer) ->
  NProgress.start()

  $scope.currentPage = 1
  $scope.numPerPage  = 100
  $scope.maxSize     = 5
  $scope.numPages    = 1
  $scope.order       = 'DESC'
  $scope.order_type  = 'id'
  $scope.orders      =
    first_name:   'ASC'
    email:        'ASC'
    address:      'ASC'
    total_orders: 'ASC'
    id:           'DESC'


  $http.get "/admin/customers/total_users"
    .success (data, status) ->
      $scope.total_users = data
      $scope.numPages    = Math.ceil data / $scope.numPerPage

      # $scope.customers = Customer.query page: $scope.currentPage, (customers) ->
      # NProgress.done()

      $scope.$watch 'currentPage + numPerPage', ->
        begin = ($scope.currentPage - 1) * $scope.numPerPage
        end   = begin + $scope.numPerPage

        NProgress.start()
        $scope.customers  = Customer.query page: $scope.currentPage, search: $scope.keyword, order: $scope.order, order_type: $scope.order_type, (customers) ->
          NProgress.done()
        return

  $scope.search = ->
    NProgress.start()

    $http.get "/admin/customers/total_users?search=#{$scope.keyword}&page=1"
      .success (data, status) ->
        $scope.total_users = data
        $scope.numPages    = Math.ceil data / $scope.numPerPage
        $scope.currentPage = 1

        $scope.customers   = Customer.query page: $scope.currentPage, search: $scope.keyword, (customers) ->
          NProgress.done()

  $scope.order_click = true
  $scope.order_by = (order_by) ->
    NProgress.start() if $scope.order_click

    $scope.order_type       = order_by
    $scope.order            = (if $scope.orders[order_by] == 'ASC' then 'DESC' else 'ASC')
    $scope.order_click      = false
    $scope.orders[order_by] = $scope.order

    $scope.customers  = Customer.query page: $scope.currentPage, search: $scope.keyword, order: $scope.order, order_type: $scope.order_type, (customers) ->
      $scope.order_click = true
      NProgress.done()

  $scope.show = (id) ->
    $location.path("/customers/#{id}/show")

]


@CustomersShowCtrl = [ "$scope", "$http", "$location", "Customer", "$routeParams", "Order", "Note", "flashr", ($scope, $http, $location, Customer, $routeParams, Order, Note, flashr) ->
  NProgress.start()

  $scope.customer = Customer.get(id: $routeParams.id)
  $scope.orders = Order.query(user_id: $routeParams.id)
  $scope.notes = Note.query(user_id: $routeParams.id)

  $scope.exportData = ->
    blob = new Blob([document.getElementById("exportable").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "CustomerOrders.xls"
    return


  $scope.saveNotes = (data) ->
    newNote = new Note
      description: data.description
      to: data.to
      user_id: $routeParams.id
    success = (note) ->
      $scope.notes.push note
      $('#newNotesModal').modal('hide')
      flashr.now.success('New note has been added!')

    failure = (object) ->
      flashr.now.error('Error data. Something went wrong!')
    newNote.$save({}, success, failure)


  $scope.editNote = (data) ->
    $scope.editNoteData = data
    $('#editNotesModal').modal('show')
    return ''

  $scope.updateNotes = (data) ->
    newNote = new Note
      description: data.description
      to: data.to
      user_id: $routeParams.id
      id: data.id
    success = ->
      $('#editNotesModal').modal('hide')
      flashr.now.success('Note data has been updated!')

    failure = (object) ->
      flashr.now.error('Error data. Something went wrong!')
    newNote.$update({}, success, failure)

  NProgress.done()

]


@CustomersNewCtrl = [ "$scope", "$http", "$location", "Customer", "flashr", ($scope, $http, $location, Customer, flashr) ->
  NProgress.start()

  $scope.customer = new Customer()

  $scope.save = (customer) ->
    NProgress.start()
    customer.$save (savedCustomer, responseHeaders) ->
      flashr.now.success('Added a new customer!')
      $location.path("/customers")
      NProgress.done()

  NProgress.done()
]
