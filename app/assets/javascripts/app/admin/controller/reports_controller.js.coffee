@CategorySoldPerTypeCtrl = [ "$scope", "$http", "$location", "$routeParams", "flashr", "Category", "OrdredProduct", ($scope, $http, $location, $routeParams, flashr, Category, OrdredProduct) ->
  NProgress.start()

  $scope.isEmptyObject = (obj) ->
    result = true
    if Object.keys(obj).length > 0
      result = false
    return result

  $scope.ordered_products = OrdredProduct.query (ordered_products) ->

    $scope.overall_sizes = []
    $scope.categorizing = []

    $scope.categories = Category.query (categories) ->
      $.each $scope.categories, (key, value) ->
        
        all_orders = $.grep $scope.ordered_products, (n, i) ->
          return n.category_id == value.id

        sizes_obj = {}
        $.each all_orders, (k, v) ->
          trim_size = $.trim(v.size)
          sizes_obj[trim_size] = if sizes_obj[trim_size] then sizes_obj[trim_size] + v.quantity else v.quantity

        unless $scope.isEmptyObject(sizes_obj)
          $scope.categorizing.push
            category: value.name
            sizes: sizes_obj

      $scope.sizes_array = []
      $scope.sold_array = []
      $scope.categorizing.forEach (category) ->
        $.each category.sizes, (key, value) ->
          $scope.sizes_array.push key
          $scope.sold_array.push value

  $scope.getSizesCount = (category) ->
    return category.sizes.split(",").length

  $scope.getSizesLength = (category) ->
    return Object.keys(category.sizes).length

  $scope.hasSizes = (category) ->
    # return Object.keys(category.sizes).length > 0
    console.log 'category has sizes: ', category
    return true

  NProgress.done()
]

@SalesOverviewCtrl = [ "$scope", "$http", ($scope, $http) ->
  NProgress.start()

  $http.get "#{Routes.sold_per_category_path()}"
  .success (data) ->
    $scope.results = data
    NProgress.done()
]