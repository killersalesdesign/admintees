# Category list
@CategoryListCtrl = [ "$scope", "$http", "$location", "Category", ($scope, $http, $location, Category) ->
  NProgress.start()
  $scope.categories = Category.query()

  $scope.show = (id) ->
    $location.path("/product_type/#{id}/edit")

  $scope.delete = (id) ->
    if confirm("Delete Product Type?")
      Category.delete id: id, (success) ->
        $scope.categories = $scope.categories.filter (item) ->
          return item if item.id isnt id

  NProgress.done()
]

# new Category
@CategoryNewCtrl = [ "$scope", "$http", "$location", "$upload", "flashr", "Category", ($scope, $http, $location, $upload, flashr, Category) ->
  NProgress.start()
  $scope.category = new Category()
  $scope.add_size = ""
  $scope.add_weight = ""

  # form variables
  $scope.form        = {}
  $scope.form.header = "Add A New Product Type"
  $scope.form.submit = "Save Product Type"

  # inizalize category
  $scope.category.sizes  = []
  $scope.category.weights= []
  $scope.category.active = true

  $scope.addSize = ->
    if $scope.category.sizes.indexOf($scope.add_size) is -1 && $scope.add_size != "" && $scope.add_weight != ""
      $scope.category.sizes.push $scope.add_size
      $scope.category.weights.push {size: $scope.add_size, weight: $scope.add_weight}

      $scope.add_size = ""
      $scope.add_weight = ""

  $scope.removeSize = (size) ->
    $scope.category.weights = $scope.category.weights.filter (w) ->
      return w if w.size != size

    $scope.category.sizes = $scope.category.sizes.filter (item) ->
      return item if item != size

  #initzalize uploaded file
  uploaded_files = []
  $scope.files   = []

  $scope.getWeight = (size) ->
    res = $scope.category.weights.filter (w) ->
      return w if w.size == size
    return res[0].weight

  $scope.onFileSelect = (file) ->
    uploaded_files.push file[0]
    $scope.files.push file[0]
    console.log file

  $scope.submit = ->
    NProgress.start()

    $scope.category.$save (data, putResponseHeaders) ->
      if data.id
        flashr.now.success('Added a new product type!')
        $location.path("/product_types")
        NProgress.done()
      else
        flashr.now.error('Error Addeding new product type!')
        NProgress.done()

  NProgress.done()
]

# Edit Category
@CategoryEditCtrl = [ "$scope", "$http", "$location", "$routeParams", "flashr", "Category", ($scope, $http, $location, $routeParams, flashr, Category) ->
  NProgress.start()
  $scope.add_size = ""
  $scope.add_weight = ""
  $scope.category = Category.get id: $routeParams.id

  # form variables
  $scope.form        = {}
  $scope.form.header = "Edit Product Type"
  $scope.form.submit = "Update Product Type"

  $scope.addSize = ->
    if $scope.category.sizes.indexOf($scope.add_size) is -1 && $scope.add_size != "" && $scope.add_weight != ""
      $scope.category.sizes.push $scope.add_size
      $scope.category.weights.push {size: $scope.add_size, weight: $scope.add_weight}

      $scope.add_size = ""
      $scope.add_weight = ""

  $scope.removeSize = (size) ->
    $scope.category.weights = $scope.category.weights.filter (w) ->
      return w if w.size != size

    $scope.category.sizes = $scope.category.sizes.filter (item) ->
      return item if item != size

  $scope.getWeight = (size) ->
    weight = 0
    res = $scope.category.weights.filter (w) ->
      return w if w.size == size
    weight = res[0].weight if res.length > 0
    return weight

  $scope.submit = ->
    NProgress.start()
    $scope.category.$update (data, putResponseHeaders) ->
      flashr.now.success('Successfully Updated!')
      NProgress.done()

  NProgress.done()
]