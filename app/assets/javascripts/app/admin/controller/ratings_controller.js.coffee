@RatingsListCtrl = [ "$scope", "$http", "$location", "Rating", ($scope, $http, $location, Rating, Website) ->
  $scope.ratings = Rating.query()
]

@RatingNewCtrl = [ "$scope", "$http", "$location", "Rating", "Website", "flashr", ($scope, $http, $location, Rating, Website, flashr) ->
  $scope.rating   = new Rating()
  $scope.title    = "Add a new Rating"
  $scope.websites = Website.query (website) ->
    $scope.website = website[0]
    $scope.rating.website_id = website[0].id

  $scope.selectWebsite = ->
    $scope.rating.website_id = $scope.website.id

  $scope.save = ->
    NProgress.start()

    $scope.rating.$save (savedrating, responseHeaders) ->
      flashr.now.success('Added a new rating!')
      $location.path("/ratings")
      NProgress.done()
]

@RatingEditCtrl = [ "$scope", "$http", "$location", "Rating", "Website", "$routeParams", "flashr", ($scope, $http, $location, Rating, Website, $routeParams, flashr) ->
  $scope.title    = "Update new Rating"
  $scope.rating   = Rating.get id: $routeParams.id, ->
    $scope.websites = Website.query (websites) ->
      $.each websites, (index, value) ->
        $scope.website = value if value.id == $scope.rating.website_id

  $scope.selectWebsite = ->
    $scope.rating.website_id = $scope.website.id

  $scope.save = ->
    NProgress.start()

    $scope.rating.$update (savedRating, responseHeaders) ->
      flashr.now.success('Rating Updated!')
      $location.path("/ratings")
      NProgress.done()
]