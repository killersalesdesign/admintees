@DesignsCtrl = [ "$scope", "$http", "$location", "$upload", "$timeout", "$routeParams", "$modal","flashr", "Category", "Product", "Website", "Brand", "ItemColor", "Design", ($scope, $http, $location, $upload, $timeout, $routeParams, $modal, flashr, Category, Product, Website, Brand, ItemColor, Design) ->
  # NProgress.start()

  canvas = new fabric.Canvas('teeCanvas')
  object = {}
  default_text = "untitled"
  default_font_size = 40
  $scope.onSelect = (website) ->
    $scope.product.website_id = website.selected_website.id

  # New Product
  $scope.product            = new Product()
  $scope.productTags        = []

  # New Design
  if $routeParams.id
    $scope.design           = Design.get id: $routeParams.id, (design)->
      canvas_js             = JSON.stringify(design.canvas)
      canvas.loadFromJSON(canvas_js)

  else
    $scope.design           = new Design()

  console.log $scope.design
  # initialize default value

  $scope.images = []

  $scope.product.print_colors = 1

  $scope.categories         = Category.query()

  $scope.websites           = Website.query (websites) ->
    $scope.selected_website = websites[0]
    $scope.product.website_id = $scope.selected_website.id

  $scope.brands             = Brand.query (brands) ->
    $scope.selected_brand   = brands[0]
    $scope.product.brand_id = $scope.selected_brand.id

  $scope.item_colors        = ItemColor.query (item_colors) ->
    $scope.selected_item_color = item_colors[0]
    $scope.product.item_color_id = $scope.selected_item_color.id

  $scope.product.categories = []
  $scope.product.active     = true
  $scope.product.show       = true
  $scope.product.date       = moment().format("YYYY-MM-DD")
  $scope.product.prices     = []
  $scope.product.price      = 0
  $scope.product.profit     = 0
  $scope.product.postage    = 0
  $scope.product.bundle     = false
  $scope.product.gift_card     = false

  # default values
  $scope.product.international_cutoff = 0
  $scope.product.additional_item_cost = 0

  # $scope.product.website_id = $scope.selected_website.id

  uploaded_files    = []

  $scope.onFileSelect = ($files) ->
    console.log $files
    $.each $files, (key, file) ->
      last_file  = uploaded_files[uploaded_files.length - 1]
      file.index = (if last_file then last_file.index + 1 else 1)
      uploaded_files.push file

      fileReader = new FileReader()
      fileReader.readAsDataURL(file)
      fileReader.onload = (e) ->
        $timeout ->
          $scope.images.push {id: null, src: e.target.result, index: file.index, type: file.type}
          # $scope.last_upload = {id: null, src: e.target.result, index: file.index, type: file.type}
          # console.log $scope.images
          imageObject = new Image();
          imageObject.src = e.target.result;
          imageObject.onload = ->
            image = new fabric.Image(imageObject);
            canvas.add(image.set().scale(0.22))
            canvas.centerObject(image)
            canvas.renderAll()


  $scope.save = (product) ->
    NProgress.start()
    product.tags = JSON.stringify($scope.productTags)
    product.$save (savedProduct, putResponseHeaders) ->
      flashr.now.success('Added a new product!')
      $location.path("/products")
      NProgress.done()
    , (response)->
      console.log response.data.errors
      alert("Some required fields are missing")
      NProgress.done()

  fabric.Canvas.prototype.getAbsoluteCoords = (object)->
    {
      left: object.left + this._offset.left
      top: object.top + this._offset.top
    }

  $scope.tee_types = [
    {id: "F4002B", type: "tshirt", name: "4002 Black Flat", src: "4002.Black.Flat.1301x1301.jpg"}
    {id: "F4002K", type: "tshirt", name: "4002 Kelly Green Flat", src: "4002.Kelly.Green.Flat.1301x1301.jpg"}
    {id: "F4002R", type: "tshirt", name: "4002 Ref Flat", src: "4002.Red.Flat.1301x1301.jpg"}
    {id: "F4007B", type: "sando", name: "4007 Black Flat", src: "4007.Black.Flat.1301x1301.jpg"}
    {id: "F4010B", type: "wvneck", name: "4010 Black Flat", src: "4010.Black.Flat.1301x1301.jpg"}
    {id: "F5002B", type: "tshirt", name: "5002 Black Flat", src: "5002.Black.Flat.1301x1301.jpg"}
    {id: "F5002K", type: "tshirt", name: "5002 Kelly Green Flat", src: "5002.Kelly.Green.Flat.1301x1301.jpg"}
    {id: "F5002R", type: "tshirt", name: "5002 Red Flat", src: "5002.Red.Flat.1301x1301.jpg"}
    {id: "F5007B", type: "sando", name: "5007 Black Flat", src: "5007.Black.Flat.1301x1301.jpg"}
    {id: "F5007N", type: "sando", name: "5007 Navy Flat", src: "5007.Navy.Flat.1301x1301.jpg"}
  ]

  $scope.touchCanvas = ->
    @object = canvas.getActiveObject()
    console.log @object?.get('type')
    if @object?.get('type') == 'i-text'
      $("input[name=title]").val(@object.getText())
      $("input[name=size]").val(@object.getFontSize())
      
      # Showing relevant controls
      $("#text-editor").removeClass("hide")
      $("button.remove").removeClass("hide")

      # Remove unecessary controls
      $("#image-editor").addClass("hide")
      $("#triggerSet").spectrum("set", @object.getFill())

    else if @object?.get('type') == 'image'
      $("#text-editor").addClass("hide")
      $("button.remove").removeClass("hide")

    else
      $("input[name=title]").val('')
      $("input[name=size]").val('')
      $("#text-editor").addClass("hide")
      $("#image-editor").addClass("hide")
      $("button.remove").addClass("hide")
    @object

  $scope.moveObjects = (obj)->
    absoluteCoords = canvas.getAbsoluteCoords(obj)

  $scope.init = (e)->
    # Setup canvas settings
    canvas.setWidth(550)
    canvas.height = 621

    # Load fonts
    $('input#fonts').fontselect
      style: 'font-select'
      placeholder: 'Select a font'
      lookahead: 2
    .change ->
      font = $(this).val().replace(/\+/g, ' ')
      canvas.getActiveObject().setFontFamily font
      canvas.renderAll()

    $("#triggerSet").spectrum
      preferredFormat: "hex"
      showPalette: true
    .change (color)->
      canvas.getActiveObject().setFill($("#triggerSet").val())
      canvas.renderAll()

    # For font resizing
    $(document).on "keydown", "input[name=size]", (e)->
      currentFontSize = canvas.getActiveObject().getFontSize()
      
      if e.keyCode == 40
        fontSize = canvas.getActiveObject().getFontSize() - 1
        canvas.getActiveObject().setFontSize(fontSize)
        $(e.currentTarget).val(fontSize)
      else if e.keyCode == 38
        fontSize = canvas.getActiveObject().getFontSize() + 1
        canvas.getActiveObject().setFontSize(fontSize)
        $(e.currentTarget).val(fontSize)
      canvas.renderAll()

    #Setup background image
    default_tee = "/assets/plains/" + $scope.tee_types[0].src
    $("#canvasBackgroundImage").css("background-image", "url("+default_tee+")");
    # fabric.Image.fromURL default_tee, (img)->
    #   # Lock background Image
    #   img.lockMovementX = true
    #   img.lockMovementY = true
    #   img.lockRotation = true
    #   img.lockScalingFlip = true
    #   img.lockScalingX = true
    #   img.lockScalingY = true
    #   img.lockUniScaling = true
    #   img.selectable = false
    #   canvas.add(img.set().scale(0.44))
    #   canvas.centerObject(img)

    # Set file upload
    # $("#fileupload").fileupload(
    #   url: "/admin/products/product_image"
    #   dataType: "json"
    #   done: (e, data) ->
    #     $.each data.result.files, (index, file) ->
    #       $("<p/>").text(file.name).appendTo "#files"
    #       # $scope.addImage
    #       console.log file
    #       return

    #     return

    #   progressall: (e, data) ->
    #     progress = parseInt(data.loaded / data.total * 100, 10)
    #     $("#progress .progress-bar").css "width", progress + "%"
    #     return
    # ).prop("disabled", not $.support.fileInput).parent().addClass (if $.support.fileInput then `undefined` else "disabled")

  $scope.addText = ->
    $("#text-editor").removeClass("hide")
    $("#image-editor").addClass("hide")
    $("button.remove").removeClass("hide")
    text = new fabric.IText default_text,
      fill: '#FFFFFF'

    text.set
      left: (canvas.getWidth() / 2) - (text.getWidth() / 2)
      top: 200
    text.setCoords()

    $("input[name=title]").val(default_text)
    $("input[name=size]").val(default_font_size)

    canvas.add text
    canvas.setActiveObject(text)

  $scope.removeObject = ->
    $("#image-editor").addClass("hide")
    $("#text-editor").addClass("hide")
    $("button.remove").addClass("hide")
    canvas.getActiveObject()?.remove()

  $scope.changeText = (value)->
    canvas.getActiveObject().setText(value)
    canvas.renderAll()

  $scope.changeSize = (value)->
    canvas.getActiveObject().setFontSize(value)
    canvas.renderAll()

  $scope.changeColor = (value)->
    canvas.getActiveObject().setFill(value)
    canvas.renderAll()

  $scope.changeShirt = (e)->
    tee = "/assets/plains/" + e.src;
    $("#canvasBackgroundImage").css("background-image", "url(" + tee + ")");
    true

  $scope.addImage = ->
    $("#image-editor").removeClass("hide")
    $("#text-editor").addClass("hide")
    $("button.remove").removeClass("hide")
    true

  $scope.uploadImage = (src)->
    fabric.Image.fromURL src, (img)->
      canvas.add img.set
        left: (canvas.getWidth() / 2) - (text.getWidth() / 2)
        top: 200

  $scope.showProductInfoPage = ->
    NProgress.start()
    $scope.design.canvas = JSON.stringify(canvas)
    $scope.design.$save (savedDesign, putResponseHeaders) ->
      # $location.path("/designs/new/" + savedDesign.id)
      $("section#product_designer").addClass("hide")
      $("section#product_info").removeClass("hide")
      NProgress.done()
    , (response)->
      NProgress.done()

  $scope.init()
]
