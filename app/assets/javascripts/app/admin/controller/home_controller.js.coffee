@HomeController = [ "$scope", "$http", "$location", "Category", ($scope, $http, $location, Category) ->
  NProgress.start()
  $scope.categories = Category.query()

  $scope.teesCounter = $http.get(Routes.tees_counts_admin_statistics_path())
  $scope.date_from  = moment().format("YYYY-MM-DD")
  $scope.date_to    = moment().format("YYYY-MM-DD")
  $scope.total_amount = 0.0
  $scope.total_sold = 0

  $scope.get_product_stat = () ->
    NProgress.start()
    $http.get(Routes.show_sold_stats_admin_products_path() + "?date_from=#{$scope.date_from}&date_to=#{$scope.date_to}")
      .success (data, status)->
        console.log(data)
        $scope.total_amount = data.total_amount
        $scope.total_sold = data.total_sold
        NProgress.done()

  NProgress.done()

  
]
