# Coupon index
@CouponsListCtrl = [ "$scope", "$http", "$location", "Coupon", "Website", ($scope, $http, $location, Coupon, Website) ->
  NProgress.start()
  $scope.coupons = Coupon.query (coupons) ->
    NProgress.done()

  $scope.show = (id) ->
    console.log id
    $location.path("/coupons/#{id}/edit")

  $scope.delete = (id) ->
    if confirm("Delete Coupon?")
      Coupon.delete id: id, (success) ->
        $scope.coupons = $scope.coupons.filter (item) ->
          return item if item.id isnt id
]

# Coupon new
@CouponsNewCtrl = [ "$scope", "$http", "$location", "Coupon", "Website", "flashr", ($scope, $http, $location, Coupon, Website, flashr) ->
  NProgress.start()

  $scope.coupon        = new Coupon()
  $scope.coupon.active = true

  # form variables
  $scope.form =
    header: "Add A New Coupon"
    submit: "Save Coupon"

  $scope.submit = ->
    NProgress.start()

    $scope.coupon.$save (data, putResponseHeaders) ->
      if data.id
        flashr.now.success('Added a new coupon!')
        $location.path("/coupons")
        NProgress.done()
      else
        flashr.now.error('Error Addeding new coupon!')
        NProgress.done()

  NProgress.done()
]

# Coupon new
@CouponsEditCtrl = [ "$scope", "$http", "$location", "$routeParams", "Coupon", "Website", "flashr", ($scope, $http, $location, $routeParams, Coupon, Website, flashr) ->
  NProgress.start()

  $scope.coupon = Coupon.get id: $routeParams.id, (coupon) ->
    $scope.coupon.expiration = moment(coupon.expiration).format("YYYY-MM-DD")

  # form variables
  $scope.form =
    header: "Edit Coupon"
    submit: "Update Coupon"

  $scope.submit = ->
    NProgress.start()
    $scope.coupon.$update (data, putResponseHeaders) ->
      $scope.coupon.expiration = moment(data.expiration).format("YYYY-MM-DD")
      flashr.now.success('Successfully Updated!')
      NProgress.done()

  NProgress.done()
]