@InstockInvoiceCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", "Order", ($scope, $http, $location, Product, $routeParams, Order) ->
  NProgress.start()
  $scope.orders = []
  $http.get Routes.in_stocks_admin_orders_path()
  .success (data, status) ->
    $scope.orders = data
    NProgress.done()

  $scope.root_path = Routes.root_path()
  $scope.showAddress = (data, product) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Name + "<br/>"
    shtml += shippingInfo.Street1 + "<br/>"
    if shippingInfo.Street2 isnt null and shippingInfo.Street2 isnt undefined
      shtml += shippingInfo.Street2 + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.CityName isnt null and shippingInfo.CityName isnt undefined
      shtml += shippingInfo.CityName + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.StateOrProvince isnt null and shippingInfo.StateOrProvince isnt undefined
      shtml += shippingInfo.StateOrProvince + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.Country isnt null and shippingInfo.Country isnt undefined
      shtml += shippingInfo.Country + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.Phone isnt null and shippingInfo.Phone isnt undefined
      shtml += shippingInfo.Phone + "<br/>"
    else
      shtml += "<br/>"
    if shippingInfo.PostalCode isnt null and shippingInfo.PostalCode isnt undefined
      shtml += shippingInfo.PostalCode + "<br/>"
    else
      shtml += "<br/>"
    shtml += "<b>"+product+"</b>"
    return shtml

  $scope.range = ->
    n = Math.ceil($scope.orders.length / 100)
    new Array(n)

  $scope.showStatus = (status) ->
    switch status
      when 0 then return "<span class='label label-primary'>Campaign</span>"
      when 1 then return "<span class='label label-success'>Printing</span>"
      when 2 then return "<span class='label label-warning'>In Stock</span>"
      else return ""


]


@InstockEparcelCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", "Order", ($scope, $http, $location, Product, $routeParams, Order) ->
  NProgress.start()

  $http.get Routes.in_eparcel_admin_orders_path()
  .success (data, status) ->
    $scope.orders = data
    NProgress.done()

  $scope.showAddress1 = (data) ->
    shtml = "null"
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      shtml = shippingInfo.Street1
    return shtml

  $scope.showAddress2 = (data) ->
    shippingInfo = angular.fromJson(data)

    if shippingInfo.Street2 isnt null and shippingInfo.Street2 isnt undefined
      shtml = shippingInfo.Street2 + "<br/>"
    else
      shtml = ""
    return shtml

  $scope.showName = (data) ->
    shtml = "null"
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      shtml = shippingInfo.Name
    return shtml

  $scope.showPhone = (data) ->
    shippingInfo = angular.fromJson(data)
    if shippingInfo.Phone isnt null and shippingInfo.Phone isnt undefined
      shtml = shippingInfo.Phone
    else
      shtml = ""
    return shtml

  $scope.showCity = (data) ->
    shtml = "null"
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      shtml = shippingInfo.CityName
    return shtml

  $scope.showState = (data) ->
    shtml = "null"
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      state = shippingInfo.StateOrProvince
      switch state
        when 'South Australia'
          shtml = 'SA'
        when 'Victoria'
          shtml = 'VIC'
        when 'New South Wales'
          shtml = 'NSW'
        when 'Queensland'
          shtml = 'QLD'
        when 'South Australia'
          shtml = 'SA'
        when 'Western Australia'
          shtml = 'WA'
        when 'Tasmania'
          shtml = 'TAS'
        when 'Australian Capital Territory'
          shtml = 'ACT'
        when 'Northern Territory'
          shtml = 'NT'
        else
          shtml = shippingInfo.StateOrProvince
    return shtml

  $scope.showPostal = (data) ->
    shtml = "null"
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      shtml = shippingInfo.PostalCode
    return shtml

  $scope.showCountry = (data) ->
    shtml = "null"
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      shtml = shippingInfo.Country
    return shtml

  $scope.showPaid = (data, total) ->
    shtml = ""
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        shtml = ''
      else
        shtml = total
    return shtml


  $scope.showSale = (data) ->
    shtml = ""
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        shtml = ''
      else
        shtml = "SALE VIA INTERNET"
    return shtml


  $scope.exportParcelData = ->
    blob = new Blob([document.getElementById("ineparceltable").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "InEparcelExports.xls"
    return

  $scope.isInternational = (data) ->
    signal = false
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        signal = false
      else
        signal = true
    else
      signal = false
    return signal


]

