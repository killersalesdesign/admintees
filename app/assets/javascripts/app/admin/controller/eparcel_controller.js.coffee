@EparcelShowCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", "Order", ($scope, $http, $location, Product, $routeParams, Order) ->
  $scope.loading = true
  NProgress.start()

  console.log "naa bai?"
  $scope.product = Product.get(id: $routeParams.pid)
  # $scope.orders = Order.query(product_id: $routeParams.pid, shipped:1)

  $http.get Routes.admin_eparcel_index_path(product_id: $routeParams.pid)
  .success (data, status) ->
    $scope.orders = data
    $scope.loading = false
  $scope.products = Product.query(print: 'all')

  $scope.showAddress1 = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Street1
    return shtml

  $scope.showAddress2 = (data) ->
    shippingInfo = angular.fromJson(data)

    if shippingInfo.Street2 isnt null and shippingInfo.Street2 isnt undefined
      shtml = shippingInfo.Street2
    else
      shtml = ""
    return shtml

  $scope.showPhone = (data) ->
    shippingInfo = angular.fromJson(data)
    if shippingInfo.Phone isnt null and shippingInfo.Phone isnt undefined
      shtml = shippingInfo.Phone
    else
      shtml = ""
    return shtml

  $scope.showName = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.Name
    return shtml

  $scope.showCity = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.CityName
    return shtml

  $scope.showState = (data) ->
    shippingInfo = angular.fromJson(data)
    state = shippingInfo.StateOrProvince
    switch state
        when 'South Australia'
          shtml = 'SA'
        when 'Victoria'
          shtml = 'VIC'
        when 'New South Wales'
          shtml = 'NSW'
        when 'Queensland'
          shtml = 'QLD'
        when 'South Australia'
          shtml = 'SA'
        when 'Western Australia'
          shtml = 'WA'
        when 'Tasmania'
          shtml = 'TAS'
        when 'Australian Capital Territory'
          shtml = 'ACT'
        when 'Northern Territory'
          shtml = 'NT'
        else
          shtml = shippingInfo.StateOrProvince
    return shtml

  $scope.showPostal = (data) ->
    shippingInfo = angular.fromJson(data)
    shtml = shippingInfo.PostalCode
    return shtml

  $scope.showCountry = (data) ->
    shippingInfo = angular.fromJson(data)

    shtml = shippingInfo.Country
    return shtml

  $scope.showPaid = (data, total) ->
    shtml = ""
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        shtml = ''
      else
        shtml = total
    return shtml


  $scope.showSale = (data) ->
    shtml = ""
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        shtml = ''
      else
        shtml = "SALE VIA INTERNET"
    return shtml

  $scope.isInternational = (data) ->
    signal = false
    unless data == "null"
      shippingInfo = angular.fromJson(data)
      if shippingInfo.Country is "AU" || shippingInfo.Country is "Australia"
        signal = false
      else
        signal = true
    else
      signal = false
    return signal

  # $scope.showPaid = (data, order) ->
  #   shippingInfo = angular.fromJson(data)
  #   if shippingInfo.Country is "AU"
  #     shtml = shippingInfo.Country
  #   else
  #     shtml = order
  #   shtml = shippingInfo.Country
  #   return shtml

  $scope.gotoProduct = ->
    console.log $scope.filter.setProduct
    $location.path("/eparcel/#{$scope.filter.setProduct}")

  $scope.exportParcelData = ->
    blob = new Blob([document.getElementById("eparcelexportable").innerHTML],
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    )
    saveAs blob, "EparcelExports.xls"
    return

  $scope.showStatus = (status) ->
    switch status
      when 0 then return "<span class='label label-primary'>Campaign</span>"
      when 1 then return "<span class='label label-success'>Printing</span>"
      when 2 then return "<span class='label label-warning'>In Stock</span>"
      else return ""

  NProgress.done()
]
