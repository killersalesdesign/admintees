# Coupon index
@GiftcardsListCtrl = [ "$scope", "$http", "$location", "Giftcard", "Website", ($scope, $http, $location, Giftcard, Website) ->
  NProgress.start()
  $scope.giftcards = Giftcard.query (giftcards) ->
    NProgress.done()

  $scope.show = (id) ->
    console.log id
    $location.path("/giftcards/#{id}/edit")

  $scope.delete = (id) ->
    if confirm("Delete Coupon?")
      Coupon.delete id: id, (success) ->
        $scope.giftcards = $scope.giftcards.filter (item) ->
          return item if item.id isnt id
]

