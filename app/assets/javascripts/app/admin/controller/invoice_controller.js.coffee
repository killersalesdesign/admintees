@InvoiceShowCtrl = [ "$scope", "$http", "$location", "Product", "$routeParams", "Order", "OrdredProduct", ($scope, $http, $location, Product, $routeParams, Order, OrdredProduct) ->
  NProgress.start()
  $scope.order = Order.get(id: $routeParams.id
    ,(success) ->
      # console.log $scope.order
      $scope.shippingInfo = angular.fromJson($scope.order.shipping_info)
      # console.log "shipping", $scope.shippingInfo
  )
  $scope.ordered_products = OrdredProduct.query(order_id: $routeParams.id)

  $scope.subTotalExcGST = ->
    subTotal = 0
    angular.forEach $scope.ordered_products, (value, key) ->
      subTotal += value.quantity * (value.product_price - (value.product_price / 11))

    return subTotal

  $scope.subTotalGSTAmount = ->
    subGST = 0
    angular.forEach $scope.ordered_products, (value, key) ->
      subGST += (value.product_price / 11) * value.quantity

    return subGST

  $scope.subTotalAmountPayable = ->
    amountPayable = 0
    angular.forEach $scope.ordered_products, (value, key) ->
      amountPayable += value.product_price * value.quantity

    return amountPayable


  $scope.showSizes = ->
    itemSizes = ''
    angular.forEach $scope.ordered_products, (value, key) ->
      itemSizes += value.category_name + '(' + value.size + ') '

    return itemSizes

  $scope.showWebName = (website)->
    itemID = ''
    if website is 'FanTees'
      itemID = 'FAN0'
    else
      itemID = 'JOB0'

    return itemID



  NProgress.done()
]
