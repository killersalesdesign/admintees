App.factory "ExtraOrder", [ "$resource", ($resource) ->
  ExtraOrder = $resource "#{Routes.admin_extra_orders_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return ExtraOrder
]