App.factory "Coupon", [ "$resource", ($resource) ->
  Coupon = $resource "#{Routes.admin_coupons_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Coupon
]