App.factory "Customer", [ "$resource", ($resource) ->
  Customer = $resource "#{Routes.admin_customers_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Customer
]