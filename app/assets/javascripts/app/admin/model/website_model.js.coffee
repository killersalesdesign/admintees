App.factory "Website", [ "$resource", ($resource) ->
  Website = $resource "#{Routes.admin_website_index_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Website
]