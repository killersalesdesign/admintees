App.factory "ItemColor", [ "$resource", ($resource) ->
  ItemColor = $resource "#{Routes.admin_item_color_index_path()}/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}
  return ItemColor
]