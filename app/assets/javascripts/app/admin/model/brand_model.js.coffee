App.factory "Brand", [ "$resource", ($resource) ->
  Brand = $resource "#{Routes.admin_brand_index_path()}/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}
  return Brand
]