App.factory "Abandonment", [ "$resource", ($resource) ->
  Abandonment = $resource "#{Routes.admin_abandonment_index_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Abandonment
]