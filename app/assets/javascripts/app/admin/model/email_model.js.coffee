App.factory "Email", [ "$resource", ($resource) ->
  Email = $resource "#{Routes.admin_emails_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}

  return Email
]