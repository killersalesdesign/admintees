App.factory "Design", [ "$resource", ($resource) ->
  Design = $resource "#{Routes.admin_designs_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Design
]