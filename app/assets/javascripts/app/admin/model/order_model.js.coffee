App.factory "Order", [ "$resource", ($resource) ->
  Order = $resource "#{Routes.admin_orders_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}

  Order::isPrinted = ->
    if @printed is true
      return "Yes"
    else return "No"

  Order::isShipped = ->
    if @shipped is 1
      return "Yes"
    else if @shipped is 2
      return "Pending"
    else
      return "No"



  Order::showMethod = ->
    if @ip_address is "127.0.0.1"
      return "Manual"
    else if @payer_id is null || @payer_id is ""
      return "Credit Card"
    else
      return "Paypal"


  return Order
]