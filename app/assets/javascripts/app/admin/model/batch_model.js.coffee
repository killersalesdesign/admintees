App.factory "Batch", [ "$resource", ($resource) ->
  Batch = $resource "#{Routes.admin_batches_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Batch
]