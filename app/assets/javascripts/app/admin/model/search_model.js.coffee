App.factory "Search", [ "$resource", ($resource) ->
  Search = $resource "#{Routes.admin_searches_path()}"
  return Search
]