App.factory "Rating", [ "$resource", ($resource) ->
  Rating = $resource "#{Routes.admin_ratings_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Rating
]