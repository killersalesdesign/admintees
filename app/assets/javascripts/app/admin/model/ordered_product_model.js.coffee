App.factory "OrdredProduct", [ "$resource", ($resource) ->
  OrdredProduct = $resource "#{Routes.admin_ordered_products_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return OrdredProduct
]