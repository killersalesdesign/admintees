App.factory "BatchOrder", [ "$resource", ($resource) ->
  Batch = $resource "#{Routes.admin_batch_orders_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Batch
]