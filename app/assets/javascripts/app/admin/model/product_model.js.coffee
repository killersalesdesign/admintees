App.factory "Product", [ "$resource", ($resource) ->
  Product = $resource "#{Routes.admin_products_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Product
]