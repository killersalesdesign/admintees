App.factory "Giftcard", [ "$resource", ($resource) ->
  Giftcard = $resource "#{Routes.admin_giftcards_path()}/:id",
                      {id: "@id"}
                      {update: {method: "PUT"}}


  return Giftcard
]