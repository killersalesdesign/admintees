App.factory "Category", [ "$resource", ($resource) ->
  Category = $resource "#{Routes.admin_category_index_path()}/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}

  return Category
]