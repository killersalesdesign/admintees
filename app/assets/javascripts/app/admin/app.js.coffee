'use strict';
@app_name = $("html").attr("ng-app")
@App      = angular.module @app_name, [
  "ngResource"
  "ng-rails-csrf"
  "ngRoute"
  "ui.bootstrap"
  "angularFileUpload"
  "flashr"
  "textAngular"
  "toggle-switch"
  "md5"
  "ngSanitize"
  "angularCharts"
  "xeditable"
  "ngTagsInput"
  "ui.bootstrap"
  "angucomplete-alt"
]
@App.helper = {}
# @App.run (editableOptions) ->
#   editableOptions.theme = "bs3" # bootstrap3 theme. Can be also 'bs2', 'default'
#   return

