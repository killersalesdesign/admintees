App.config [ "$routeProvider", "$locationProvider", "$httpProvider", ($routeProvider, $locationProvider, $httpProvider) ->
  # $locationProvider.html5Mode(true)

  $httpProvider.defaults.transformRequest.push (data, headersGetter) ->
    utf8_data = data
    unless angular.isUndefined(data)
      d = angular.fromJson(data)
      d["utf8"] = "&#9731;"
      utf8_data = angular.toJson(d)
    utf8_data

  $routeProvider
  .when "/home",
    template: JST["app/admin/views/home/index"]
    controller: "HomeController"

  .when "/searches",
    template: JST["app/admin/views/searches/index"]
    controller: "SearchListCtrl"

  .when "/products",
    template: JST["app/admin/views/products/index"]
    controller: "ProductsListCtrl"

  .when "/product/new",
    template: JST["app/admin/views/products/new"]
    controller: "ProductsNewCtrl"

  .when "/product/:id/edit",
    template: JST["app/admin/views/products/edit"]
    controller: "ProductsEditCtrl"

  .when "/product/:id/stats",
    template: JST["app/admin/views/products/stats"]
    controller: "ProductsStatsCtrl"

  .when "/product/:id/mailing",
    template: JST["app/admin/views/products/mailing"]
    controller: "ProductsMailingCtrl"

  # Product Designer
  .when "/designs/new",
    template: JST["app/admin/views/designs/new"]
    controller: "DesignsCtrl"

  .when "/v2/orders",
    template: JST["app/admin/views/v2/orders/index"]
    controller: "OrdersV2ListCtrl"

  .when "/orders",
    template: JST["app/admin/views/orders/index"]
    controller: "OrdersListCtrl"

  .when "/order/new",
    template: JST["app/admin/views/orders/new"]
    controller: "OrdersNewCtrl"

  .when "/orders/eparcel",
    template: JST["app/admin/views/orders/eparcel"]
    controller: "OrdersEparcelCtrl"

  .when "/order/:page",
    template: JST["app/admin/views/orders/index"]
    controller: "OrdersListCtrl"

  .when "/order/:page/:newdate",
    template: JST["app/admin/views/orders/index"]
    controller: "OrdersListCtrl"


  .when "/orders/:id/show",
    template: JST["app/admin/views/orders/show"]
    controller: "OrdersShowCtrl"

  .when "/invoice/:id/show",
    template: JST["app/admin/views/invoice/show"]
    controller: "InvoiceShowCtrl"

  .when "/orders/:id/edit",
    template: JST["app/admin/views/orders/edit"]
    controller: "OrdersEditCtrl"

  .when "/orders/new",
    template: JST["app/admin/views/orders/edit"]
    controller: "OrdersNewCtrl"

  .when "/product_types",
    template: JST["app/admin/views/categories/index"]
    controller: "CategoryListCtrl"

  .when "/product_types/new",
    template: JST["app/admin/views/categories/form"]
    controller: "CategoryNewCtrl"

  .when "/product_type/:id/edit",
    template: JST["app/admin/views/categories/form"]
    controller: "CategoryEditCtrl"

  .when "/reports/sold_per_type",
    template: JST["app/admin/views/reports/sold_per_type"]
    controller: "CategorySoldPerTypeCtrl"

  .when "/reports/category",
    template: JST["app/admin/views/reports/category"]
    controller: "ReportCategoryCtrl"

  .when "/statistics",
    template: JST["app/admin/views/statistics/index"]
    controller: "StatisticsListCtrl"

  .when "/customers",
    template: JST["app/admin/views/customers/index"]
    controller: "CustomersListCtrl"

  .when "/customer/new",
    template: JST["app/admin/views/customers/new"]
    controller: "CustomersNewCtrl"

  .when "/customers/:id/show",
    template: JST["app/admin/views/customers/show"]
    controller: "CustomersShowCtrl"

  .when "/customers/new",
    template: JST["app/admin/views/customers/new"]
    controller: "CustomersNewCtrl"

  .when "/exports",
    template: JST["app/admin/views/exports/index"]
    controller: "ExportsListCtrl"

  .when "/addresslabels/:pid",
    template: JST["app/admin/views/addresslabels/show"]
    controller: "AddressLabelCtrl"

  .when "/eparcel/:pid",
    template: JST["app/admin/views/eparcel/show"]
    controller: "EparcelShowCtrl"

  .when "/instock/invoice",
    template: JST["app/admin/views/instock/invoice"]
    controller: "InstockInvoiceCtrl"

  .when "/instock/eparcel",
    template: JST["app/admin/views/instock/eparcel"]
    controller: "InstockEparcelCtrl"

  .when "/newexports",
    template: JST["app/admin/views/exports/allnew"]
    controller: "ExportsNewlistCtrl"

  .when "/coupons",
    template: JST["app/admin/views/coupons/index"]
    controller: "CouponsListCtrl"

  .when "/coupons/new",
    template: JST["app/admin/views/coupons/form"]
    controller: "CouponsNewCtrl"

  .when "/coupons/:id/edit",
    template: JST["app/admin/views/coupons/form"]
    controller: "CouponsEditCtrl"

  .when "/giftcards",
    template: JST["app/admin/views/giftcards/index"]
    controller: "GiftcardsListCtrl"

  .when "/singlexports",
    template: JST["app/admin/views/exports/single"]
    controller: "ExportsSingleProductCtrl"

  .when "/singlereport/:id",
    template: JST["app/admin/views/exports/singlereport"]
    controller: "SingleReportProductCtrl"

  .when "/batches",
    template: JST["app/admin/views/batches/index"]
    controller: "BatchListCtrl"

  .when "/batch/:id",
    template: JST["app/admin/views/batches/show"]
    controller: "BatchShowCtrl"

  .when "/ratings",
    template: JST["app/admin/views/ratings/index"]
    controller: "RatingsListCtrl"

  .when "/ratings/new",
    template: JST["app/admin/views/ratings/form"]
    controller: "RatingNewCtrl"

  .when "/ratings/:id/edit",
    template: JST["app/admin/views/ratings/form"]
    controller: "RatingEditCtrl"

  .when "/abandonment",
    template: JST["app/admin/views/abandonment/index"]
    controller: "AbandonmentCtrl"

  .when "/report/sales-overview",
    template: JST["app/admin/views/reports/sales_overview"]
    controller: "SalesOverviewCtrl"

  .otherwise redirectTo: '/home'
]