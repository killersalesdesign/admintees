angular.module(@app_name).filter "listfilter", [->
  (items, searchText) ->
    filtered = []
    searchText = searchText.toLowerCase()
    angular.forEach items, (item) ->
      filtered.push item  if item.label.toLowerCase().indexOf(searchText) >= 0
      return

    filtered
]