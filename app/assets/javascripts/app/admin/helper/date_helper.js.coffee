angular.module(@app_name).filter "fromNow", ->
  (date) ->
    moment(date).fromNow()

angular.module(@app_name).filter "niceDate", ->
  (date) ->
    if date
      moment(date).format("MMM Do YYYY")
    else
      null

angular.module(@app_name).filter "niceDateTime", ->
  (date) ->
    moment(date).format("MMM DD, YYYY h:mm:ss a")