App.filter "customCurrency", [
  "$filter"
  ($filter) ->
    return (amount, currencySymbol) ->
      currency = $filter("currency")
      return currency(amount, currencySymbol).replace("(", "-").replace(")", "")  if amount.charAt(0) is "-"
      currency amount, currencySymbol
]