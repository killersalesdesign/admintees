@HeaderController = [ "$scope", "$http", "$location", ($scope, $http, $location) ->
  $scope.user = gon.current_user

  $scope.isActive = (viewLocation) ->
    if viewLocation is $location.path()
      return "active"
]