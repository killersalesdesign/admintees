@ProfileCtrl = [ "$scope", "$http", "$location", "flashr", "User", ($scope, $http, $location, flashr, User) ->
  NProgress.start()
  $scope.user = User.get id: gon.current_user.id

  $scope.submit = () ->
    NProgress.start()
    $scope.user.$update (data, putResponseHeaders) ->
      flashr.now.success('Successfully Updated!')
      NProgress.done()

  $scope.onFileSelect = ($files) ->
    fd = new FormData()
    fd.append "avatar", $files[0]
    $http.put(Routes.dashboard_user_path($scope.user.id), fd,
      withCredentials: true
      headers:
        "Content-Type": `undefined`
      transformRequest: angular.identity
    ).success((data, status) ->
      $scope.user = new User(data)
      flashr.now.success('Avatar updated!')
    ).error (data, status) ->
      flashr.now.error(data.errors)

  NProgress.done()
]

@ProfilePasswordCtrl = [ "$scope", "$http", "$location", "flashr", "User", ($scope, $http, $location, flashr, User) ->
  NProgress.start()
  $scope.user = User.get id: gon.current_user.id

  $scope.submit = () ->
    if $scope.user.password is $scope.user.password_confirmation
      $scope.user.$update()
      flashr.now.success('Successfully Updated!')

      setTimeout(->
        window.location = "/dashboard"
      , 1000)
    else
      flashr.now.error('Password does not match!')

  NProgress.done()
]