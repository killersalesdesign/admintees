class SendgridController < ApplicationController
  def index
    if params[:pid].present?
      @user_ids = Order.where(:product_id => params[:pid]).collect(&:user_id)
      @users = User.where(:id => @user_ids )

      @export = CSV.generate do |csv|
        csv << [
          'Email',
          'First Name',
          'Last Name'
        ]

        @users.each do |record|
          csv << [
            record.email,
            record.first_name.capitalize,
            record.last_name.capitalize
          ]
        end
      end



    end

    respond_to do |format|
      format.csv { send_data @export, :type => 'text/csv; charset=utf-8; header=present', :disposition => 'attachment: filename=export.csv' }
    end

  end

  def all
    @users = User.all()
    @export = CSV.generate do |csv|
      csv << [
        'Email',
        'First Name',
        'Last Name'
      ]

      @users.each do |record|
        csv << [
          record.email,
          record.first_name.capitalize,
          record.last_name.capitalize
        ]
      end
    end

    respond_to do |format|
      format.csv { send_data @export, :type => 'text/csv; charset=utf-8; header=present', :disposition => 'attachment: filename=allusers.csv' }
    end
  end
end
