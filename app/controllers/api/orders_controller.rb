class Api::OrdersController < Api::BaseController
  layout "pdfview"

  def index
    render :json => true
  end

  def reports
    limit = params[:limit] || 50
    @website = Website.find(1)
    # orders   = Order.select("user_id, COUNT(user_id) as order_count").where(:website_id => 1).group("user_id HAVING count(user_id) >= 2")
    orders   = Order.select("user_id, COUNT(user_id) as order_count").where(:website_id => 1, :refund => false).having("COUNT(user_id) >= 2").group("user_id")
    @orders  = orders.order("id DESC").page(params[:page]).per(limit)
    @total   = orders.length
  end

  def sales
    limit = params[:limit] || 50
    op    = OrderedProduct.select("SUM(quantity) as total_quantity, product_id").group("product_id").order("total_quantity desc")
    @op   = op.page(params[:page]).per(limit)
  end
end