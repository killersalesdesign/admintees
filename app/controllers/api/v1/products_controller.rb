class Api::V1::ProductsController < Api::V1::BaseController
  def index
    limit   = Rails.env.production? ? 100 : 10
    if params[:last_id].present?
      @products = Product.where('id > ?', params[:last_id]).page(params[:page]).per(limit)
    elsif params[:updated_at].present?
      @products = Product.where('updated_at > created_at', params[:last_id]).order(:id => :asc).page(params[:page]).per(limit)

      @products.each do |order|
        order.updated_at = order.created_at
        order.save
      end
    else
      @products = Product.page(params[:page]).per(limit)
    end
  end

  def show
    render :json => :show
  end
end