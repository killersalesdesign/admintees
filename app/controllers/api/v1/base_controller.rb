require 'digest/md5'

class Api::V1::BaseController < ApplicationController
  # TOKEN = Digest::MD5.hexdigest "admin:adm1nt33s:#{Time.now.in_time_zone('Brisbane').beginning_of_hour.to_i}"
  # TOKEN = Base64.urlsafe_encode64("admin:adm1nt33s:#{Time.now.in_time_zone('Brisbane').beginning_of_hour.to_i}")

  before_filter :authenticate

  private

  def authenticate
    md5_token = Digest::MD5.hexdigest("#{ENV['API_TOKEN_USERNAME']}:#{ENV['API_TOKEN_PASSWORD']}:#{Time.now.in_time_zone('Brisbane').beginning_of_hour.to_i}")
    puts "TOKEN: #{md5_token}"
    authenticate_or_request_with_http_token do |token, options|
      token == md5_token
    end
  end
end