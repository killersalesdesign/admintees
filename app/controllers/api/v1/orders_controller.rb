class Api::V1::OrdersController < Api::V1::BaseController
  def index
    limit   = Rails.env.production? ? 100 : 10
    if params[:last_id].present?
      @orders = Order.where('id > ?', params[:last_id]).page(params[:page]).per(limit)
    elsif params[:updated_at].present?
      @orders = Order.where('updated_at > created_at', params[:last_id]).order(:id => :asc).page(params[:page]).per(limit)

      @orders.each do |order|
        order.updated_at = order.created_at
        order.save
      end
    else
      @orders = Order.page(params[:page]).per(limit)
    end
  end

  def show
    @order = Order.find(params[:id]) rescue nil
    # render :json => :show
  end

  private


end