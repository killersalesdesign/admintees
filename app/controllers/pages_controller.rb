class PagesController < ApplicationController
  before_filter :set_cache_buster
  def contact_us
    @contact = ContactUs.new
  end

  def send_contact_us
    # contact = ContactUs.new(params[:contact_us])
    contact = ContactUs.new(params[:contact_us])
    if contact.valid? && !contact.spam?
      contact.deliver
      respond_to do |format|
        format.html { redirect_to "send_contact_us", :flash => { :notice => "Message has been sent!" } }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to :back, :flash => { :error => contact.errors.full_messages } }
        format.json { head :no_content }
      end
    end
    # render :json => {params: params}
  end

  def about_us
  end

  def faq
  end

  def terms
  end

  def privacy
  end

  private

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
