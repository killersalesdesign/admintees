class Admin::InvoicesController < Admin::BaseController
  layout "pdfview"
  require 'rubygems'
  require 'json'

  def index
    @orders = Order.where(:product_id => params[:product_id], :shipped => 0, :refund => false).order("id ASC").page(params[:page]).per(100)
  end

  def show
    @order = Order.find(params[:id])
    @product = @order.product

    @shipping_fee = @product.settings(:options).prices
    @shippingFee = []
    @order.ordered_products.each do |ordered|
      @shipping_fee.each do |shipFee|
        if shipFee[:category_id] == ordered.category_id
          @shippingFee << shipFee[:postage]
        end
      end
    end

    @shipping_info = JSON.parse(@order.shipping_info)
    @ordered_products = @order.ordered_products
    if @order.website.name == "FanTees"
      @itemID = "FAN0#{@product.id}"
    else
      @itemID = "JOB0#{@product.id}"
    end
  end

  def by_batch
    batch = BatchOrder.find_by_batch_key(params[:batch_id])
    if batch
      @orders = batch.orders
    end
  end

  def in_stocks
    # @product_ids = Product.where(:status => 2).collect(&:id)
    # @orders = Order.where(:product_id => @product_ids, :shipped => 0, :refund => false).order("product_id DESC").page(params[:page]).per(100)

    @product_ids = Product.where(:status => 2).collect(&:id)
    order_ids = OrderedProduct.where(:product_id => @product_ids).select('order_id').collect(&:order_id)
    @orders = Order.where(:id => order_ids.uniq, :shipped => 0, :refund => false).order("product_id DESC").page(params[:page]).per(100)
  end

  def by_sizes
    op_ids  = OrderedProduct.joins(:order).where("orders.product_id = ? && orders.shipped = ? && ordered_products.size = ?", params[:product_id], 0, params[:size].upcase).order("ordered_products.size ASC").group("ordered_products.order_id").collect(&:order_id)
    @orders = Order.where(:id => op_ids, :refund => false)
  end

  def by_category
    op_ids  = OrderedProduct.joins(:order).where("orders.product_id = ? && orders.shipped = ? && ordered_products.category_id = ?", params[:product_id], 0, params[:type]).order("ordered_products.size ASC").group("ordered_products.order_id").collect(&:order_id)
    @orders = Order.where(:id => op_ids, :refund => false)
  end

  def by_category_size
    op_ids  = OrderedProduct.joins(:order).where("orders.product_id = ? && orders.shipped = ? && ordered_products.category_id = ? && ordered_products.size = ?", params[:product_id], 0, params[:type], params[:size].upcase).order("ordered_products.size ASC").group("ordered_products.order_id").collect(&:order_id)
    @orders = Order.where(:id => op_ids, :refund => false)
  end

  def by_all
    order_ids  = OrderedProduct.where(:product_id => params[:product_id]).group("order_id").collect(&:order_id)
    @test = order_ids

    @orders = Order.where('id IN (?) AND shipped != ? AND refund = ?', order_ids, 1, false)
  end

  def by_single
    order_ids  = OrderedProduct.where(:product_id => params[:product_id]).group("order_id").collect(&:order_id)
    allOrders = Order.includes(:ordered_products).where('orders.id IN (?) AND orders.shipped != ? AND orders.refund = ?', order_ids, 1, false).order('ordered_products.category_id ASC,  ordered_products.size ASC')

    @orders = []
    allOrders.each do |o|
      if o.quantity == 1
        @orders << o
      end
    end
    # @orders.sort { |a,b| a.ordered_products.size <=> b.ordered_products.size }
  end

  def by_multiple
    order_ids  = OrderedProduct.where(:product_id => params[:product_id]).group("order_id").collect(&:order_id)
    # allOrders = Order.where('id IN (?) AND shipped != ? AND refund = ?', order_ids, 1, false)
    allOrders = Order.includes(:ordered_products).where('orders.id IN (?) AND orders.shipped != ? AND orders.refund = ?', order_ids, 1, false).order('ordered_products.category_id ASC,  ordered_products.size ASC')
    @orders = []
    allOrders.each do |o|
      if o.quantity != 1
        @orders << o
      end
    end
  end
end
