class Admin::NotesController < Admin::BaseController
  def index
    unless params[:user_id].nil?
      @notes = Note.where(:user_id => params[:user_id])
    end
  end

  def show
    @note = Note.find(params[:id])
  end

  def update
    @note = Note.find(params[:id])
    @note.update_attributes(params[:note])
  end

  def create
    @note = Note.new(params[:note])
    @note.save
  end


end
