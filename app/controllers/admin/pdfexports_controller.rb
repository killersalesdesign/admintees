class Admin::PdfexportsController < Admin::BaseController
  layout "pdfview"
  require 'rubygems'
  require 'json'

  def index
    @product = Product.find(params[:product_id])
    @orders = Order.where(:product_id => params[:product_id], :shipped => 0).order("id ASC")

  end

  def show
    @order = Order.find(params[:id])
    @product = @order.product
    @shipping_info = JSON.parse(@order.shipping_info)
    @ordered_products = @order.ordered_products
    if @order.website.name == "FanTees"
      @itemID = "FAN0#{@product.id}"
    else
      @itemID = "JOB0#{@product.id}"
    end
  end

  def singlereport
    @product = Product.find(params[:product_id])
    @stats = @product.stats(params[:print])
    # @stats = @product.settings(:options).prices
  end

  def batches
    @batches = Batch.where(:batch_key => params[:batch_key])
      @categories = []
      @boxCategories = []
      @batches.each do |batch|
        stats = JSON.parse(batch.batch_stats)
        stats.each do |stat, index|

          product = Product.find(batch.product_id)
          p_slug = product.slug

          unless @categories.include?(stat['category_id'])
            @categories << stat['category_id']
            @boxCategories << {:category_id => stat['category_id'], :allSizes => nil, :category_name => stat['type'], :stats => [{:slug => p_slug, :product_name => batch.product_name, :product_id => batch.product_id, :sizes => stat['sizes']}]}
          else
            @boxCategories.each do |boxCat|
              puts "awa bai: #{boxCat[:category_id]}"
              if boxCat[:category_id] == stat['category_id']
                boxCat[:stats] << {:slug => p_slug, :product_name => batch.product_name, :product_id => batch.product_id, :sizes => stat['sizes']}
              end
            end
          end
        end

      end

      @boxCategories.each do |boxCat|
        allSizes = []
        boxCat[:stats].each do |stats|
          stats[:sizes].each do |size|
            unless allSizes.include?(size['name'])
              allSizes << size['name']
            end
          end
        end

        boxCat[:allSizes] = allSizes
      end

  end
end
