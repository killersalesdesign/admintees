class Admin::V2::OrdersController < Admin::BaseController
  before_filter :query_order, :only => [:index, :total_orders]

  def index
    @orders = @orders.select("products.title, products.id as product_id, orders.*, users.first_name as user_first_name, users.last_name as user_last_name, batch_orders.batch_key")
    @orders = @orders.page(params[:page]).per(100).order('orders.id DESC')
  end

  def total_orders
    render :json => @orders.count
  end

  private

  def query_order
    @orders = Order.joins(:product)
    @orders = @orders.joins(:user)
    # @orders = @orders.joins(:batch_order, "LEFT OUTER")
    @orders = @orders.joins("LEFT OUTER JOIN `batch_orders` ON `batch_orders`.`id` = `orders`.`batch_order_id`")
    @orders = @orders.where("products.title LIKE ? OR users.first_name LIKE ? OR users.last_name LIKE ? OR batch_orders.batch_key LIKE ? OR orders.order_id LIKE ?", "%#{params[:keyword]}%", "%#{params[:keyword]}%", "%#{params[:keyword]}%", "%#{params[:keyword]}%", "%#{params[:keyword]}%") unless params[:keyword].blank?
    @orders = @orders.where(:shipped => params[:shipped]) unless params[:shipped].blank?

    if !params[:from].blank? || !params[:to].blank?
      params[:from] = params[:from].blank? ? Time.zone.now : params[:from].to_date
      params[:to]   = params[:to].blank? ? Time.zone.now : params[:to].to_date
      @orders       = @orders.where(:created_at => params[:from].beginning_of_day..params[:to].end_of_day)
    end

    unless params[:printed].blank?
      bool = params[:printed] == 'true' ? true : false
      @orders = @orders.where(:printed => bool)
    end
  end
end
