class Admin::DesignsController < Admin::BaseController
  respond_to :json

  def create
    @design = ProductDesign.new
    @design.canvas = params[:canvas]
    @design.save
    respond_with @design
  end

  def show
    @design = ProductDesign.find params[:id]
    respond_with @design
  end
end