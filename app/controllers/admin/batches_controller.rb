class Admin::BatchesController < Admin::BaseController
  def index
    @total_count = 0
    if params[:product_id].present?
      @batches = Batch.where(product_id: params[:product_id]).first
    elsif params[:batch_key].present?
      @batches = Batch.where(batch_key: params[:batch_key])
    elsif params[:page].present?
      limit = params[:limit].to_i
      page  = params[:page].to_i

      all_batches   = Batch.order('created_at DESC')
      batches       = all_batches.group_by(&:batch_key)
      @total_count  = batches.length

      @result_batches = []
      x = 0
      p = 1
      batches.each do |i, v|
        if x <= limit
          if p == page
            @result_batches << v
          end
          x += 1
          if x == limit
            x = 0
            p += 1
            break if @result_batches.length >= 1
          end
        end
      end

      render :json => {batches: @result_batches, total: @total_count}
      return
    else
      @batches = Batch.all
    end

    render :json => @batches
  end

  def destroy
    @batch = Batch.find(params[:id])
    @batch.delete
    render :json => {success: true}
  end

  def create
    @batch = Batch.new(params[:batch])
    if @batch.save
      render :json => {success: true}
    else
      render :json => {error: true}
    end
  end

  def show
    @batch = Batch.where(id: params[:id]).first
    if @batch.nil?
      render :json => {error: true}
    else
      render :json => @batch
    end
  end

  def update
    @batch = Batch.find(params[:id])
    @batch.update_attributes(params[:batch])
    render :json => @batch
  end

end
