class Admin::EparcelController < Admin::BaseController

  def index
    # order_ids  = OrderedProduct.where(:product_id => params[:product_id]).group("order_id").collect(&:order_id)
    # @orders = Order.where('id IN (?) AND shipped != ? AND refund = ?', order_ids, 1, false)

    order_ids  = OrderedProduct.where(:product_id => params[:product_id]).group("order_id").collect(&:order_id)
    allOrders = Order.includes(:ordered_products).where('orders.id IN (?) AND orders.shipped != ? AND orders.refund = ?', order_ids, 1, false).order('ordered_products.category_id ASC,  ordered_products.size ASC')

    @orders = []
    allOrders.each do |o|
      if o.quantity == 1
        @orders << o

      end
    end

    allOrders.each do |o|
      if o.quantity != 1
        @orders << o
      end
    end



  end

end
