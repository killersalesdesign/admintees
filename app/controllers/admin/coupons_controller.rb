class Admin::CouponsController < ApplicationController
  def index
    @coupons = Coupon.all
  end

  def create
    @coupon = Coupon.new params[:coupon]
    @coupon.save
  end

  def show
    @coupon = Coupon.find params[:id]
  end

  def update
    @coupon = Coupon.find(params[:id])
    @coupon.update_attributes(params[:coupon])
  end

  def destroy
    @coupon = Coupon.find(params[:id])
    @coupon.delete

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
