class Admin::CategoryController < Admin::BaseController
  def index
    if params[:product_id]
      product     = Product.find(params[:product_id])
      @categories = Category.where(:id => product.settings(:options).categories).where(:active => true)
    else
      @categories = Category.all
    end
  end

  # def index
  #   @categories = Category.all
  # end

  def show
    @category = Category.find(params[:id])
  end

  def create
    @category = Category.new(:name => params[:name])
    @category.settings(:options).sizes = params[:sizes]
    @category.settings(:options).weights = params[:weights]
    @category.save
  end

  def update
    @category = Category.find(params[:id])
    @category.settings(:options).sizes = params[:sizes]
    @category.settings(:options).weights = params[:weights]
    @category.update_attributes(:name => params[:name], :active => params[:active])
  end

  def destroy
    @category = Category.find(params[:id])
    @category.delete

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def sold_per_category
    result = Category.per_category_sizes_sold

    respond_to do |format|
      format.json do
        render json: result
      end
    end
  end
end
