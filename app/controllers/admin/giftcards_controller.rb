class Admin::GiftcardsController < ApplicationController
  def index
    @giftcards = Giftcard.all
  end

  def create
    @giftcard = Giftcard.new params[:giftcard]
    @giftcard.save
  end

  def show
    @giftcard = Giftcard.find params[:id]
  end

  def update
    @giftcard = Giftcard.find(params[:id])
    @giftcard.update_attributes(params[:giftcard])
  end

  def destroy
    @giftcard = Giftcard.find(params[:id])
    @giftcard.delete

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
