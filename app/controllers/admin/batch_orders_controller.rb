class Admin::BatchOrdersController < Admin::BaseController
  def index
    @batches = BatchOrder.where("batch_key LIKE ?", "%#{params[:k]}%")
    render :json => @batches
  end

  def show

  end

  def show_by_order
    order = Order.find params[:id]
    render :json => order.batch_order
  end

  def create
    batch_key = params[:batch_key].strip
    batch = BatchOrder.where(:batch_key => batch_key)
    if batch.count > 0
      batch = batch.first
    else
      batch = BatchOrder.new
      batch.batch_key = batch_key
      batch.save
    end

    begin
      order = Order.find params[:order_id]
      order.batch_order_id = batch.id
      order.save
    rescue
    end

    render :json => batch
  end
end