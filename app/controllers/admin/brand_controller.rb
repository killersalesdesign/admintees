class Admin::BrandController < Admin::BaseController
  def index
    @brands = Brand.all
  end

  def show
    @brand = Brand.find(params[:id])
  end

  def create
    @brand = Brand.new(:name => parmas[:name])
    @brand.save
  end

  def update
    @brand = Brand.find(params[:id])
    @brand.update_attributes(:name => params[:name], :active => params[:active])
  end

  def destroy
    @brand = Brand.find(params[:id])
    @brand.delete

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
