class Admin::ProductsController < Admin::BaseController
  respond_to :html, :xml, :json
  def autocomplete
    render json: Product.search(params[:query], autocomplete: true, limit: 10).map{|p| {"title" => p.title, "id" => p.id, "status" => p.status, "stocks" => p.stocks, "prices" => p.settings(:options).prices} }
  end

  def index
    unless params[:search].nil?
      @products = Product.where( "title LIKE ?", "%#{params[:search]}%")
    else

      if params[:active]
        status = params[:active] == "true" ? true : false
        @products = Product.where("active = ?", status)
      else
        @products = Product.all
      end
    end
  end

  def all_products
    @products = Product.filters(params, params[:limit])
  end

  def show_sold_stats
    render :json => Product.stats_amount_sold(params[:date_from], params[:date_to])
  end

  def products_info
    p_inactive = params[:inactive]
    status   = params[:status].to_i
    total    = 0

    params[:inactive] = "false"
    params[:status] = 0
    campaign = Product.filters(params).count
    total    = campaign if status == 0

    params[:status] = 1
    printing = Product.filters(params).count
    total    = printing if status == 1

    params[:status] = 2
    in_stocks = Product.filters(params).count
    total     = in_stocks if status == 2

    params[:inactive] = "true"
    inactive  = Product.filters(params).count
    total     = inactive if p_inactive == "true"

    render :json => {total: total, campaign: campaign, printing: printing, in_stocks: in_stocks, inactive: inactive}
  end

  def new
    @product = Product.new
  end

  def destroy
    @product = Product.find(params[:id])
    @product.remove_from_index!
    @product.destroy

    render :json => {success: true}
  end

  def create
    @product = Product.new(params[:product])
    if params[:categories].present?
      @product.settings(:options).categories = params[:categories]
    end
    if params[:prices].present?
      @product.settings(:options).prices = params[:prices]
    end

    if params[:design].present?
      design = ProductDesign.find params[:design][:id]
      design.product = @product
      design.save
    end

    if params[:product_types].present?
      @product.settings(:options).categories = params[:product_types].collect{|item| item[:category_id]}
      @product.settings(:options).prices = params[:product_types]
      # params[:product_types].each do |product_type|
      #   svg_image = get_svg_image(@product.product_design.canvas)
      #   png_file = Tempfile.new ["#{@product.id}_#{Time.new}", '.png']
      #   svg_image.write png_file.path
      #   product_image = @product.product_images.build(product_image: png_file)
      # end
    end

    if params[:gift_card_code].present?
      @product.gift_card_code = params[:gift_card_code]
    end
    if @product.save
      respond_with @product
    else
      render json: {message: @product.errors.full_messages.first}
    end
    # website = @product.website.name
    # client = Sendgrid::API::Client.new('graphicsadmin', 'Killer_001')
    # list = Sendgrid::API::Entities::List.new(:list => "#{website} #{@product.title}")
    # response = client.lists.add(list)

    # if @product.save
    #   params[:prices].each do |p|
    #     prices = @product.prices.new(category_id: p[:category_id], amount: p[:amount], profit: p[:profit])
    #     prices.save
    #   end
    # end
  end

  def show
    # @product = Product.find(params[:id])
    @product = Product.where(id: params[:id]).first
    if @product.nil?
      render :json => {error: true}
    end
  end

  def update
    @product = Product.find(params[:id])
    if params[:gift_card_code].present?
      @product.gift_card_code = params[:gift_card_code]
    end
    if params[:prices].present?
      @product.settings(:options).categories = params[:categories]
      @product.settings(:options).prices = params[:prices]

      puts "sizes: #{@product.settings(:options).prices}"
    end

    if params[:mailing].present?
      params[:mailing] = [] if params[:mailing].nil?
      @product.settings(:options).mailing = params[:mailing]

      if params[:to_mail_id].present?
        email_obj = Email.find(params[:to_mail_id])

        orders = Order.where("product_id = ?", @product.id)
        orders.each do |o|
          user_emails_received = o.user.settings(:options).emails_received

          exist = false
          user_emails_received.each do |i|
            if i[:product].to_i == @product.id
              if i[:emails].include? params[:to_mail_id]
                exist = true
              end
            end
          end

          # do not include refunded orders
          exist = true if o.refund

          unless exist
            ProductsEmailWorker.perform_async(o.user.id, params[:to_mail_id], @product.id, o.id)
            if email_obj.email_type == "campaign_ended"
              # update order printing status here
              o.printed = true
              o.save
            end

            if email_obj.email_type == "arrived"
              # schedule `howd we do` email 6 days after
              how_we_do = Email.find_by_email_type("howd_we_do")
              ProductsEmailWorker.perform_in(6.days, o.user.id, how_we_do.id, @product.id, o.id)
              # ProductsEmailWorker.perform_in(1.minute, o.user.id, how_we_do.id, @product.id, o.id)

              # schedule `new design` email 11 days after
              new_design = Email.find_by_email_type("new_designs")
              ProductsEmailWorker.perform_in(11.days, o.user.id, new_design.id, @product.id, o.id)
              # ProductsEmailWorker.perform_in(2.minutes, o.user.id, new_design.id, @product.id, o.id)
            end

            if email_obj.email_type == "howd_we_do"
              # schedule `new design` email 11 days after
              new_design = Email.find_by_email_type("new_designs")
              ProductsEmailWorker.perform_in(5.days, o.user.id, new_design.id, @product.id, o.id)
            end
          end

          # unless o.user.settings(:options).emails_received.include? params[:to_mail_id]
          #   ProductsEmailWorker.perform_async(o.user.id, params[:to_mail_id], @product.id)
          # end
        end
      end
    else
      @product.settings(:options).mailing = []
    end


    Stock.update_stocks(params[:id], params[:stocks], params[:categories]) if params[:stocks] && params[:status] != 0

    @product.update_attributes(params[:product])
  end

  def product_image
    @product = Product.find(params[:product_id])
    @image   = @product.product_images.new(:product_image => params[:file], :category_id => params[:category_id])
    @image.save

    render :json => @image
  end

  def delete_product_image
    @image = ProductImage.find(params[:id])
    @image.delete
    render :json => {success: true}
  end

  def update_single_report_extra
    # @product = Product.find(params[:product_id])
    product = Product.find params[:id]
    product.settings(:options).prices = params[:prices]
    product.save

    render :json => product.settings(:options).prices
  end

  # private

  # def get_svg_image(svg_data)
  #   image_list = Magick::ImageList.new
  #   svg_image = image_list.from_blob(svg_data)
  # end

  def download_csv
    @products = Product.filters(params)
    headers['Content-Disposition'] = "attachment; filename=\"products.csv\""
    headers['Content-Type'] ||= 'text/csv'
  end
end
