class Admin::OrderedProductsController < Admin::BaseController
  def index
    if params[:order_id].present?
      @ordered_products = OrderedProduct.where(:order_id => params[:order_id])
    else
      @ordered_products = OrderedProduct.all
    end
  end

  def update
    @ordered_products = OrderedProduct.find(params[:id])
    params[:ordered_product]["size"] = params[:ordered_product]["size"].to_s.strip
    @ordered_products.update_attributes(params[:ordered_product])
  end


end
