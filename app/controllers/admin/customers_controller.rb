class Admin::CustomersController < Admin::BaseController
  def index
    limit = params[:limit] || 100
    if params[:search]
      @users = User.user_orders params
      # @users = User.where("email LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR address LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").order("id DESC").page(params[:page]).per(limit)
    else
      @users = User.user_orders params
      # @users = User.order("id DESC").page(params[:page]).per(limit)
    end
    render :json => @users
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(params[:customer])
  end

  def show
    @user = User.where(:id => params[:id]).first
  end

  def create
    @user = User.new
    @user.email = params[:email]
    @user.first_name = params[:first_name]
    @user.last_name = params[:last_name]
    @user.address = params[:address]
    @user.phone = params[:phone]

    password = Devise.friendly_token[0,8]
    @user.password = password
    @user.password_confirmation = password

    if @user.save
      UserMailer.send_on_manual_user(params[:email], password).deliver
    end
  end

  def total_users
    if params[:search]
      total = User.where("email LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR address LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").count
    else
      total = User.count
    end
    render :json => total
  end
end
