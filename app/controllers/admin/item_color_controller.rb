class Admin::ItemColorController < Admin::BaseController
  def index
    @item_colors = ItemColor.all
  end

  def show
    @item_color = ItemColor.find(params[:id])
  end

  def create
    @item_color = ItemColor.new(:name => parmas[:name])
    @item_color.save
  end

  def update
    @item_color = ItemColor.find(params[:id])
    @item_color.update_attributes(:name => params[:name], :active => params[:active])
  end

  def destroy
    @item_color = ItemColor.find(params[:id])
    @item_color.delete

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
