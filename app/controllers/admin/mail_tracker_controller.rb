class Admin::MailTrackerController < ApplicationController
  def stats
    stats = ["today","week","month","year"]

    mail_stats = {}
    stats.each do |stat|
      result = MailTracker.get_stats(stat)
      mail_stats[stat] = result
    end

    render :json => mail_stats
  end
end
