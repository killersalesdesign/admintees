class Admin::AbandonmentController < ApplicationController
  def index
    @unfinished_orders = UnfinishedOrder.all
  end

  def stats
  	today 		= UnfinishedOrder.get_stats("today")
  	yesterday = UnfinishedOrder.get_stats("yesterday")
  	week 			= UnfinishedOrder.get_stats("week")
  	month 		= UnfinishedOrder.get_stats("month")
  	alltime 	= UnfinishedOrder.get_stats("alltime")

  	render :json => { 
  		:today => { 
  			abandons: today[:abandons], 
  			abandon_rate: today[:abandon_rate], 
  			recovered_revenue: today[:recovered_revenue],
  			lost_revenue: today[:lost_revenue], 
  			rejoins: today[:rejoins],  
  			rejoin_rate: today[:rejoin_rate]
  		},
  		:yesterday => {
  			abandons: yesterday[:abandons], 
  			abandon_rate: yesterday[:abandon_rate], 
  			recovered_revenue: yesterday[:recovered_revenue],
  			lost_revenue: yesterday[:lost_revenue], 
  			rejoins: yesterday[:rejoins],  
  			rejoin_rate: yesterday[:rejoin_rate]
  		},
  		:week => {
  			abandons: week[:abandons],
  			abandon_rate: week[:abandon_rate], 
  			recovered_revenue: week[:recovered_revenue],
  			lost_revenue: week[:lost_revenue], 
  			rejoins: week[:rejoins],  
  			rejoin_rate: week[:rejoin_rate]
  		},
  		:month => {
  			abandons: month[:abandons], 
  			abandon_rate: month[:abandon_rate], 
  			recovered_revenue: month[:recovered_revenue],
  			lost_revenue: month[:lost_revenue], 
  			rejoins: month[:rejoins],
  			rejoin_rate: month[:rejoin_rate]
  		},
  		:alltime => {
  			abandons: alltime[:abandons], 
  			abandon_rate: alltime[:abandon_rate], 
  			recovered_revenue: alltime[:recovered_revenue],
  			lost_revenue: alltime[:lost_revenue], 
  			rejoins: alltime[:rejoins],  
  			rejoin_rate: alltime[:rejoin_rate]
  		}
  	}
  end

end
