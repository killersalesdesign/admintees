class Admin::NotificationsController < Admin::BaseController
  def index
    notifications = current_admin.notifications.where('created_at > ?', Time.zone.now.beginning_of_day)
    unread_notifications = current_admin.notifications.where('created_at > ?', Time.zone.now.beginning_of_day).where(seen: false)
    read_notifications = current_admin.notifications.where('created_at > ?', Time.zone.now.beginning_of_day).where(seen: true)
    
    ending = []
    ended = []

    if (notifications.count < Product.ending.count)
      notifications.destroy_all
      Product.ending.each do |product|
        if product.time_left[:days] == 0
          ended.push(product)
        else product.time_left[:days].between?(1,3)
          ending.push(product)
        end
      end
      ended.each do |product|
        notification = current_admin.notifications.build(message: "Ended: " + product.title + " sold #{product.total_sold}", ref_id: product.id, ref_type: "Product", seen: false)
        notification.save
      end
      ending.each do |product|
        notification = current_admin.notifications.build(message: "Ended: " + product.title + " sold #{product.total_sold}", ref_id: product.id, ref_type: "Product", seen: false)
        notification.save
      end
    end

    unread_notifications = current_admin.notifications.where('created_at > ?', Time.zone.now.beginning_of_day).where(seen: false)

    if notifications.count == unread_notifications.count
      @notifications = current_admin.notifications
    else
      @notifications = []
    end
    
  end

  def clear
    current_admin.notifications.destroy_all
    notifications = current_admin.notifications.where('created_at > ?', Time.zone.now.beginning_of_day)
    notifications.each do |n|
      n.seen = true
      n.save
    end
    render json: {success: true}
  end
end
