
class Admin::StatisticsController < Admin::BaseController
  require 'time'
  require 'date'

  def all_products
    if params[:id].present?
      @products = Product.find(params[:id])
    else
      @products = Product.last()
    end
  end

  def all_products_list
    @products = Product.find(:all, :select => "id, title")
  end

  def index
    @ordered_products = OrderedProduct.all
  end

  def product
    render json: {title: Product.find(params[:id]).title}
  end

  def daily_sales
    # @sales = OrderedProduct.group_by_day(:created_at).count
    # sales = OrderedProduct.all
    # sales_per_day = sales.group_by { |s| s.created_at.to_date unless s.created_at.nil? }
    # sales_per_day = sales.group_by { |s| s.created_at.beginning_of_day unless s.created_at.nil? }
    sales = OrderedProduct.select('DATE_FORMAT(created_at,"%Y-%m-%d") as created_day, category_id, created_at, id, order_id, product_id, quantity, size, updated_at')
    sales = sales.group_by(&:created_day)
    render :json => sales
  end

  def daily_product_types
    # @sales = OrderedProduct.group_by_day(:created_at).count
    # sales = OrderedProduct.all
    # sales_per_day = sales.group_by { |s| s.category.name unless s.category.name.nil? }
    sales = OrderedProduct.joins(:category)
    sales = sales.select("categories.name, category_id, ordered_products.created_at, ordered_products.id, order_id, product_id, quantity, size, ordered_products.updated_at")
    sales = sales.group_by(&:name)
    render :json => sales
  end

  def daily_product_sizes
    # @sales = OrderedProduct.group_by_day(:created_at).count
    # @sales = OrderedProduct.all
    # @sales_per_day = @sales.group_by { |s| s.size }
    sales = OrderedProduct.all
    sales = sales.group_by(&:size)
    render :json => sales
  end

  def tees_counts
    @tees = OrderedProduct.where(:category_id => 1).count
    render :json => @tees
  end

  def hat_counts
    @hat = OrderedProduct.where(:category_id => 2).count
    render :json => @hat
  end

  def keyring_counts
    @keyring = OrderedProduct.where(:category_id => 3).count
    render :json => @keyring
  end


  #Reports

  def sales_today
    render :json => Product.find(params[:id]).sales_today
  end

  def sales_yesterday
    render :json => Product.find(params[:id]).sales_yesterday
  end

  def sales_all_time
    render :json => Product.find(params[:id]).sales_all_time
  end

  def views_over_time
  end

  def sizes
    product = Product.find(params[:id])
    # if !params[:start_date].empty? && !params[:start_date].nil?
    #   start_date = DateTime.parse(params[:start_date]).beginning_of_day
    #   end_date = DateTime.parse(params[:start_date]).end_of_day
    # else
    start_date = product.created_at
    puts start_date
    end_date = Date.today.end_of_day
    puts end_date
    # end

    size = {}
    items = []
    results = []
    orders = Order.where("product_id = ? AND DATE(created_at) BETWEEN ? AND ?", params[:id], start_date, end_date)
    puts orders.inspect
    size = Hash.new(0)
    orders.each do |order|
      order.ordered_products.each do |op|
        size[op.size.strip] += op.quantity
      end
    end
    total = 0
    size.each_with_index do |k,v|
      item = Hash.new(0)
      item[:name] = k[0]
      item[:qty] = k[1]
      total += k[1]
      results.push item
    end

    render :json => {sizes: results, total: total}
  end

  def ordered
    today = Date.yesterday
    today = Order.last.created_at
    start_day = today - 1.week
    product = Product.find(params[:id]).sales_by_type(start_day.beginning_of_day, today.end_of_day)
    render json: product
  end

  def hourly_orders
    if !params[:start_date].empty?
      start_date = DateTime.parse(params[:start_date]).end_of_day
    else
      start_date = Date.today.end_of_day
    end
    render json: Product.find(params[:id]).sales_per_hour(start_date)
  end

  def analytics
    analytics = GoogleAnalytics.new(
      start_date: DateTime.now.prev_month.strftime("%Y-%m-%d"),
      end_date: DateTime.now.strftime("%Y-%m-%d")
    )
    items = analytics.product_referrals(params[:id])

    render json: items
  end

  def campaigns
    analytics = GoogleAnalytics.new(
      start_date: DateTime.now.prev_month.strftime("%Y-%m-%d"),
      end_date: DateTime.now.strftime("%Y-%m-%d")
    )
    items = analytics.product_campaigns(params[:id])

    render json: items
  end

  def views
    if !params[:start_date].empty?
      start_date = DateTime.parse(params[:start_date]).beginning_of_day.strftime("%Y-%m-%d")
      end_date = DateTime.parse(params[:start_date]).end_of_day.strftime("%Y-%m-%d")
    else
      start_date = DateTime.now.beginning_of_day.strftime("%Y-%m-%d")
      end_date = DateTime.now.end_of_day.strftime("%Y-%m-%d")
    end

    analytics = GoogleAnalytics.new(
      start_date: start_date,
      end_date: start_date
    )
    render json: analytics.product_views(params[:id])
    # render json: []
  end
end