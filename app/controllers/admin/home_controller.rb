class Admin::HomeController < Admin::BaseController
  def index
    @admin = current_admin
    gon.current_admin = current_admin
  end
end
