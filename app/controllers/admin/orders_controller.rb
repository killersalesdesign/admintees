class Admin::OrdersController < Admin::BaseController
  def index
    # Order.reindex

    if params[:type].present?
      case params[:type].to_s
      when "search"

        if params[:total].present?
          @orders = Order.joins(:product).where("shipping_info LIKE ? OR order_id LIKE ? OR products.title LIKE ?", "%#{params[:k]}%", "%#{params[:k]}%", "%#{params[:k]}%").count()
          render :json => @orders
        else
          @orders = Order.joins(:product).where("shipping_info LIKE ? OR order_id LIKE ? OR products.title LIKE ?", "%#{params[:k]}%", "%#{params[:k]}%", "%#{params[:k]}%").order("id DESC")
        end

      else
        newdate = Date.parse(params[:k]).strftime("%Y-%m-%d")

        if params[:total].present?
          @orders = Order.where("created_at LIKE ?" , "#{newdate}%").count()
          render :json => @orders
        else
          @orders = Order.where("created_at LIKE ?" , "#{newdate}%").order("id DESC").page(params[:page]).per(100)
        end

      end
    else
      if params[:product_id].present?
        order_ids  = OrderedProduct.where(:product_id => params[:product_id]).group("order_id").collect(&:order_id)
        @orders = Order.where('id IN (?) AND shipped != ?', order_ids, 1)


        if params[:shipped].present?
          @orders = Order.where(:product_id => params[:product_id], :shipped => 0).order("id ASC")
        end
      elsif params[:user_id].present?
        @orders = Order.where(:user_id => params[:user_id]).order("id DESC")
      elsif params[:order_id].present?
        @orders = Order.where(:order_id => params[:order_id].upcase)
      else
        if params[:newdate].present?
          newdate = Date.parse(params[:newdate]).strftime("%Y-%m-%d")
          if params[:total].present?
            @orders = Order.where("created_at LIKE ?" , "#{newdate}%").count()
            render :json => @orders
          else
            @orders = Order.where("created_at LIKE ?" , "#{newdate}%").order("id DESC").page(params[:page]).per(100)
          end

        else
          # date_today = DateTime.now.beginning_of_day.strftime("%Y-%m-%d")
          date_today = ""

          if params[:total].present?
            @orders = Order.all().count()
            render :json => @orders
          else
            @orders = Order.order("id DESC").page(params[:page]).per(100)
          end

        end
      end

    end
  end

  def new
    @order = Order.new
  end

  def addresslabel
  end

  def update
    @order = Order.find(params[:id])
    params[:order].delete(:batch_key)

    @order.update_attributes(params[:order])
    # render :json => params[:order]
  end

  def show
    @order = Order.where(:id => params[:id]).first
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    render :json => @order
  end

  def create
    details = params[:details]
    params[:order][:shipping_info] = params[:order][:shipping_info].to_json
    params[:order][:order_id] = SecureRandom.hex[0..7].upcase

    @order = Order.new(params[:order])
    if @order.save
      # save details here
      details.each do |p|
        OrderedProduct.create(
          :category_id => p[:category][:id],
          :order_id => @order.id,
          :quantity => p[:qty],
          :size => p[:size].upcase.strip,
          :product_id => @order.product_id
        )
      end
    end
  end

  def in_stocks
    @product_ids = Product.where(:status => 2).collect(&:id)
    order_ids = OrderedProduct.where(:product_id => @product_ids).select('order_id').collect(&:order_id)
    @orders = Order.where(:id => order_ids.uniq, :shipped => 0, :refund => false).order("product_id DESC")
  end

  def in_eparcel
    # render :json => "test"
    # @product_ids = Product.where(:status => 2).collect(&:id)
    # @orders = Order.where(:product_id => @product_ids, :shipped => 0).order("product_id DESC")
    @product_ids = Product.where(:status => 2).collect(&:id)
    order_ids = OrderedProduct.where(:product_id => @product_ids).select('order_id').collect(&:order_id)
    @orders = Order.where(:id => order_ids.uniq, :shipped => 0, :refund => false).order("product_id DESC")
  end

  def get_orders_by_order_id
    if params[:order_ids]
      order_ids = params[:order_ids].gsub(/\s+/, "").split(",")
      all_orders = Order.includes(:ordered_products).where('orders.order_id IN (?) AND orders.shipped != ? AND orders.refund = ?', order_ids, 1, false).order('ordered_products.category_id ASC, ordered_products.size ASC')
    else
      b = BatchOrder.find_by_batch_key(params[:batch_key])
      all_orders = b.orders.includes(:ordered_products).where("orders.shipped != ? AND orders.refund = ?", 1, false).order('ordered_products.category_id ASC, ordered_products.size ASC')
    end
    @orders = []

    if all_orders
      all_orders.each do |o|
        if o.quantity
          @orders << o
        end
      end

      # all_orders.each do |o|
      #   if o.quantity != 1
      #     @orders << o
      #   end
      # end
    end
  end
end
