class Admin::RatingsController < Admin::BaseController
  def index
    @ratings = Rating.all
  end

  def create
    rating = Rating.new(params[:rating])
    if rating.save
      render :json => "Success"
    else
      render :json => "Error"
    end
  end

  def show
    @rating = Rating.find(params[:id])
  end

  def update
    rating            = Rating.find(params[:id])
    rating.percent    = params[:rating][:percent]
    rating.website_id = params[:rating][:website_id]

    if rating.save
      render :json => "Success"
    else
      render :json => "Error"
    end
  end
end