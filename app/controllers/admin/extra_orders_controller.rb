class Admin::ExtraOrdersController < Admin::BaseController


  def update
    @extra_orders = ExtraOrder.find(params[:id])
    @extra_orders.settings(:options).sizes = params[:sizes]
    @extra_orders.update_attributes(params[:extra_order])
  end


end
