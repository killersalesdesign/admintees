class Admin::SearchesController < Admin::BaseController

  def index
    unless params[:q].nil?
      @searches = Product.search_by_tags params[:q]
      if @searches.empty?
        s = Search.new
        s.name = params[:q]
        s.save
      end
    else
      @searches = Search.select('*, count(name) as ncount').group(['name']).order('ncount desc')
    end
    respond_to do |format|
      format.json do
        @searches = Search.select('*, count(name) as ncount').group(['name']).order('ncount desc')
        render json: @searches
      end
      format.csv do
        @searches = Search.all
        headers['Content-Disposition'] = "attachment; filename=\"names\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end
end