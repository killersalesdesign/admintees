class Dashboard::HomeController < Dashboard::BaseController
  def index
    @user = current_user
    gon.current_user = current_user
  end
end
