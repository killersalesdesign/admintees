class Dashboard::BaseController < ApplicationController
  before_filter :authenticate_user!
  layout "dashboard"

end