class Dashboard::UsersController < Dashboard::BaseController
  def show
    @user = current_user
  end

  def update
    @user = current_user
    if params[:avatar]
      @user.update_attributes(:avatar => params[:avatar])
    else
      @user.update_attributes(params[:user])
    end
    # respond_to do |format|

    #   # if @user.update_attributes(:avatar => params[:avatar])
    #   #   format.json { render :json => @user }
    #   # else
    #   #   format.json { render :json => @user.errors, :status => :unprocessable_entity }
    #   # end

    #   if @user.update_attributes(params[:user])
    #     format.json { render :json => @user }
    #   else
    #     format.json { render :json => @user.errors, :status => :unprocessable_entity }
    #   end
    # end
  end
end
