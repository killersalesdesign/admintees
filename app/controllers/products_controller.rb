include ActionView::Helpers::NumberHelper
class ProductsController < ApplicationController
  def index
  end

  def show
    # @style      = "margin-bottom: 286px;"
    @product    = Product.where(:slug => params[:slug]).first
    @sold       = @product.total_sold
    @sales      = @product.start_number + @sold
    @categories = Category.where(:id => @product.settings(:options).categories).where(:active => true)
    @error      = params[:error] ? true : false

    gon.current_user = current_user
    gon.product      = @product
    gon.gallery      = @product.gallery
    gon.prices       = @product.settings(:options).prices
    gon.categories   = @categories
  end

  def paypal_checkout
    # puts params.inspect
    product = Product.find(params[:product_id])
    # qty   = 0
    key   = 0
    total = 0
    items = []
    session[:orders] = []
    params[:orders][:qty].each do |o|
      category_id = params[:orders][:styles][key]
      size  = params[:orders][:sizes][key]
      style = Category.find(category_id)
      info  = product.price_and_profit_of_style(category_id)

      # qty   += o.to_i
      total += o.to_i * info[:amount].to_f

      session[:orders] << {:category_id => style.id, :quantity => o.to_i, :size => size, :price => info[:amount]}
      items << {:name => product.title, :quantity => o.to_i, :description => "#{style.name} (#{size})", :amount => (info[:amount]*100)}

      key += 1
    end

    # total = qty * product.price

    response = EXPRESS_GATEWAY.setup_purchase((total*100).round,
      :items             => items,
      :ip                => request.remote_ip,
      :currency          => 'AUD',
      :return_url        => success_product_url(product.id),
      :cancel_return_url => "#{root_url}/#{product.slug}"
    )

    # render :json => {params: params, total: total}
    redirect_to "#{EXPRESS_GATEWAY.redirect_url_for(response.token)}&useraction=commit"
  end

  def return_url
    unless session[:orders].nil?
      params[:ip] = request.remote_ip
      paypal      = PaypalCheckout.new(params, session[:orders])
      product     = Product.find(params[:id])

      if paypal.purchase?
        order = paypal.create_order
        if order
          session.delete(:orders)

          sign_in(:user, paypal.user)

          redirect_to order_url(order.order_id)
        else
          redirect_to "#{root_url}#{product.slug}?error=true"
        end
      else
        redirect_to "#{root_url}#{product.slug}?error=true"
      end

    end
  end

  def set_checkout_session
    session[:checkout] = params
    redirect_to checkout_products_url
  end

  def checkout
    if session[:checkout]
      params = session[:checkout]

      @product = Product.find(params[:product_id])
      @order   = Order.new
      @info    = session[:checkout][:info]
      gon.info = @info || {}

      @total = 0
      key    = 0
      @orders = []
      params[:orders][:qty].each do |o|
        size  = params[:orders][:sizes][key]
        style = Category.find(params[:orders][:styles][key])
        price = @product.price_and_profit_of_style(style.id)[:amount].to_f * o.to_f
        @total += price
        @orders << {:description => "#{o}x #{style.name} (#{size})", :price => number_to_currency(price)}
        key += 1
      end
    else
      redirect_to root_url
    end

    # render :json => {session: session, params: params}
  end

  def eway_checkout
    session[:checkout][:info] = params
    params[:orders]           = session[:checkout][:orders]
    params[:product_id]       = session[:checkout][:product_id]

    eway     = EwayCheckout.new(params)
    response = eway.checkout(EWAY_COSTUMER_ID)

    puts "RESPONSE: #{response.inspect}"
    # render :json => {response: response}

    if response.success?
      token = response.authorization

      user     = User.find_or_initialize_by_email(params[:email])
      password = nil
      if user.id.nil?
        password                   = Devise.friendly_token[0,8]
        user.password              = password
        user.password_confirmation = password
        user.first_name            = params[:first_name]
        user.last_name             = params[:last_name]
        address                    = []

        address << params["address"] unless params["address"].blank?
        address << params["city"] unless params["city"].blank?
        address << params["state"] unless params["state"].blank?
        address << params["country"] unless params["country"].blank?
        user.address = address.join(", ")

        user.save
      end

      order = user.orders.new(
        :ip_address => request.remote_ip,
        :product_id => params[:product_id],
        :token      => token,
        :shipping_info => eway.paypal_shipping_format.to_json,
        :order_id   => eway.get_order_id
      )
      # render :json => {eway: eway.creditcard}
      if order.save
        session.delete(:checkout)

        eway.get_items.each do |o|
          # total += (o[:quantity] * o[:price])

          order.ordered_products.new(
            :quantity => o[:quantity],
            :size => o[:size],
            :category_id => o[:category_id]
          ).save
        end
      end

      sign_in(:user, user)
      # send mail after create
      order.send_order_mail(password)

      redirect_to order_url(order.order_id)
    else
      redirect_to :back, :flash => { :error => response.message }
    end
  end
end
