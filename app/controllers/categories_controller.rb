class CategoriesController < ApplicationController
  def index
    product     = Product.find(params[:product_id])
    @categories = Category.where(:id => product.settings(:options).categories).where(:active => true)
  end

  def show
  end
end
