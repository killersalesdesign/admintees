include ActionView::Helpers::NumberHelper
class ApplicationController < ActionController::Base
  # before_filter :redirect_to_admin
  protect_from_forgery

  def redirect_to_admin
    namespace = controller_path.split('/').first.strip
    if namespace != 'admin' && Rails.env.production?
      redirect_to admin_path unless namespace == 'devise'
    end
  end
end
