class HomeController < ApplicationController
  def index
    redirect_to admin_path
    # @products = Product.order("created_at desc").limit(3)
  end

  def barcode
    require 'barby'
    require 'barby/barcode/code_128'
    require 'barby/outputter/png_outputter'
    require 'parcel_barcode'
    
    p = ParcelBarcode.new(consignment_id: params[:id])

    barcode = Barby::Code128B.new p.get_eye_readable_article_no
    send_data barcode.to_png
  end

  def test
    # require 'parcel_barcode'
    # @p = ParcelBarcode.new(consignment_id: params[:id])
    render json: GoogleFonts.list
  end

  def eparcel
    respond_to do |format|
      format.xml
    end
  end
end
