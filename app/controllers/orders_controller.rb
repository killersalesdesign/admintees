class OrdersController < ApplicationController
  def show
    # @style      = "margin-bottom: 286px;"
    @order      = Order.find_by_order_id(params[:id])
    @product    = @order.product
    @sold       = @product.total_sold
    @sales      = @product.start_number + @sold
    @categories = Category.where(:id => @product.settings(:options).categories).where(:active => true)

    gon.product    = @product
    gon.gallery    = @product.gallery
    gon.categories = @categories
  end
end
