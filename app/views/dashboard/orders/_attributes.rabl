attributes :id, :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :shipping_info, :printed, :shipped, :description, :order_id

node do |order|
  {
    :shipping_info => ActiveSupport::JSON.decode(order.shipping_info),
    :product_title => order.product.title,
    :order_total => order.total_orders_amount[0]
  }
end