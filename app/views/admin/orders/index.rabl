collection @orders
extends "admin/orders/_attributes"

node do |order|
  {
    :user_first_name => order.user_id.nil? ? "" : order.user.first_name,
    :user_last_name => order.user_id.nil? ? "" : order.user.last_name,
    :product_title => order.product.title,
    :order_total => order.total_orders_amount[0],
    :shipping_fee => order.total_orders_amount[1],
    :website_name => order.website.name,
    :created_at => order.created_at.in_time_zone('Arizona'),
    :ordered_products_count => order.ordered_products.count,
    :weight => order.get_weight
  }
end