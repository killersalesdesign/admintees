collection @orders
extends "admin/orders/_attributes_stock"

node do |order|
  {
    :user_email => order.user.email,
    :order_total => order.total_amount,
    :weight => order.get_weight
  }
end
