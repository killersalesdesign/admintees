attributes :id, :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :shipping_info, :printed, :shipped, :description, :order_id, :website_id, :refund

node do |order|
  {
    :user_first_name => order.user.first_name,
    :user_last_name => order.user.last_name,
    :user_email => order.user.email,
    :product_title => order.product.title,
    :product_price => order.product.price,
    :order_total => order.total_orders_amount[0],
    :website_name => order.website.name,
    :created_at => order.created_at.in_time_zone('Arizona'),
    :ordered_products_count => order.ordered_products.count,
    :weight => order.get_weight
  }
end