object @order
extends "admin/orders/_attributes"

node do |order|
  {
    :user_first_name => order.user_id.nil? ? "" : order.user.first_name,
    :user_last_name => order.user_id.nil? ? "" : order.user.last_name,
    :user_email => order.user_id.nil? ? "" : order.user.email,
    :product_title => order.product.title,
    :product_price => order.product.price,
    :gallery => order.product.gallery,
    :website_name => order.website.name,
    :created_at => order.created_at.in_time_zone('Arizona'),
    :sent_emails => order.get_sent_emails,
    :notes => order.notes.to_json
  }
end