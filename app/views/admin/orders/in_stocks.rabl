collection @orders
extends "admin/orders/_attributes_stock"

node do |order|
  {
    :product_title => order.get_product_names,
  }
end