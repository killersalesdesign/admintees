attributes :id, :quantity, :size, :category_id, :order_id, :created_at, :product_id

node do |ordered|
  {
    :category_name => ordered.category.name,
    :product_title => ordered.title,
    :product_ads_spend => ordered.product.ads_spend,
    :image => ordered.category_image.product_image(:thumb),
    :product_price => ordered.price_and_profit[:amount]
  }
end