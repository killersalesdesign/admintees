collection @orders
extends "admin/v2/orders/_attributes"

node do |order|
  {
    :order_total => order.total_orders_amount[0],
    :shipping_fee => order.total_orders_amount[1],
  }
end
