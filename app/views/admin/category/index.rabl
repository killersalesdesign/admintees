collection @categories

attributes :id, :name, :slug, :active, :total_orders

node do |category|
  {
    :total_products => 0,
    :sizes          => category.settings(:options).sizes.join(", "),
    :sizes_count          => category.settings(:options).sizes.count,
    :sizes_weights 	=> category.build_sizes_weights,
    :weights => category.settings(:options).weights,
    :stats => category.stats()
  }
end