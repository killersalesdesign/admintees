attributes :id, :name, :slug, :active, :total_orders

node do |category|
  {
    :sizes => category.settings(:options).sizes,
    :weights => category.settings(:options).weights

  }
end