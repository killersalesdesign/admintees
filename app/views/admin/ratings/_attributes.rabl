attributes :id, :percent, :website_id

node do |rating|
  {
    :website => rating.website.name
  }
end