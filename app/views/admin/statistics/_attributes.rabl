attributes :id, :active, :description, :price, :sales, :title, :url, :profit, :start_number, :slug, :ads_spend, :total_sold, :website_id, :status, :brand_id, :item_color_id, :print_colors, :free_shipping, :international_cutoff, :cname, :tags

node do |product|
  {
    :stats => product.stats(params[:print])
  }
end