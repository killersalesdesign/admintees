object @product
attributes :id, :active, :date, :description, :price, :sales, :title, :url, :profit, :start_number, :slug, :ads_spend, :total_sold, :status, :brand_id, :item_color_id, :print_colors, :free_shipping, :show, :international_cutoff, :additional_item_cost, :cname, :tags, :gift_card, :screenprint_link, :dtg_link, :seo_title, :meta_description

node do |product|
  {
    :stats => product.stats(params[:print]),
    :categories => product.settings(:options).categories,
    :prices => product.settings(:options).prices,
    :stocks => product.stock_list,
    :gallery => product.gallery,
    :mailing => product.settings(:options).mailing
  }
end