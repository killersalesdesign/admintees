object @product

attributes :id, :active, :date, :description, :price, :sales, :title, :url, :profit, :start_number, :ads_spend, :website_id, :status, :brand_id, :item_color_id, :print_colors, :free_shipping, :show, :international_cutoff, :additional_item_cost, :cname, :tags, :bundle, :gift_card, :screenprint_link, :dtg_link, :seo_title, :meta_description

node do |product|
  {
    :categories => product.settings(:options).categories,
    :prices => product.settings(:options).prices,
    :stocks => product.stock_list,
    :gallery => product.gallery,
    :category_by_sizes => product.stats_sizes,
    :stats => product.stats(params[:print]),
    :website_name => product.website.name,
    :product_brand => product.get_brand,
    :product_item_color => product.get_item_color,
    :mailing => product.settings(:options).mailing
  }
end