attributes :id, :active, :description, :price, :sales, :title, :url, :profit, :start_number, :slug, :ads_spend, :total_sold, :website_id, :status, :brand_id, :item_color_id, :print_colors, :free_shipping, :international_cutoff, :cname, :tags, :gift_card

node do |product|
  {
    :product_type => product.settings(:options).categories.join(", "),
    :date => product.date.in_time_zone('Arizona'),
    :stats => product.stats(params[:print]),
    :extra_orders => product.extra_order_data,
    :extra_order_id => product.extra_order.id,
    :extra_order_design => product.extra_order.design,
    :extra_order_batch_id => product.extra_order.batch_id,
    :website_name => product.website.name,
    :product_brand => product.get_brand,
    :product_item_color => product.get_item_color,
    :mailing => product.settings(:options).mailing

  }
end