collection @products

attributes :id, :active, :description, :price, :sales, :title, :url, :profit, :start_number, :slug, :ads_spend, :total_sold, :website_id, :status, :brand_id, :item_color_id, :print_colors, :cname, :tags, :bundle, :gift_card

node do |product|
  {
    :product_type => product.settings(:options).categories.join(", "),
    :date => product.date.in_time_zone('Arizona'),
    :website_name => product.website.name,
    :brand_name => product.get_brand_name,
    :mailing => product.settings(:options).mailing
  }
end