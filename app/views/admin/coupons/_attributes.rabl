attributes  :id, :code, :limit, :usage, :active, :discount

node do |coupon|
  {
    :expiration => coupon.to_server_timzone
  }
end
