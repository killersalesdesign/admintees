attributes  :id, :created_at, :updated_at

node do |abandonment|
  {
  	:items => abandonment.settings(:cart).items,
  	:user_email => abandonment.settings(:cart).user_email,
  	:first_name => abandonment.settings(:cart).first_name,
  	:last_name => abandonment.settings(:cart).last_name,
  	:address => abandonment.settings(:cart).address,
  	:town => abandonment.settings(:cart).town,
  	:state => abandonment.settings(:cart).state,
  	:postal => abandonment.settings(:cart).postal,
  	:country => abandonment.settings(:cart).country,
  	:phone => abandonment.settings(:cart).phone,
  	:shipping_fee => abandonment.settings(:cart).shipping_fee
  }
end
