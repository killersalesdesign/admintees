collection @users
extends "admin/customers/_attributes"

node do |user|
  {
    :created_at    => user.created_at.in_time_zone('Arizona'),
    :total_orders  => user.orders.count
  }
end