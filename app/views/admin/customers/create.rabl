object @user
extends "admin/customers/_attributes"
node do |user|
  {
    :created_at => user.created_at.in_time_zone('Arizona')
  }
end