attributes :id, :name, :slug, :active

node do |category|
  {
    :sizes => category.settings(:options).sizes
  }
end