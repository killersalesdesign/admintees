attributes :id, :order_id, :ip_address, :printed, :shipped, :description, :quantity, :refund, :shipping_fee, :order_total, :created_at

node do |order|
  {
    :email            => order.user.present? ? order.user.email : nil,
    :payment_method   => order.payer_id.present? ? "Paypal" : "Credit Card",
    :note             => order.notes.collect(&:description).join(", "),
    :batch_key        => order.batch_order.nil? ? nil : order.batch_order.batch_key,
    :shipping_details => JSON.parse(order.shipping_info),
    :fulfillment      => order.is_fulfilled?
  }
end

child :ordered_products => :order_items do
  attribute :id, :quantity, :size

  node do |order_item|
    {
      :product_id    => order_item.product_id,
      :product_title => Product.find(order_item.product_id).present? ? Product.find(order_item.product_id).title : nil,
      :category      => order_item.category.present? ? order_item.category.name : nil,
      :category_id   => order_item.category.present? ? order_item.category.id : nil,
      :price         => order_item.price_and_profit[:amount],
      :weight        => order_item.category.present? ? order_item.category.get_weight(order_item.size) : 0,
      :variant_title => "#{order_item.category.present? ? order_item.category.name : nil} / #{order_item.size} / #{order_item.category.present? ? order_item.category.get_weight(order_item.size) : 0}",
      :img_url       => order_item.category_image.product_image(:original)
    }
  end
end