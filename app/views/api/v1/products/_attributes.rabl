attributes :id, :description, :active, :sales, :title, :slug, :status, :print_colors, :tags, :gift_card

node do |product|
  {
    :date     => product.date.in_time_zone('Arizona'),
    :variants  => product.gallery
  }
end