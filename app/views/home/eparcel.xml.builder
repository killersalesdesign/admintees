xml.instruct!
xml.PCMS(:xmlns => "http://www.auspost.com.au/xml/pcms") do
  xml.SendPCMSManifest do
    xml.header do
      xml.TransactionDateTime Time.now.utc.iso8601
      xml.TransactionId 1
      xml.TransactionSequence 1
      xml.ApplicationId "MERCHANT"
    end

    xml.body do
      xml.PCMSManifest do
        xml.MerchantLocationId "AV123"
        xml.ManifestNumber 1
        xml.DateSubmitted Time.now.utc.iso8601
        xml.DateLodged Time.now.utc.iso8601
      
        xml.PCMSConsignment do
          xml.ConsignmentNumber "AV1230022647"
          xml.ChargeCode "S1"
          xml.AdditionalServices do
            xml.ServiceCodeGroup do
              xml.ServiceCodes do
                xml.ServiceCode(109)
              end

              xml.ServiceCodes do
                xml.ServiceCode 104
                xml.ServiceQualifier do
                  xml.ServiceAttributeName "DEL_DATE"
                  xml.Date "2011-07-17"
                end
              end

              xml.ServiceCodes do
                xml.ServiceCode 105
                xml.ServiceQualifier do
                  xml.ServiceAttributeName "DEL_TIME"
                  xml.TimePeriod do
                    xml.StartTime "07:00:00"
                    xml.EndTime "07:00:00"
                  end

                  xml.TimePeriod do
                    xml.StartTime "12:00:00"
                    xml.EndTime "17:00:00"
                  end
                end
              end
            end
          end

          xml.InternalChargebackAccount "Finance"
          xml.ReferenceNo1 "Job #1"
          xml.ReferenceNo2 "Invoice #1"
          xml.DeliveryName "A Post"
          xml.DeliveryCompanyName "Australia Post"

          xml.EmailNotification "Y"
          xml.BusinessPartnerID "1000000011"

          xml.DeliveryAddressLine1 "Reception"
          xml.DeliveryAddressLine2 "Level 10"
          xml.DeliveryAddressLine3 "110 Bourke St"
          xml.DeliveryAddressLine4 "New COrder of Exhibition St"

          xml.DeliveryPhoneNumber "+61391060000"

          xml.DeliveryEmailAddress "a.post@auspost.com.au"
          xml.DeliverySuburb "Melbourne"
          xml.DeliveryStateCode "VIC"
          xml.DeliveryPostcode "3000"
          xml.DeliveryPointIdentifier "234545"
          xml.DeliveryCountryCode "AU"
          xml.DeliveryInstructions "Drop off at reception"
          xml.IsInternationalDelivery "false"
          xml.ReturnName "B Post"
          xml.ReturnAddressLine1 "Reception"
          xml.ReturnAddressLine2 "Ground Floor"
          xml.ReturnAddressLine3 "111 Bourke St"
          xml.ReturnAddressLine4 "Near corner of Exhibition St"
          xml.ReturnSuburb "Melbourne"
          xml.ReturnStateCode "VIC"
          xml.ReturnPostcode "3000"
          xml.ReturnCountryCode "AU"
          xml.CreatedDateTime Time.now.iso8601
          xml.PostChargeToAccount "6616510"
          xml.IsSignatureRequired "Y"
          xml.CTCAmount "0.00"
          xml.DeliverPartConsignment "N"
          xml.ContainsDangerousGoods "false"
          xml.ProfileId "PT_01"
          xml.PCMSDomesticArticle do
            xml.ArticleNumber "AV123002264701000080208"
            xml.BarcodeArticleNumber "0199312650999998AV123002264701000080208|4203000|9212345678|8008140214121212"

            xml.Length "12"
            xml.Width "14"
            xml.Height "16"
            xml.ActualWeight "0.5"
            xml.CubicWeight "0.67"
            xml.ArticleDescription "Article 1"
            xml.IsTransitCoverRequired "Y"
            xml.TransitCoverAmount "100.00"
            xml.ContentsItem do
              xml.GoodsDescription "Goods 1"
              xml.Weight "0.5"
              xml.Quantity "1"
              xml.UnitValue "100.00"
              xml.Value "100"
            end
          end

        end

        xml.PCMSConsignment do
          xml.ConsignmentNumber "AV1230022648"
          xml.ChargeCode "S1"
          xml.DeliveryName "A Post"
          xml.DeliveryAddressLine1 "1/111 Bourke St"
          xml.DeliverySuburb "Melbourne"
          xml.DeliveryStateCode "VIC"
          xml.DeliveryPostcode "3000"
          xml.DeliveryCountryCode "AU"
          xml.IsInternationalDelivery "false"
          xml.ReturnName "Australia Post"
          xml.ReturnAddressLine1 "111 Bourke St"
          xml.ReturnSuburb "Melbourne"
          xml.ReturnStateCode "VIC"
          xml.ReturnPostcode "3000"
          xml.ReturnCountryCode "AU"
          xml.CreatedDateTime Time.now.iso8601
          xml.PostChargeToAccount "6616510"
          xml.IsSignatureRequired "N"
          xml.DeliverPartConsignment "N"
          xml.ContainsDangerousGoods "false"

          xml.PCMSDomesticArticle do
            xml.ArticleNumber "AV123002264701000080208"
            xml.BarcodeArticleNumber "0199312650999998AV123002264701000080208|4203000|9212345678|8008140214121212"
            xml.ActualWeight "0.85"
            xml.IsTransitCoverRequired "N"

            xml.ContentsItem
          end
        end 
      end

      xml.PCMSManifest do
        xml.MerchantLocationId "AV123"
        xml.ManifestNumber "RP2"
        xml.DateSubmitted Time.now.utc.iso8601
        xml.DateLodged Time.now.utc.iso8601

        xml.PCMSConsignment do
          xml.ConsignmentNumber "AV1230000003"
          xml.ChargeCode "PR"
          xml.InternalChargebackAccount "Finance"
          xml.ReferenceNo1 "Job #2"
          xml.ReferenceNo2 "Invoice #2"
          xml.DeliveryName "A Post"
          xml.DeliveryCompanyName "Australia Post"
          xml.DeliveryAddressLine1 "Reception"
          xml.DeliveryAddressLine2 "Level 10"
          xml.DeliveryAddressLine3 "111 Bourke St"
          xml.DeliveryAddressLine4 "Near corner of Exhibition St"
          xml.DeliveryPhoneNumber "+61391060000"
          xml.DeliveryEmailAddress "a.post@auspost.com.au"
          xml.DeliverySuburb "Melbourne"
          xml.DeliveryStateCode "VIC"
          xml.DeliveryPostcode "3000"
          xml.DeliveryCountryCode "AU"
          xml.DeliveryInstructions "Drop off at reception"
          xml.IsInternationalDelivery "false"
          xml.ReturnName "B Post"
          xml.ReturnAddressLine1 "Reception"
          xml.ReturnAddressLine2 "Ground Floor"
          xml.ReturnAddressLine3 "111 Bourke St"
          xml.ReturnAddressLine4 "Near corner of Exhibition St"
          xml.ReturnSuburb "Melbourne"
          xml.ReturnStateCode "VIC"
          xml.ReturnPostcode "3000"
          xml.ReturnCountryCode "AU"
          xml.ReturnDeliveryInstructions "Drop off at reception"
          xml.CreatedDateTime Time.now.iso8601
          xml.PostChargeToAccount "6616510"
          xml.IsSignatureRequired "Y"
          xml.DeliverPartConsignment "N"
          xml.ContainsDangerousGoods "false"
          xml.ProfileId "PT_01"

          xml.PCMSDomesticArticle do
            xml.ArticleNumber "AV123000000301000080208"
            xml.BarcodeArticleNumber "0199312650999998AV123000000301000080208|4203000|9212345678|8008140214121212"
            xml.ActualWeight "0.5"
            xml.IsTransitCoverRequired "Y"
            xml.TransitCoverAmount "100.00"

            xml.ContentsItem do
              xml.GoodsDescription "Goods 1"
            end
          end
        end

        xml.PCMSConsignment do
          xml.ConsignmentNumber "AV1230000004"
          xml.ChargeCode "PR"
          xml.DeliveryName "A Post"
          xml.DeliveryAddressLine1 "1/111 Bourke St"
          xml.DeliverySuburb "Melbourne"
          xml.DeliveryStateCode "VIC"
          xml.DeliveryPostcode "3000"
          xml.DeliveryCountryCode "AU"
          xml.IsInternationalDelivery "false"
          xml.ReturnName "Australia Post"
          xml.ReturnAddressLine1 "111 Bourke St"
          xml.ReturnSuburb "Melbourne"
          xml.ReturnStateCode "VIC"
          xml.ReturnPostcode "3000"
          xml.ReturnCountryCode "AU"
          xml.ReturnDeliveryInstructions "Drop off at reception"
          xml.CreatedDateTime Time.now.iso8601
          xml.IsSignatureRequired "N"
          xml.DeliverPartConsignment "N"
          xml.ContainsDangerousGoods "false"

          xml.PCMSDomesticArticle do
            xml.ArticleNumber "AV123000000301000080208"
            xml.BarcodeArticleNumber "0199312650999998AV123000000301000080208|4203000|9212345678|8008140214121212"
            xml.ActualWeight "0.85"
            xml.IsTransitCoverRequired "N"

            xml.ContentsItem
          end
        end


      end

    end
  end


end