class Product < ActiveRecord::Base
  searchkick autocomplete: ['title']

  extend FriendlyId
  include AlgoliaSearch

  # belongs_to :category
  has_many :product_images
  has_many :orders
  has_many :stocks
  has_many :ordered_products
  belongs_to :website
  has_one :extra_order , :dependent => :destroy

  belongs_to :brand
  belongs_to :item_color

  attr_accessible :id, :active, :date, :description, :price, :sales, :title, :url, :profit, :start_number,
                  :slug, :ads_spend, :website_id, :status, :brand_id, :item_color_id, :print_colors, :free_shipping, :show,
                  :international_cutoff, :additional_item_cost, :cname, :tags, :bundle, :gift_card,
                  :dtg_link, :screenprint_link, :seo_title, :meta_description

  friendly_id :url, use: :slugged

  validates :url, presence: true

  has_settings do |s|
    s.key :options, :defaults => {:categories => [], :prices => [], :mailing => []}
    # s.key :tags, :defaults => {:ptags => []}
  end

  after_create :new_extra_order
  # after_create :algoliasearch_index!
  # after_update :algoliasearch_update_index!

  algoliasearch per_environment: true, enqueue: :trigger_sidekiq_worker do
    attribute :title, :description, :price, :url, :slug, :tags
  end

  def self.trigger_sidekiq_worker(record, remove)
    AlgoliaProductSearchWorker.perform_async(record.id, remove)
  end

  def self.search_by_tags(keyname)
    search = "#{keyname}"
    puts search
    (!keyname) ? [] : Product.where((['products.tags LIKE ?'] * search.split.length).join(' OR '), *search.split(" ").map{|keyword| "%\"#{keyword}\%"})
  end

  def algoliasearch_index!
    AlgoliaProductSearchWorker.perform_async(self.id, false)
    AlgoliaProductSearchWorker.perform_at(self.date, self.id, true)
  end

  def algoliasearch_update_index!
    if self.previous_changes.include?(:date)
      Time.zone = "Brisbane"
      # if self.previous_changes[:date][0] > Time.zone.now
      # elsif self.previous_changes[:date][0] > self.date
      AlgoliaProductSearchWorker.perform_async(self.id, false)
      AlgoliaProductSearchWorker.perform_at(self.date, self.id, true)
    end
  end

  def self.search_by_tags(keyname)
    search = "#{keyname}"
    (!keyname) ? [] : Product.where((['products.tags LIKE ?'] * search.split.length).join(' OR '), *search.split(" ").map{|keyword| "%#{keyword}%"})
  end

  def self.ending
    Product.all.select{|i| i.time_left[:days].between?(0,3)}
  end

  def self.ended
    Product.all.select{|i| i.time_left[:days].between?(0,3)}
  end

  def self.show_sales
    OrderedProduct.select("SUM(quantity) as total_quantity, product_id").group("product_id").order("total_quantity desc")
  end

  def gallery
    gallery = []
    self.product_images.each do |i|
      image = {}
      image[:id]          = i.id
      image[:category_id] = i.category_id
      image[:type]        = i.category_id.nil? ? nil : i.category.name
      image[:price]       = self.price_and_profit_of_style(i.category_id)[:amount]
      image[:original]    = i.product_image
      image[:medium]      = i.product_image(:medium)
      image[:thumb]       = i.product_image(:thumb)
      gallery << image
    end
    gallery
  end

  def get_brand_name
    if self.brand.nil?
      name = ""
    else
      name = self.brand.name
    end
    name
  end

  def time_left
    time_now    = self.date
    t_in_au     = time_now.in_time_zone('Australia/Brisbane')
    offset      = t_in_au.gmt_offset
    time_now_au = self.date + offset

    secs_left   = time_now_au - Time.now
    day_in_secs = 86400
    days        = secs_left / day_in_secs
    hours       = 24 * (days - days.to_i)
    mintues     = 60 * (hours - hours.to_i)

    {:days => days.to_i, :hours => hours.to_i, :mintues => mintues.to_i}
  end

  def price_and_profit_of_style(category_id, product_id = nil)
    current_price = nil
    if product_id.nil?
      self.settings(:options).prices.each do |p|
        current_price = p if p[:category_id].to_i == category_id.to_i
      end
    else
      product = Product.find(product_id)
      product.settings(:options).prices.each do |p|
        current_price = p if p[:category_id].to_i == category_id.to_i
      end
    end
    current_price = {:category_id => category_id.to_i, :type => "", :amount => 0, :profit => 0} if current_price.nil?

    return current_price
  end

  # def orig_price_and_profit_of_style(category_id)

  #   current_price = nil
  #   self.settings(:options).prices.each do |p|
  #     current_price = p if p[:category_id].to_i == category_id.to_i
  #   end
  #   current_price = {:category_id => category_id.to_i, :type => "", :amount => 0, :profit => 0} if current_price.nil?

  #   return current_price
  # end

  def total_sold
    # total = 0
    # self.orders.each do |o|
    #   total += o.ordered_products.sum(:quantity)
    # end

    # total
    # self.orders.sum(:quantity)
    self.ordered_products.sum(:quantity)
  end

  def stats(sort_print)
    stats     = []
    # order_ids = self.orders.where('shipped != ? && refund = ?', 1, false).collect(&:id)
    # products  = OrderedProduct.where(:order_id => order_ids)
    products  = OrderedProduct.where(:product_id => self.id)
    flag = Product.find self.id
    self.settings(:options).prices.each do |p|
      p[:total_sold] = products.where(:category_id => p[:category_id]).sum(:quantity)
      category = Category.find(p[:category_id].to_i)
      all_sizes = category.settings(:options).sizes
      price         = flag.price_and_profit_of_style(p[:category_id].to_i)
      puts "prices", price
      product_sizes = []
      all_sizes.each do |s|
        option         = {:name => s, :sold => products.where(:size => s, :category_id => p[:category_id]).sum(:quantity)}
        option[:extra] = 0
        price[:sizes].each { |p| option[:extra] = p[:extra].nil? ? 0 : p[:extra] if s == p[:name] } unless price[:sizes].blank?
        product_sizes << option
      end
      p[:sizes] = product_sizes
      stats << p
    end
    stats
  end

  def today_stats
    stats = []
    Time.zone = "Brisbane"
    products = OrderedProduct.where("product_id = ? AND created_at >= ? AND created_at <= ?", self.id, Time.zone.now.beginning_of_day, Time.zone.now.end_of_day)
    self.settings(:options).prices.each do |p|
      p[:total_sold]  = products.where(:category_id => p[:category_id]).sum(:quantity)
      category        = Category.find(p[:category_id].to_i)
      all_sizes       = category.settings(:options).sizes
      price           = self.price_and_profit_of_style(p[:category_id].to_i)
      product_sizes   = []
      all_sizes.each do |s|
        option          = {:name => s, :sold => products.where(:size => s, :category_id => p[:category_id]).sum(:quantity)}
        option[:extra]  = 0
        price[:sizes].each { |p| option[:extra] = p[:extra].nil? ? 0 : p[:extra] if s == p[:name] } unless price[:sizes].blank?
        product_sizes << option
      end
      p[:sizes] = product_sizes
      stats << p
    end
    stats
  end


  def stats_sizes
    stats     = []
    order_ids = self.orders.where(:shipped => 0).collect(&:id)
    products  = OrderedProduct.where(:order_id => order_ids)
    self.settings(:options).prices.each do |p|
      p[:total_sold] = products.where(:category_id => p[:category_id]).sum(:quantity)
      category = Category.find(p[:category_id].to_i)
      all_sizes = category.settings(:options).sizes
      product_sizes = []
      all_sizes.each do |s|
          product_sizes << {:size_name => s, :total => products.where(:size => s, :category_id => p[:category_id]).sum(:quantity)}
      end
      p[:sizes] = product_sizes
      stats << p
    end
    stats
  end

  def extra_order_data
    extra_order = self.extra_order
    if extra_order.nil?
      new_data = ExtraOrder.new
      new_data.product_id = self.id

      new_sizes = [
        {:category => 'Mens Tee', :S => 0, :M => 0, :L => 0, :XL => 0, :XXL => 0, :XXXL => 0},
        {:category => 'Womens Tee', :S => 0, :M => 0, :L => 0, :XL => 0, :XXL => 0, :XXXL => 0},
        {:category => 'Hoodie', :S => 0, :M => 0, :L => 0, :XL => 0, :XXL => 0, :XXXL => 0, :XS => 0, :XXS => 0}
      ]
      new_data.settings(:options).sizes = new_sizes
      new_data.save

      return new_data
    else
      sizes = []
      extra_order.settings(:options).sizes.each do |p|
        sizes << p
      end
      return sizes
    end
  end

  def new_extra_order
    new_data = ExtraOrder.new
    new_data.product_id = self.id
    new_sizes = []
    allCategories = Category.all
    allCategories.each do |category|
      all_sizes = category.settings(:options).sizes
      tmpSizes = []
      all_sizes.each do |s|
        tmpSizes << {:size_name => s, :sold => 0}
      end
      new_sizes << {:category_name => category.name, :category_id => category.id, :sizes =>  tmpSizes}
    end
    new_data.settings(:options).sizes = new_sizes
    new_data.save
  end

  def stock_list
    in_stocks = []
    self.stocks.group(:category_id).each do |category|
        obj = {:type => category.category.name, :category_id => category.category_id}
        obj[:sizes] = []
      self.stocks.where(:category_id => category.category_id).each do |stock|
        obj[:sizes] << {label: stock.size, value: stock.amount, id: stock.id}
      end
      in_stocks << obj
    end
    in_stocks
  end

  def get_brand
    brand = ""
    brand = self.brand.name unless self.brand.nil?
  end

  def get_item_color
    item_color = ""
    item_color = self.item_color.name unless self.item_color.nil?
  end

  def self.filters(filters, limit = 0)
    product = Product

    if filters[:inactive] == "true"
      product = product.where("active = ?", false)
    else
      product = product.where("active = ?", true)
      product = product.where("status = ?", filters[:status])
    end

    product = product.where("title like ?", "%#{filters[:keyword]}%") unless filters[:keyword].blank?
    product = product.where("website_id = ?", filters[:website]) if filters[:website] == "1" || filters[:website] == "2"
    product = product.page(filters[:page]).per(limit) if limit != 0

    product.order("id desc")
  end

  def sales_today
    return self.sales_on(Date.today.beginning_of_day, Date.today.end_of_day)
  end

  def sales_yesterday
    return self.sales_on(Date.yesterday.beginning_of_day, Date.yesterday.end_of_day)
  end

  def sales_all_time
    return self.sales_on(self.created_at.beginning_of_day, Date.today.end_of_day)
  end

  def sales_on(from, to)
    awrders = self.orders.where("DATE(created_at) BETWEEN ? AND ?", from, to)
    profit = 0
    customers_count = awrders.collect{|o| o.user_id}.uniq.count
    product_count = 0
    order_count = awrders.count
    awrders.each do |o|
      o.ordered_products.each do |op|
        product_count += op.quantity
        # if !self.price_and_profit_of_style(op.category_id)[:total_sold].nil? && self.price_and_profit_of_style(op.category_id)[:total_sold] > 0
        profit += op.price_and_profit[:profit]
        # end
      end
    end

    orders_count = self.total_sold

    return {profit: profit, customers: customers_count, products: product_count, orders: order_count}
  end

  def sales_per_hour(current_day)
    # current_day = self.orders.first.created_at + 24.hours
    puts current_day.inspect
    current_time = current_day - 24.hours
    items = []
    hours = []
    begin

      hours.push current_time.strftime("%l%p")
      orders = self.orders.where('DATE(created_at) BETWEEN ? and ?', current_time, current_time + 1.hour)
      current_time += 1.hour

      items.push orders.count

    end while current_time < current_day
    return {datasets: [data: items], labels: hours}
  end

  def sales_by_type(from, to)
    categories = []
    catids = []
    # puts self.orders.count
    self.orders.each do |order|

      order.ordered_products.each do |ordered_product|
        category_id = ordered_product.category_id
        if catids.exclude? category_id
          catids.push category_id
          category_name = self.price_and_profit_of_style(category_id)[:type]
          categories.push id: category_id, name: category_name
        end
      end
    end

    catids = catids.uniq
    dates = []
    items = []



    catids.each do |catid|
      current_day = from.beginning_of_day
      data = []
      begin

        orders = OrderedProduct.where("category_id = (?) and DATE(created_at) BETWEEN ? and ?", catid, current_day, current_day.end_of_day).collect{|op| op.order_id}.uniq.count

        if orders
          data.push orders

        else
          data.push 0
        end

        current_day += 1.day
        dates.push current_day.strftime("%B %d") unless dates.include? current_day.strftime("%B %d")
      end while current_day < to.beginning_of_day
      items.push data: data, category_id: catid
    end
    return {datasets: items, labels: dates, categories: categories}
  end

  def self.stats_amount_sold(from = 10.days.ago, to = Date.today)
    orders = OrderedProduct.where("DATE(created_at) BETWEEN ? AND ?", from.to_datetime.beginning_of_day.in_time_zone("Magadan"), to.to_datetime.end_of_day.in_time_zone("Magadan"))
    stats = {:total_amount => 0, :total_sold => 0}
    orders.each do |order|
     stats[:total_sold] += order.quantity
     order.product.settings(:options).prices.each do |p|
      begin
      stats[:total_amount] += p[:amount]
      rescue
       next
      end
     end
    end
    stats
  end

  def self.revive_inactive_products(record_counts = nil, pid = nil, forInStocks = false)
    price_change = [
      {category_id: 82, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 81, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 80, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 79, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 78, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 77, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 76, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 75, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 74, price: 40, dship: 8.35, iship: 8.35},
      {category_id: 73, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 72, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 71, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 70, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 69, price: 60, dship: 10.2, iship: 10.2},
      {category_id: 68, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 67, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 66, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 65, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 64, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 63, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 62, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 61, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 60, price: 40, dship: 3, iship: 3},
      {category_id: 59, price: 40, dship: 3, iship: 3},
      {category_id: 58, price: 35, dship: 3, iship: 3},
      {category_id: 56, price: 35, dship: 3, iship: 3},
      {category_id: 55, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 54, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 53, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 52, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 51, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 50, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 49, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 48, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 47, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 46, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 45, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 44, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 42, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 41, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 40, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 39, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 38, price: 25, dship: 8.35, iship: 8.35},
      {category_id: 37, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 36, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 35, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 34, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 33, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 32, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 31, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 30, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 29, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 28, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 24, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 23, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 22, price: 60, dship: 10.2, iship: 10.2},
      {category_id: 21, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 20, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 19, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 18, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 17, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 14, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 13, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 12, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 11, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 9, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 8, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 7, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 6, price: 50, dship: 10.2, iship: 10.2},
      {category_id: 5, price: 30, dship: 8.35, iship: 8.35},
      {category_id: 4, price: 30, dship: 8.35, iship: 8.35}
    ]

    if record_counts
      if pid
        if forInStocks
          # IN_STOCK status is 2
          products = Product.where("id = ? AND status = ?", pid, 2).limit(record_counts)
        else
          products = Product.where("id = ? AND active = ?", pid, false).limit(record_counts)
        end
      else
        if forInStocks
          products = Product.where(:status => 2).limit(record_counts)
        else
          products = Product.where(:active => false).limit(record_counts)
        end
      end
    else
      if pid
        if forInStocks
          products = Product.where("id = ? AND status = ?", pid, 2)
        else
          products = Product.where("id = ? AND active = ?", pid, false)
        end
      else
        if forInStocks
          products = Product.where(:status => 2)
        else
          products = Product.where(:active => false)
        end
      end
    end

    if products
      File.open("#{Rails.root}/tmp/revive.out", "a") do |f|
        f.puts "####################################"
        f.puts "#{Time.now}"

        products.each do |p|

          # updating prices
          new_prices = []
          prices = p.settings(:options).prices
          unless prices.empty?
            f.puts "Product: #{p.id}"
            puts "Product: #{p.id}"
            prices.each do |pr|
              cat_id  = pr["category_id"].nil? ? pr[:category_id] : pr["category_id"]
              amt     = pr["amount"].nil? ? pr[:amount] : pr["amount"]
              dship   = pr["postage"].nil? ? pr[:postage] : pr["postage"]
              iship   = pr["i_postage"].nil? ? pr[:i_postage] : pr["i_postage"]

              puts "pr category id: #{cat_id}"
              puts "amount: #{amt}, dship: #{dship}, iship: #{iship}"

              res   = price_change.select {|s| s[:category_id] == cat_id}
              unless res.empty?
                puts 'found!'
                puts res.inspect
                amt   = res.first[:price]
                dship = res.first[:dship]
                iship = res.first[:iship]
              end # end unless res.empty?

              amtFound = false
              postageFound = false
              ipostageFound = false
              new_pr = {}
              pr.each do |key, value|
                if key.to_s == "amount"
                  value = amt
                  amtFound = true
                end # end key == "amount"
                if key.to_s == "postage"
                  value = dship
                  postageFound = true
                end
                if key.to_s == "i_postage"
                  value = iship
                  ipostageFound = true
                end
                new_pr[:"#{key}"] = value
              end # end pr.each

              new_pr[:amount] = amt if not amtFound
              new_pr[:postage] = dship if not postageFound
              new_pr[:i_postage] = iship if not ipostageFound

              new_prices << new_pr
            end # end prices.each

            p.settings(:options).prices = new_prices
          end # end unless prices.empty?

          p.active  = true
          p.show    = true
          p.status  = 1

          if p.stocks.length == 0
            prices = p.settings(:options).prices
            if prices
              prices.each do |pr|
                category = Category.find(pr[:category_id])
                if category
                  sizes = category.settings(:options).sizes
                  if sizes
                    sizes.each do |s|
                      begin
                        Stock.create :size => "#{s}", :category_id => pr[:category_id], :amount => 100, :product_id => p.id
                      rescue Exception => e
                        f.puts "Unable to create stocks for #{p.id}"
                        f.puts "Error is"
                        f.puts e.inspect
                      end # end begin rescue
                    end # end sizes.each
                  end # end if sizes
                end # end if category
              end # end prices.each
            end # end if prices

          else
            p.stocks.each do |st|
              st.amount = 100
              if st.save
                f.puts "Successfully updated stock #{st.id}"
              else
                f.puts "Something went wrong while updating stock #{st.id}"
              end # end st.save
            end # end p.stocks.each
          end # end if p.stocks.length == 0

          if p.save!
            f.puts "Successfully updated product #{p.id}"
          else
            f.puts "Unable to save #{p.id}"
          end # end p.save
        end # end products.each

        f.puts "Process done!"
        f.puts "####################################"
      end # end File.open

    end # end if products
  end

end
