class MailTracker < ActiveRecord::Base
  attr_accessible :action_count, :action_type, :time_frame

  def self.get_stats(date_frame)
  	case date_frame
  	when "today"
  		from	= Date.today.beginning_of_day
  		to 		= Date.today.end_of_day
  	when "week"
  		from	= 7.days.ago.beginning_of_day
  		to 		= Date.today.end_of_day
  	when "month"
			from 	= 30.days.ago.beginning_of_day
			to 		= Date.today.end_of_day
  	when "year"
  		from 	= Date.today.beginning_of_year.beginning_of_day
			to 		= Date.today.end_of_day
		end

		action_types = ["open","click","rejoins"]

		tracked_mails = {}

		action_types.each do |action|
			tracked_mails[action] = {}
			result = MailTracker.where("DATE(created_at) BETWEEN ? AND ? AND action_type = ?", from, to, action)

			time_frames = ["30-minutes","1-day","10-days"]

			
			time_frames.each do |tf|
				time_frame_result = result.where("time_frame = ?", tf)

				action_count = 0
				time_frame_result.each do |tf_result|
					action_count += tf_result.action_count
				end

				case tf
				when "30-minutes"
					tf = "thirty_minutes"
				when "1-day"
					tf = "one_day"
				when "10-days"
					tf = "ten_days"
				end
					
				tracked_mails[action][tf] = action_count
			end
		end

		return tracked_mails

  end

end
