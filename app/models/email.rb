class Email < ActiveRecord::Base
  attr_accessible :subject, :name, :status, :email_type
  has_many :sent_emails
end
