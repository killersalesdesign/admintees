class UnfinishedOrder < ActiveRecord::Base
  attr_accessible :category, :price, :product_id, :qty, :size, :style, :tracking_id, :website_id, :recovered, :recovered_date

  has_settings do |s|
    s.key :cart, :defaults => {:items => [], :user_email => "", :first_name => "", :last_name => "", :address => "", :town => "", :state => "", :postal => "", :country => "", :phone => "", :shipping_fee => 0}
  end

  def self.get_stats(date_frame)
  	data = {}

  	abandons 									= UnfinishedOrder.abandons(date_frame, false)
  	data[:abandons] 					= abandons[:count]
  	data[:lost_revenue] 			= abandons[:revenue]
  	data[:abandon_rate]				= abandons[:percentage]

  	rejoins 									= UnfinishedOrder.abandons(date_frame, true)
  	data[:rejoins]						= rejoins[:count]
  	data[:recovered_revenue] 	= rejoins[:revenue]
  	data[:rejoin_rate] 				= rejoins[:percentage]

  	return data
  end

  def self.abandons(date_frame, recovered)
  	case date_frame
  	when 'today'
  		from	= Date.today.beginning_of_day
  		to 		= Date.today.end_of_day
  	when 'yesterday'
  		from 	= Date.yesterday.beginning_of_day
  		to 		= Date.yesterday.end_of_day
  	when 'week'
  		from 	= 7.days.ago.beginning_of_day
  		to 		= Date.today.end_of_day
  	when 'month'
  		from 	= Date.today.beginning_of_month.beginning_of_day
  		to 		= Date.today.end_of_day
  	when 'alltime'
  		from 	= UnfinishedOrder.first.created_at.beginning_of_day
  		to 		= Date.today.end_of_day
		end

		# get number of CC views from GA
		analytics = GoogleAnalytics.new(
		  start_date: from.strftime("%Y-%m-%d"),
		  end_date: to.strftime("%Y-%m-%d")
		)
		visit_count = analytics.get_visits_of_page("/cart/creditcard", from.strftime("%Y-%m-%d"), to.strftime("%Y-%m-%d"))

		total_visits = 0
		visit_count.data.rows.each do |row|
			if row[0] == "/cart/creditcard"
				total_visits += row[1].to_i
			end
		end

		result 	= abandons_date_range(from, to, recovered)
		# orders	= Order.where("DATE(created_at) BETWEEN ? AND ?", from, to)
		
		puts "date frameeeeeee: #{date_frame}"
		puts "recoveredddddddd: #{recovered}"
		puts "total visitsssss: #{total_visits}"
		puts "resulttttttttttt: #{result.count}"

		percent = 0

		# rejoins
		if recovered == true
			abandons 	= abandons_date_range(from, to, false)
			if abandons.count > 0
				percent = (result.count.to_f / abandons.count) * 100
			end
		# abandons
		else
			if total_visits > 0
				orders	= Order.where("DATE(created_at) BETWEEN ? AND ?", from, to).count
				# percent = (result.count.to_f / (orders.count + result.count)) * 100
				# percent = (result.count.to_f / total_visits) * 100
				percent = (orders / total_visits) * 100
			end
		end

		revenue = 0
		result.each do |order|
			order.settings(:cart).items.each do |item|
				revenue += item[:quantity] * item[:info][:amount]
			end
		end

		actual 							= {}
		actual[:count] 			= result.count()
		actual[:revenue] 		= revenue
		actual[:percentage] = percent.round(2)

		return actual
  end

  def self.abandons_date_range(from, to, recovered)
  	UnfinishedOrder.where("DATE(created_at) BETWEEN ? AND ? AND recovered = ?", from, to, recovered)
  end
end
