class Giftcard < ActiveRecord::Base
  attr_accessible :key_code, :order_id, :is_used, :amount, :category_id, :product_id
  belongs_to :orders
end
