class FbPixel < ActiveRecord::Base
  DEFAULT_ID = {landing: 733031176719197, thankyou: 6013515465699}

  belongs_to :product
  attr_accessible :landing_id, :thank_you_id, :product_id
end
