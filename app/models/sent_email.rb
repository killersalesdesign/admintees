class SentEmail < ActiveRecord::Base
  attr_accessible :email_id, :order_id, :product_id

  belongs_to :order
  belongs_to :email

  has_many :orders
  has_many :emails
end
