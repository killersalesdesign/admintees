class ExtraOrder < ActiveRecord::Base
  attr_accessible :product_id, :design, :batch_id
  belongs_to :product

  has_settings do |s|
    s.key :options, :defaults => {:sizes => []}
  end
end
