class CategoryImage < ActiveRecord::Base
  belongs_to :category
  attr_accessible :color, :image, :label

  has_attached_file :image,
  :styles => { :medium => "415x500>", :thumb => "100x100>" },
  :default_url => "/assets/thumbnail-default.jpg"

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
