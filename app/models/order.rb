require 'barby'
require 'barby/barcode/code_39'
require 'barby/outputter/png_outputter'

class Order < ActiveRecord::Base

  belongs_to :product
  belongs_to :user
  belongs_to :website
  belongs_to :coupon
  belongs_to :batch_order
  has_many :ordered_products, :dependent => :destroy
  has_many :giftcards, :dependent => :destroy
  has_many :sent_emails
  has_many :notes

  # attr_accessible :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :category_id, :size, :quantity

  attr_accessible :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :shipping_info, :printed, :shipped, :description, :order_id, :website_id, :quantity, :refund, :coupon_id, :tracking_key, :barcode, :batch_key,
                  :shipping_fee, :order_total

  has_attached_file :barcode,
  :storage       => :s3,
  :url           => ':s3_alias_url',
  :s3_host_alias => ENV['AWS_ASSET_HOST'],
  :bucket        => ENV['FOG_DIRECTORY'],
  :path          => ":class/:attachment/:id_partition/:style/:filename",
  :s3_credentials => {
    :access_key_id     => ENV['AWS_ACCESS_KEY_ID'],
    :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
  }

  # before_update :send_mail

  def send_order_mail(password = nil)
    UserMailer.delay_for(5.minutes).send_on_order(self.id, password)
  end

  def total_orders_amount
    if self.shipping_fee.present? && self.order_total.present?
      return [self.order_total, self.shipping_fee]
    else
      subtotal = 0
      quantity = 0
      shipping = 0
      cut_off  = 0

      self.ordered_products.each do |p|
        product   = Product.find(p.product_id) rescue nil
        if product.present?
          puts "productttttttttt: #{product.id}"
          info      = product.price_and_profit_of_style(p.category_id)
          subtotal += p.quantity * info[:amount]
          quantity += p.quantity
          # shipping = info[:postage].to_f if info[:postage] && info[:postage] > shipping
          shipping = info[:postage] and info[:postage] > shipping ? info[:postage] : shipping
          cut_off  = product.free_shipping > cut_off ? product.free_shipping : cut_off
        end
      end

      subtotal = subtotal - (subtotal * (self.coupon.discount.to_f / 100)) unless self.coupon_id.nil?
      shipping = cut_off > quantity ? shipping : 0
      # total    = subtotal + shipping
      total    = subtotal

      self.order_total = total
      self.shipping_fee = shipping
      self.save

      return [total, shipping]
    end
  end

  def generate_barcode
    # create dir if not exist
    directory_name = "#{Rails.root}/public/barcodes"
    Dir.mkdir(directory_name) unless File.exists?(directory_name)

    return if self.barcode.present?

    barcode39 = Barby::Code39.new(self.order_id)
    File.open("public/barcodes/barcode_#{self.order_id}.png", 'w') do |f|
      f.write barcode39.to_png(:height => 70, :margin => 2, :xdim => 2, :ydim => 2)
    end
    self.barcode = File.open("public/barcodes/barcode_#{self.order_id}.png")
    Rails.logger.debug ">>>>----> #{self.inspect}"
    if self.save
      self.delay_for(1.minute).delete_temp_barcode(self.order_id)
    end
  end

  def delete_temp_barcode(order_id)
    FileUtils.rm_rf("#{Rails.root}/public/barcodes/barcode_#{order_id}.png")
  end

  def purchase
    response = EXPRESS_GATEWAY.purchase(price_in_cents, express_purchase_options)
    # response.success?
    response.success?
  end

  def price_in_cents
    total   = 0
    product = self.product
    self.ordered_products.each do |p|
      total += p.quantity * product.price_and_profit_of_style(p.category_id)[:amount]
    end

    (total * 100).round
  end

  def self.update_quantity
    Order.all.each do |o|
      qty = o.ordered_products.sum(:quantity)
      o.quantity = qty
      o.save
    end
  end

  def get_weight
    weight = 0
    self.ordered_products.each do |o|
      category_weight = 0
      o.category.settings(:options).weights.each do |w|
        category_weight = w[:weight].to_f if w[:size].strip == o.size.strip
      end
      weight += (category_weight * o.quantity)
    end
    weight
  end

  def get_product_names

    if self.product_id.nil?
      return ''
    elsif self.product_id != 0
      return self.product.title
    else
      pnames = []
      self.ordered_products.each do |o|
        pnames << o.product.title
      end
      return pnames
    end

  end

  def total_amount
    subtotal = 0
    quantity = 0
    shipping = 0
    # product  = self.product

    self.ordered_products.each do |p|
      product   = Product.find(p.product_id)
      info      = product.price_and_profit_of_style(p.category_id)
      subtotal += p.quantity * info[:amount]
      quantity += p.quantity
      shipping = info[:postage].to_f if info[:postage] && info[:postage] > shipping
    end

    subtotal = subtotal - (subtotal * (self.coupon.discount.to_f / 100)) unless self.coupon_id.nil?
    if self.product_id.nil?
      if self.ordered_products.count >= 2
        shipping = 0
      end
    else
      shipping = self.product.free_shipping > quantity ? shipping : 0
    end
    total    = subtotal
    return total + shipping
  end

  def get_sent_emails
    emails = []
    self.sent_emails.each do |sent|
      email = Email.find(sent.email_id)

      email_obj = {}
      email_obj[:name] = email.name
      email_obj[:sent] = sent.created_at.in_time_zone('Brisbane').strftime("%B %d, %Y %I:%M:%S %p")
      emails.push email_obj
    end
    emails
  end

  def is_fulfilled?
    self.shipped == 1 ? "fulfilled" : "unfulfilled"
  end

  private

  def express_purchase_options
    {
      :ip => ip_address,
      :currency => 'AUD',
      :token => token,
      :payer_id => payer_id
    }
  end

  def send_mail
    if self.printed == true && changed_attributes["printed"] == false
      UserMailer.delay.send_on_print(self.id)
    end

    if self.shipped == 1 && changed_attributes["shipped"] == false
      UserMailer.delay.send_on_shipping(self.id)
    end
  end

  def self.to_csv
    CSV.generate do |csv|
      csv << ["Email"]
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file)
    @orderUpdated = []
    CSV.foreach(file.path, headers: true) do |row|

      @dataField = row.to_hash
      puts @dataField["Reference 1"]
      @order = Order.where(:order_id => @dataField["Reference 1"]).first
      if @order
        # puts @dataField
        @order.shipped = 1
        @order.tracking_key = @dataField["Consignment No."]
        @order.save

        # send email
        # Product Tracking
        email = Email.find_by_email_type("tracking")
        from  = "FanTees Family <support@fantees.com.au>"
        UserMailer.product_email_tracking(@order.user, from, email.subject).deliver
        SentEmail.create order_id: @order.id, email_id: email.id, product_id: @order.product.id

        # send `Product Arrived` email 7 days after
        arrived_email = Email.find_by_email_type("arrived")
        ProductsEmailWorker.perform_in(7.days, @order.user.id, arrived_email.id, @order.product.id, @order.id)

        @orderUpdated << {"order_id" => @order.id, "csv" => @dataField }
      else
        @orderUpdated << {"order_id" => 0, "csv" => @dataField }
      end
    end

    return @orderUpdated

  end

  def self.import_v2(file)
    orderUpdated = []
    begin

      CSV.foreach(file.path, headers: true) do |row|

        row.each { |r| r[0] = r[0].parameterize.underscore }

        dataField = row.to_hash
        order     = Order.find_by_order_id dataField["order_id"]

        unless order.nil?
          order.shipped = 1
          order.tracking_key = dataField["tracking_id"]
          order.save

          email = Email.find_by_email_type("tracking")


          from  = "FanTees Family <support@fantees.com.au>"
          UserMailer.delay.product_email_tracking(order.user, from, email.subject)
          SentEmail.create order_id: order.id, email_id: email.id, product_id: order.product.id

          orderUpdated << {"order_id" => order.id, "csv" => dataField }
        else
          orderUpdated << {"order_id" => 0, "csv" => dataField }
        end
      end
      return orderUpdated
    rescue
      return orderUpdated
    end

  end

end
