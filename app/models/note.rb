class Note < ActiveRecord::Base
  belongs_to :user
  belongs_to :website
  belongs_to :order
  attr_accessible :description, :to, :website_id, :user_id, :order_id, :note_type

  # note types
  # 0 - user
  # 1 - order

end
