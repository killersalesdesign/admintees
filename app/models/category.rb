class Category < ActiveRecord::Base
  has_many :orders
  has_many :ordered_products
  has_many :product_images
  has_many :stocks
  attr_accessible :name, :slug, :active

  before_validation :generate_slug

  has_settings do |s|
    # s.key :options, :defaults => {:sizes => ["S", "M", "L", "XL", "XXL"]}
    s.key :options, :defaults => {:sizes => ["S", "M", "L", "XL", "XXL"], :weights => [{:size=>"S",:weight=>"0"},{:size=>"M",:weight=>"0"},{:size=>"L",:weight=>"0"},{:size=>"XL",:weight=>"0"},{:size=>"XXL",:weight=>"0"}]}
  end

  def total_products
    self.products.count
  end

  def total_orders
    self.ordered_products.count
  end

  def build_sizes_weights
    str = ""
    self.settings(:options).sizes.each do |s|
      weight = 0
      res = self.settings(:options).weights.find {|item| item["size"]==s}
      weight = res["weight"] unless res.nil?

      if str == ""
        str = "#{s}[#{weight}]"
      else
        str = "#{str}, #{s}[#{weight}]"
      end
    end
    str
  end

  def get_weight(size)
    begin
      self.settings(:options).weights.each do |w|
        return w["weight"] if w["size"] == size
      end
      return 0
    rescue
      return 0
    end
  end

  def stats()

    # self.products
    # order_ids = self.orders.collect(&:id)


      products  = self.ordered_products
      all_sizes = self.settings(:options).sizes

      product_sizes = []
      all_sizes.each do |s|
        product_sizes << {:name => s, :sold => products.where(:size => s).sum(:quantity)}
      end

      return product_sizes

  end

  def self.per_category_sizes_sold
    result = []
    Category.all.each do |cat|
      sizes_sold = {}
      total_qty_sold = 0

      orders = cat.ordered_products
      orders.each do |op|
        this_size = op.size.strip

        if this_size != ""
          sizes_sold[this_size] = 0 unless sizes_sold[this_size]
          sizes_sold[this_size] += op.quantity

          total_qty_sold += op.quantity
        end
      end

      result << {category:cat.name, qty_sold:total_qty_sold, sizes:sizes_sold}

    end

    result
  end

  # will generate a file called `testing.txt`
  # that will consist of the sold per category
  # per size
  def self.sold_per_category_per_size
    f = File.new("testing.txt", "w")
    Category.all.each do |cat|
      # sales = 0
      # sizes = {}
      total_qty_sold = 0
      sizes_qty = {}

      orders = cat.ordered_products
      orders.each do |o|
        this_size = o.size.strip

        total_qty_sold += o.quantity
        sizes_qty[this_size] = 0 unless sizes_qty[this_size]
        sizes_qty[this_size] += o.quantity

        # unless o.order.nil?
        #   unless o.product.nil?
        #     prices = o.product.settings(:options).prices
        #     prices.each do |p|
        #       if p[:category_id] == cat.id
        #         # record here
        #         total_qty_sold += o.quantity
        #         unless sizes_qty[this_size]
        #           sizes_qty[this_size] = 0
        #         end
        #         sizes_qty[this_size] = sizes_qty[this_size] + o.quantity
        #         break
        #         # sales = sales + (p[:amount] * o.quantity)
        #         # unless sizes[this_size]
        #         #   sizes[this_size] = 0
        #         # end
        #         # sizes[this_size] = sizes[this_size] + (p[:amount] * o.quantity)
        #       end
        #     end
        #   end
        # end
      end

      f << "=======================\n"
      f << "Category: #{cat.name}\n"
      # f << "Overall Sales: #{number_to_currency(sales)}\n"
      # f << "Sizes: \n"

      # sizes.each do |s, v|
      #   f << "#{s} - #{number_to_currency(v)}\n"
      # end

      f << "Overall Items: #{total_qty_sold}\n"
      sizes_qty.each do |s, q|
        f << "#{s} - #{q}\n"
      end

      f << "\n"

    end

    f.close
  end

  def self.sold_per_category_per_size
    f = File.new("testing.txt", "w")
    Category.all.each do |cat|
      # sales = 0
      # sizes = {}
      total_qty_sold = 0
      sizes_qty = {}

      orders = cat.ordered_products
      orders.each do |o|
        this_size = o.size.strip

        if this_size != ""
          total_qty_sold += o.quantity
          sizes_qty[this_size] = 0 unless sizes_qty[this_size]
          sizes_qty[this_size] += o.quantity
        end

        # unless o.order.nil?
        #   unless o.product.nil?
        #     prices = o.product.settings(:options).prices
        #     prices.each do |p|
        #       if p[:category_id] == cat.id
        #         # record here
        #         total_qty_sold += o.quantity
        #         unless sizes_qty[this_size]
        #           sizes_qty[this_size] = 0
        #         end
        #         sizes_qty[this_size] = sizes_qty[this_size] + o.quantity
        #         break
        #         # sales = sales + (p[:amount] * o.quantity)
        #         # unless sizes[this_size]
        #         #   sizes[this_size] = 0
        #         # end
        #         # sizes[this_size] = sizes[this_size] + (p[:amount] * o.quantity)
        #       end
        #     end
        #   end
        # end
      end

      f << "=======================\n"
      f << "Category: #{cat.name}\n"
      # f << "Overall Sales: #{number_to_currency(sales)}\n"
      # f << "Sizes: \n"

      # sizes.each do |s, v|
      #   f << "#{s} - #{number_to_currency(v)}\n"
      # end

      f << "Overall Items: #{total_qty_sold}\n"
      sizes_qty.each do |s, q|
        f << "#{s} - #{q}\n"
      end

      f << "\n"

    end

    f.close
  end

  private

  def generate_slug
    self.slug ||= name.gsub("<br />", " ").parameterize
  end


end
