class Batch < ActiveRecord::Base
  attr_accessible :batch_key, :product_id, :batch_stats, :brand, :colour, :product_name, :is_auto
  after_create :email_to_trello

  def email_to_trello
    UserMailer.batch_email_to_trello(self.id).deliver
  end
end
