class Notification < ActiveRecord::Base
  attr_accessible :message, :ref_id, :ref_type, :seen
  belongs_to :admin
end
