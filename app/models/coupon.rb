class Coupon < ActiveRecord::Base
  has_many :orders

  attr_accessible :code, :expiration, :limit, :usage, :active, :discount

  validates :code, :presence => true
  validates :discount, :presence => true

  def to_server_timzone
    unless self.expiration.nil?
      self.expiration.in_time_zone('Arizona')
    else
      self.expiration
    end
  end
end
