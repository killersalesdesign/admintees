class GoogleAnalytic < ActiveRecord::Base
  DEFAULT_ID = {
    landing: 'UA-68159572-1',
    thankyou: 'UA-68159572-1'
  }

  belongs_to :product
  attr_accessible :landing_id, :thank_you_id, :product_id

  before_create :insert_default_id_if_nil

  private

  def insert_default_id_if_nil
    self.landing_id   = GoogleAnalytic::DEFAULT_ID[:landing] unless self.landing_id.present?
    self.thank_you_id = GoogleAnalytic::DEFAULT_ID[:thankyou] unless self.thank_you_id.present?
  end
end
