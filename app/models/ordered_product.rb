class OrderedProduct < ActiveRecord::Base
  belongs_to :category
  belongs_to :order
  belongs_to :product
  attr_accessible :quantity, :size, :category_id, :order_id, :created_at, :product_id

  before_create :check_stocks
  after_update :update_order_update_at

  def product
    self.order.product
  end

  def price_and_profit
    result = ""
    if self.product_id
      product = Product.find self.product_id
      result  = product.price_and_profit_of_style(self.category_id)
    else
      result = self.order.product.price_and_profit_of_style(self.category_id)
    end

    result
  end

  def title
    title = self.order.product.title
    title = Product.find(self.product_id).title unless self.product_id.nil?

    title
  end

  def category_image
    unless self.product_id.nil?
      # image = self.product.product_images.where(:category_id => self.category_id)
      image = Product.find(self.product_id).product_images.where(:category_id => self.category_id)
    else
      image = self.order.product.product_images.where(:category_id => self.category_id)
    end

    image.sample || self.order.product.product_images.first
  end

  def check_stocks
    if self.product.status != 0
      stock = self.product.stocks.where(:size => self.size, :category_id => self.category_id).first
      unless stock.nil?
        if stock.amount > 0 && stock.amount >= self.quantity
          stock.decrement(:amount, self.quantity)
          stock.save
        end
      end
    end
    return true
  end

  private

  def update_order_update_at
    order            = self.order
    order.updated_at = Time.zone.now
    order.save
  end
end
