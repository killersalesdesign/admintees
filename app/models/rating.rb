class Rating < ActiveRecord::Base
  belongs_to :website

  attr_accessible :percent, :website_id
end
