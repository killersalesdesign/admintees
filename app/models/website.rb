class Website < ActiveRecord::Base
  has_many :products
  has_many :orders
  has_many :ratings

  attr_accessible :name, :url
end
