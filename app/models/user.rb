class User < ActiveRecord::Base
  has_many :orders, :dependent => :destroy
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :first_name, :last_name, :address, :avatar, :phone

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "50x50>" }, :default_url => "/assets/thumbnail-default.jpg"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  # attr_accessible :title, :body

  has_settings do |s|
    s.key :options, :defaults => {:emails_received => []}
  end

  before_create :humanize_names

  def search_orders(params)
    unless params.blank?
      results = self.orders
      params = ActiveSupport::JSON.decode(params)

      if params[:name]
        p_ids   = Product.where("title LIKE ?", "%#{params[:name]}%").collect(&:id)
        results = results.where(:id => products)
      elsif params[:date]
      else
      end
    else
      results = self.orders
    end

    return results.order("created_at DESC")
  end

  def self.user_orders(filters)
    limit = filters[:limit] || 100
    users = User.select("users.*, COUNT(orders.id) as total_orders, orders.*")
    users = users.joins(:orders).group("orders.user_id")
    users = users.where("email LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR address LIKE ?", "%#{filters[:search]}%", "%#{filters[:search]}%", "%#{filters[:search]}%", "%#{filters[:search]}%") if filters[:search].present?

    if filters[:order_type]
      order = filters[:order] || 'ASC'
      type  = filters[:order_type] == 'total_orders' ? 'total_orders' : "users.#{filters[:order_type]}"
      users = users.order("#{type} #{order}")
    else
      users = users.order("users.id DESC")
    end

    users.page(filters[:page]).per(limit)
  end

  # get users who only ordered once and the order
  # was done from the past (time_ago)
  # the result will be put in a file
  def self.get_user_orders_by_time(time_ago)
    File.open("#{Rails.root}/tmp/user.orders.out", "a") do |f|
      f.puts "####################################"
      f.puts "#{Time.now}"

      user_emails = []
      User.all.each do |u|
        if u.orders.length == 1
          # if u.orders.first.created_at <= 10.months.ago
          if u.orders.first.created_at <= time_ago.end_of_day
            user_emails << u.email
            f.puts "#{u.email}, #{u.orders.first.order_id}, #{u.orders.first.created_at.to_date}"
          end
        end
      end

      f.puts "####################################"
    end
  end

  private

  def humanize_names
    self.first_name = self.first_name.humanize
    self.last_name  = self.last_name.humanize
  end
end
