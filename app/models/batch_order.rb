class BatchOrder < ActiveRecord::Base
  attr_accessible :active, :batch_key, :status
  has_many :orders
end
