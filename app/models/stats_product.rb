class StatsProduct < ActiveRecord::Base
	serialize :sizes_sold, :categories_sold

  attr_accessible :categories_sold, :orders, :product_id, :revenue, :seller_id, :sizes_sold
end
