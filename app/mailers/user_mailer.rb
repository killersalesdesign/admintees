include ActionView::Helpers::NumberHelper

class UserMailer < ActionMailer::Base
  default from: "support@jobtees.com.au"

  def send_on_order(id, password)
    @password = password
    @order   = Order.find(id)
    @product = @order.product
    from     = @order.website.id == 1 ? "support@fantees.com.au" : "support@jobtees.com.au"
    to       = @order.user.email

    mail(:to => to, from: from, :subject => "[#{@order.website.name}] Order ID #{@order.order_id}")
  end

  def send_on_print(id)
    @order = Order.find(id)
    to     = @order.user.email
    from   = @order.website.id == 1 ? "support@fantees.com.au" : "support@jobtees.com.au"
    puts @order.website.name

    mail(:to => to, from: from, :subject => "[#{@order.website.name}] Order ID #{@order.order_id} is now printing")
  end

  def send_on_shipping(id)
    @order = Order.find(id)
    to     = @order.user.email
    from   = @order.website.id == 1 ? "support@fantees.com.au" : "support@jobtees.com.au"

    mail(:to => to, from: from, :subject => "[#{@order.website.name}] Order ID #{@order.order_id} is now being shipped")
  end

  def send_on_manual_user(email, password)
    @email = email
    @password = password
    mail(:to => @email, :from => 'support@fantees.com.au', :subject => "User Account Created (Fantees & Jobtees)")
  end

  def product_email_packing(user, from, subject)
    @user = user
    mail(to: @user.email, from: from, subject: subject)
  end

  def product_email_printing(user, from, subject)
    @user = user
    mail(to: @user.email, from: from, subject: subject)
  end

  def product_email_tracking(user, from, subject)
    @user = user
    mail(to: @user.email, from: from, subject: subject)
  end

  def product_email_arrived(user, from, subject, product_title)
    @user = user
    @product_title = product_title
    mail(to: @user.email, from: from, subject: subject)
  end

  def product_email_campaign_ended(user, from, subject, product_title)
    @user = user
    @product_title = product_title
    mail(to: @user.email, from: from, subject: subject)
  end

  def product_email_how_we_do(user, from, subject, product_title)
    @user = user
    @product_title = product_title
    mail(to: @user.email, from: from, subject: subject)
  end

  def product_email_new_design(user, from, subject, product_title)
    @user = user
    @product_title = product_title
    mail(to: @user.email, from: from, subject: subject)
  end

  def batch_email_to_trello(batch_id)
    @batch = Batch.find(batch_id)
    if @batch
      puts "will now send email for batch creation..."
      receiver  = "rickgibson2+1vfpcq2exl74yuhnuwhi@boards.trello.com"
      # receiver  = "eralphamodia@gmail.com"
      from      = "support@fantees.com.au"
      subject   = "Batch Created - #{@batch.batch_key}"
      mail(to: receiver, from: from, subject: subject)
    end
  end

end
