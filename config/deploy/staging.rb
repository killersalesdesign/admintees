##################################
##### SET THESE VARIABLES ########
##################################
ssh_options[:keys] = ["~/.ssh/teecommerce.pem"]
server "54.85.248.184", :web, :app, :db, primary: true
set :ngnix_conf_file_loc, "staging/nginx.conf" # location of nginx conf file
set :unicorn_init_file_loc, "staging/unicorn_init.sh" # location of unicor init shell file
set :github_account_name, "killersalesdesign" # name of accout on git hub
set :github_repo_name, "admintees" # name of git hub repo
set :git_branch_name, "staging" # name of branch to deploy
set :rails_env, "staging" # name of environment: production, staging, ...
##################################