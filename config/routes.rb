AdminTees::Application.routes.draw do
  root :to => 'home#index'

  require 'sidekiq/web'
  require 'sidetiq/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == ENV['SIDEKIQ_USERNAME'] && password == ENV['SIDEKIQ_PASSWORD']
  end
  mount Sidekiq::Web => '/sidekiq'
  mount RailsAdmin::Engine => '/super_admin', :as => 'rails_admin'

  resources :sendgrid do
    collection do
      get :all, :action => :all
    end
  end

  devise_for :admins, :skip => [:registrations]
  get "admin", :controller => 'admin/home', :action => :index

  namespace :api do
    resources :orders, :only => [:index] do
      collection do
        get :reports
        get :sales
      end
    end

    namespace :v1 do
      resources :orders,   :only => [:index, :show]
      resources :products, :only => [:index, :show]
    end
  end

  namespace :admin do
    namespace :v2 do
      resources :orders, :only => [:index] do
        collection do
          get :total_orders
        end
      end
    end

    resources :batch_orders, :only => [:index, :create, :update, :show] do
      collection do
        get :show_by_order
      end
    end
    resources :website , :only => [:index]
    resources :emails , :only => [:index]
    resources :myloaders, :only => [:index]
    resources :coupons
    resources :giftcards
    resources :batches
    resources :notes
    resources :category
    resources :brand
    resources :item_color
    resources :searches
    resources :ordered_products
    resources :extra_orders
    resources :customers do
      collection do
        get :total_users
      end
    end
    resources :ratings
    resources :designs
    resources :abandonment do
      collection do
        get :stats, :action => :stats
      end
    end

    resources :mail_tracker do
      collection do
        get :stats, :action => :stats
      end
    end

    resources :pdfexports do
      collection do
        get :singlereport, :action => :singlereport
        get :batches, :action => :batches
      end
    end

    resources :csvimports do
      collection do
        get :shipment, :action => :shipment
        post :import, :action => :import
        get 'v2/shipment', :action => :shipment2
        post 'v2/import', :action => :import2
      end
    end
    resources :invoices do
      collection do
        get :in_stocks, :action => :in_stocks
        get :by_sizes, :action => :by_sizes
        get :by_category, :action => :by_category
        get :by_category_size, :action => :by_category_size
        get :by_all, :action => :by_all
        get :by_single, :action => :by_single
        get :by_multiple, :action => :by_multiple
        get "batch/:batch_id", :action => :by_batch
      end
    end

    resources :eparcel do
      collection do
      end
    end

    resources :notifications do
      collection do
        get :clear
      end
    end

    resources :products do
      collection do
        post :product_image
        get  :all_products
        get  :products_info
        get  :autocomplete
        get  :show_sold_stats
        get  :download_csv
      end
      member do
        put    :update_single_report_extra
        delete :delete_product_image
      end
    end

    resources :orders do
      collection do
        get :in_stocks, :action => :in_stocks
        get :in_eparcel, :action => :in_eparcel
        get :get_order_ids, :action => :get_orders_by_order_id
        get :get_order_by_batch, :action => :get_orders_by_order_id
      end
    end

    resources :statistics do
      collection do
        get :daily_sales, :action => :daily_sales
        get :daily_product_types, :action => :daily_product_types
        get :daily_product_sizes, :action => :daily_product_sizes
        get :tees_counts, :action => :tees_counts
        get :all_products
        get :all_products_list
      end
      member do
        get :sales_today
        get :sales_yesterday
        get :sales_all_time
        get :views_over_time
        get :sizes
        get :ordered
        get :analytics
        get :campaigns
        get :views
        get :hourly_orders
        get :product
      end
    end

  end

  # devise_for :users
  # get "dashboard", :controller => 'dashboard/home', :action => :index
  # namespace :dashboard do
  #   resources :users, :only => [:show, :update]
  #   resources :orders, :only => [:index, :show, :update] do
  #     member do
  #       get :ordered_products
  #     end
  #   end
  # end

  # resources :products, :only => [:index, :show] do
  #   collection do
  #     get  :checkout
  #     post :set_checkout_session
  #     post :paypal_checkout
  #     post :eway_checkout
  #     # get :success, :action => :return_url
  #   end
  #   member do
  #     get :success, :action => :return_url
  #   end
  # end
  # get "products/:id/:url", :controller => "products", :action => :show

  # resources :categories, :only => [:index, :show]
  # resources :orders, :only => [:show] do
  #   member do
  #     get :success, :action => :show
  #   end
  # end

  # post "pages/send_contact_us"
  # get "contact-us",       :controller => :pages, :action => :contact_us
  # get "about-us",         :controller => :pages, :action => :about_us
  # get "faq",              :controller => :pages, :action => :faq
  # get "privacy-policy",   :controller => :pages, :action => :privacy
  # get "terms-of-service", :controller => :pages, :action => :terms

  get "/test/:id", :controller => "home", :action => :test
  get "/barcode/:id", :controller => "home", :action => :barcode, via: [:get], as: :barcode
  get "/eparcel", :controller => "home", :action => :eparcel, via: [:get]
  get "/analytics", :controller => "admin/statistics", :action => 'analytics'
  get "/sales-overview", :controller => "admin/category", :action => "sold_per_category", :as => "sold_per_category"

end
