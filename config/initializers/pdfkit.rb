PDFKit.configure do |config|
  config.wkhtmltopdf = `which wkhtmltopdf`.to_s.strip
  config.default_options = {
    :encoding=>"UTF-8",
    :page_size=>"A4",
    :margin_top=>"0.25in",
    :margin_right=>"1in",
    :margin_bottom=>"0.1in",
    :margin_left=>"1in"

    }
  if Rails.env == 'development'
    config.root_url = "http://localhost:3000"
  end
  config.verbose = false
end