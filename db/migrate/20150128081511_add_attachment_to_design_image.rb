class AddAttachmentToDesignImage < ActiveRecord::Migration
  def self.up
    add_attachment :design_images, :image
  end

  def self.down
    remove_attachment :design_images, :image
  end
end
