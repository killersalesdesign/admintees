class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.references :product

      t.timestamps
    end
    add_index :brands, :product_id
  end
end
