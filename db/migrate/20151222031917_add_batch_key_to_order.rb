class AddBatchKeyToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :batch_key, :string
    add_index :orders, :batch_key
  end
end
