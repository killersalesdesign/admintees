class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.text :url
      t.text :description
      t.float :price
      t.references :category
      t.integer :sales
      t.boolean :active, :default => true
      t.datetime :date

      t.timestamps
    end
    add_index :products, :category_id
  end
end
