class AddFreeShippingToProduct < ActiveRecord::Migration
  def change
    add_column :products, :free_shipping, :integer, :default => 0
  end
end
