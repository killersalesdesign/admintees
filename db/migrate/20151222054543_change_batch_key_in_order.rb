class ChangeBatchKeyInOrder < ActiveRecord::Migration
  def up
    remove_index :orders, :batch_key
    remove_column :orders, :batch_key

    add_column :orders, :batch_order_id, :integer
    add_index :orders, :batch_order_id
  end

  def down
    add_column :orders, :batch_key, :string
    add_index :orders, :batch_key

    remove_index :orders, :batch_order_id
    remove_column :orders, :batch_order_id
  end
end
