class AddGiftCardsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :giftcard_id, :integer
    add_index :orders, :giftcard_id
    add_index :giftcards, :key_code, :unique => true
  end
end
