class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :name
      t.string :type
      t.string :filename
      t.string :status

      t.timestamps
    end
  end
end
