class CreateUnfinishedOrders < ActiveRecord::Migration
  def change
    create_table :unfinished_orders do |t|
      t.integer :product_id
      t.integer :qty
      t.integer :size
      t.integer :category
      t.string :style
      t.float :price
      t.string :tracking_id

      t.timestamps
    end
  end
end
