class AddAttachmentToCategoryImage < ActiveRecord::Migration
  def self.up
    add_attachment :category_images, :image
  end

  def self.down
    remove_attachment :category_images, :image
  end
end
