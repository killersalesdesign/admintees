class ChangeDatatypeInEmails < ActiveRecord::Migration
  def up
    change_column :emails, :subject, :text
  end

  def down
  end
end
