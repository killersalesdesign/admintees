class AddProductIdToOrderedProduct < ActiveRecord::Migration
  def change
    add_column :ordered_products, :product_id, :integer
    add_index :ordered_products, :product_id
  end
end
