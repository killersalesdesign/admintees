class AddActiveInGiftcards < ActiveRecord::Migration
  def up
    add_column :giftcards, :active, :boolean, :default => false
    add_column :giftcards, :breakdown, :string
  end


end
