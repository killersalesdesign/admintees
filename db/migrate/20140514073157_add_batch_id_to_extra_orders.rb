class AddBatchIdToExtraOrders < ActiveRecord::Migration
  def change
    add_column :extra_orders, :batch_id, :integer, :default => 0
  end
end
