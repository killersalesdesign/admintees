class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :user_id
      t.integer :website_id
      t.text :description
      t.string :to
      t.timestamps
    end
    add_index :notes, :user_id
    add_index :notes, :website_id
  end
end
