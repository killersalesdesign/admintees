class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer     :admin_id
      t.string      :message
      t.integer     :ref_id
      t.string      :ref_type
      t.boolean     :seen
      t.timestamps
    end
  end
end
