class AddDtglinkToProduct < ActiveRecord::Migration
  def change
    add_column :products, :dtg_link, :string, default: ""
  end
end
