class CreateItemColors < ActiveRecord::Migration
  def change
    create_table :item_colors do |t|
      t.string :name
      t.boolean :active

      t.timestamps
    end
  end
end
