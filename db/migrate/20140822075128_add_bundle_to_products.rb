class AddBundleToProducts < ActiveRecord::Migration
  def change
    add_column :products, :bundle, :boolean, :default => false
  end
end
