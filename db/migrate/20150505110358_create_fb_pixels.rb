class CreateFbPixels < ActiveRecord::Migration
  def change
    create_table :fb_pixels do |t|
      t.references :product
      t.string :landing_id
      t.string :thank_you_id

      t.timestamps
    end
    add_index :fb_pixels, :product_id
  end
end
