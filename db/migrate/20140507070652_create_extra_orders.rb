class CreateExtraOrders < ActiveRecord::Migration
  def change
    create_table :extra_orders do |t|
      t.references :product
      t.string :design

      t.timestamps
    end
    add_index :extra_orders, :product_id
  end
end
