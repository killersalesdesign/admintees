class RemoveProductIdFromBrand < ActiveRecord::Migration
  def up
  	remove_column :brands, :product_id
  end

  def down
  	add_column :brands, :product_id, :integer
  end
end
