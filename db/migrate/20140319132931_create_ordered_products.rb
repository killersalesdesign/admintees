class CreateOrderedProducts < ActiveRecord::Migration
  def change
    create_table :ordered_products do |t|
      t.references :category
      t.references :order
      t.integer :quantity
      t.string :size

      t.timestamps
    end
    add_index :ordered_products, :category_id
    add_index :ordered_products, :order_id
  end
end
