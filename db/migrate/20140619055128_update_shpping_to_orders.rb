class UpdateShppingToOrders < ActiveRecord::Migration
  def up
    change_column :orders, :shipped, :integer
  end

  def down
  end
end
