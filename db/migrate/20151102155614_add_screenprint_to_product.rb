class AddScreenprintToProduct < ActiveRecord::Migration
  def change
    add_column :products, :screenprint_link, :string, default: ""
  end
end
