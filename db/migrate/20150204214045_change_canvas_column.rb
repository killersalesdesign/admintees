class ChangeCanvasColumn < ActiveRecord::Migration
  def up
    change_column :product_designs, :canvas, :binary, :limit => 10.megabyte
  end

  def down
    change_column :product_designs, :canvas, :string
  end
end
