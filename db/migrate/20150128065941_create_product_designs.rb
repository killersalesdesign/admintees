class CreateProductDesigns < ActiveRecord::Migration
  def change
    create_table :product_designs do |t|
      t.references  :product
      t.string      :canvas
      t.timestamps
    end
  end
end
