class AddAttachmentToOrder < ActiveRecord::Migration
  def up
    add_attachment :orders, :barcode
  end

  def down
    remove_attachment :orders, :barcode
  end
end
