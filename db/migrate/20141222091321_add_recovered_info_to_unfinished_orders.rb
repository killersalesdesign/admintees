class AddRecoveredInfoToUnfinishedOrders < ActiveRecord::Migration
  def change
    add_column :unfinished_orders, :recovered, :boolean
    add_column :unfinished_orders, :recovered_date, :datetime
  end
end
