class AddNoteTypeToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :note_type, :integer, default: 0
  end
end
