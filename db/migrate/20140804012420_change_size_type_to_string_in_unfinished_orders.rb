class ChangeSizeTypeToStringInUnfinishedOrders < ActiveRecord::Migration
  def up
    change_column :unfinished_orders, :size, :string
  end

  def down
    change_column :unfinished_orders, :size, :integer
  end
end
