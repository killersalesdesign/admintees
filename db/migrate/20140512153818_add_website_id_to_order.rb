class AddWebsiteIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :website_id, :integer, :default => 1

    add_index :orders, :website_id
  end
end
