class AddItemTotalToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :order_total, :float
    add_column :orders, :shipping_fee, :float
  end
end
