class AddColorToProductImage < ActiveRecord::Migration
  def change
    add_column :product_images, :color, :string
  end
end
