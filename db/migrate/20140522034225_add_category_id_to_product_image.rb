class AddCategoryIdToProductImage < ActiveRecord::Migration
  def change
    add_column :product_images, :category_id, :integer

    add_index :product_images, :category_id
  end
end
