class CreateGoogleAnalytics < ActiveRecord::Migration
  def change
    create_table :google_analytics do |t|
      t.string :landing_id
      t.string :thank_you_id
      t.references :product

      t.timestamps
    end
    add_index :google_analytics, :product_id
  end
end
