class AddAdsSpendToProducts < ActiveRecord::Migration
  def change
    add_column :products, :ads_spend, :float, :default => 0
  end
end
