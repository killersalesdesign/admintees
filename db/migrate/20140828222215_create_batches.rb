class CreateBatches < ActiveRecord::Migration
  def change
    create_table :batches do |t|
      t.integer :batch_key
      t.integer :product_id
      t.text :batch_stats
      t.string :brand
      t.string :colour
      t.string :product_name
      t.timestamps
    end
    add_index :batches, :product_id
  end
end
