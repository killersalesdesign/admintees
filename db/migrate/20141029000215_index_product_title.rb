class IndexProductTitle < ActiveRecord::Migration
  def up
  	add_index :products, :title
  end

  def down
  	remove_index :products, :title
  end
end
