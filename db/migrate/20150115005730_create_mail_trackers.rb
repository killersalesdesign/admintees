class CreateMailTrackers < ActiveRecord::Migration
  def change
    create_table :mail_trackers do |t|
      t.string :action_type
      t.integer :action_count

      t.timestamps
    end
  end
end
