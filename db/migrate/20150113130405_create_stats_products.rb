class CreateStatsProducts < ActiveRecord::Migration
  def change
    create_table :stats_products do |t|
      t.integer :product_id
      t.integer :orders
      t.float :revenue
      t.text :sizes_sold
      t.text :categories_sold
      t.integer :seller_id

      t.timestamps
    end
  end
end
