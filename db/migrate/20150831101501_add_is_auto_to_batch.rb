class AddIsAutoToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :is_auto, :boolean, default: false
  end
end
