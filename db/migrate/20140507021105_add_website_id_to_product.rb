class AddWebsiteIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :website_id, :integer, :default => 1
    add_index :products, :website_id
  end
end
