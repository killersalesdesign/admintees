class AddPrintColorsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :print_colors, :integer
  end
end
