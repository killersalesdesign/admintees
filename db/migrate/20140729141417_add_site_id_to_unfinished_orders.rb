class AddSiteIdToUnfinishedOrders < ActiveRecord::Migration
  def change
    add_column :unfinished_orders, :website_id, :integer
  end
end
