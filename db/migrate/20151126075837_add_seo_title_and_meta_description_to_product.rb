class AddSeoTitleAndMetaDescriptionToProduct < ActiveRecord::Migration
  def change
    add_column :products, :seo_title, :text
    add_column :products, :meta_description, :text
  end
end
