class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :percent
      t.references :website

      t.timestamps
    end
    add_index :ratings, :website_id
  end
end
