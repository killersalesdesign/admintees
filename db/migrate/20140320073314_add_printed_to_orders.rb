class AddPrintedToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :printed, :boolean, :default => false
    add_column :orders, :shipped, :boolean, :default => false
  end
end
