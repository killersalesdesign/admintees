class AddAdditionalItemCostToProduct < ActiveRecord::Migration
  def change
    add_column :products, :additional_item_cost, :float
  end
end
