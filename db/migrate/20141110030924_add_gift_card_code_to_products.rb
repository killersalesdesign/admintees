class AddGiftCardCodeToProducts < ActiveRecord::Migration
  def change
    add_column :products, :gift_card_code, :string
  end
end
