class CreateGiftcards < ActiveRecord::Migration
  def change
    create_table :giftcards do |t|
      t.string :key_code
      t.integer :order_id
      t.boolean :is_used, :default => false
      t.timestamps
    end
    add_index :giftcards, :order_id
  end
end
