class AddEmailToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :email, :string, :default => ''
  end
end
