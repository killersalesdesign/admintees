class AddCutOffToProduct < ActiveRecord::Migration
  def change
    add_column :products, :international_cutoff, :integer
  end
end
