class RenameTypeToEmailTypeInEmails < ActiveRecord::Migration
  def up
    rename_column :emails, :type, :email_type
  end

  def down
  end
end
