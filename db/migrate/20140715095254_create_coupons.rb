class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :code
      t.integer :limit
      t.integer :usage, :default => 0
      t.datetime :expiration
      t.boolean :active, :default => true

      t.timestamps
    end
    add_index :coupons, :code, :unique => true
  end
end
