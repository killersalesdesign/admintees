class CreateCategoryImages < ActiveRecord::Migration
  def change
    create_table :category_images do |t|
      t.references :category
      t.string :color
      t.string :label

      t.timestamps
    end
    add_index :category_images, :category_id
  end
end
