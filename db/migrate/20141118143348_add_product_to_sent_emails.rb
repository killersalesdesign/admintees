class AddProductToSentEmails < ActiveRecord::Migration
  def change
    add_column :sent_emails, :product_id, :integer
  end
end
