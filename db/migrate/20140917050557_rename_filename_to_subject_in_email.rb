class RenameFilenameToSubjectInEmail < ActiveRecord::Migration
  def up
    rename_column :emails, :filename, :subject
  end

  def down
  end
end
