class AddAttachmentToProductImage < ActiveRecord::Migration
   def self.up
    add_attachment :product_images, :product_image
  end

  def self.down
    remove_attachment :product_images, :product_image
  end
end
