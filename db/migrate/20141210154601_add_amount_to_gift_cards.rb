class AddAmountToGiftCards < ActiveRecord::Migration
  def change
    add_column :giftcards, :amount, :float
    add_column :giftcards, :product_id, :integer

  end
end
