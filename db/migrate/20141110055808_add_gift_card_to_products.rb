class AddGiftCardToProducts < ActiveRecord::Migration
  def change
    add_column :products, :gift_card, :boolean, :default => false
  end
end
