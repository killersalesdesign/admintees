class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.string :size
      t.integer :amount
      t.references :product
      t.references :category

      t.timestamps
    end
    add_index :stocks, :product_id
    add_index :stocks, :category_id
  end
end
