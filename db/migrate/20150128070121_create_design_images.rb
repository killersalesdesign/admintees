class CreateDesignImages < ActiveRecord::Migration
  def change
    create_table :design_images do |t|
      t.references :product_design
      t.timestamps
    end
  end
end
