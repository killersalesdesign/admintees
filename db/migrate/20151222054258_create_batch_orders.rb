class CreateBatchOrders < ActiveRecord::Migration
  def change
    create_table :batch_orders do |t|
      t.string :batch_key
      t.integer :status, :default => 0
      t.boolean :active, :default => true

      t.timestamps
    end

    add_index :batch_orders, :batch_key, :unique => true
  end
end
