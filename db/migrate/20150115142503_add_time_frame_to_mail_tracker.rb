class AddTimeFrameToMailTracker < ActiveRecord::Migration
  def change
    add_column :mail_trackers, :time_frame, :string
  end
end
