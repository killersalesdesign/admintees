class AddTrackingKeyToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :tracking_key, :string
  end
end
