Website.delete_all
ActiveRecord::Base.connection.execute("ALTER TABLE websites AUTO_INCREMENT = 1")
Website.create(:url => "https://fantees.com.au", :name => "FanTees")
Website.create(:url => "https://jobtees.com.au", :name => "JobTees")
Website.create(:url => "http://fantees.com.nz", :name => "FanTees-NZ")
Website.create(:url => "http://54.86.46.181", :name => "FanTees-BR")

Rating.delete_all
ActiveRecord::Base.connection.execute("ALTER TABLE ratings AUTO_INCREMENT = 1")
Rating.create(:percent => 98, :website_id => 1)
Rating.create(:percent => 98, :website_id => 2)
Rating.create(:percent => 98, :website_id => 3)
Rating.create(:percent => 98, :website_id => 4)

Email.delete_all
ActiveRecord::Base.connection.execute("ALTER TABLE emails AUTO_INCREMENT = 1")
Email.create(:name => "Product Campaign Ended", :email_type => "campaign_ended", :subject => "[FanTees] Annnnnd we're off to print :)", :status => "active")
Email.create(:name => "Product Printing", :email_type => "printing", :subject => "[FanTees] The Printer Is Being Warmed Up!", :status => "active")
Email.create(:name => "Product Packing", :email_type => "packing", :subject => "[FanTees] The Van Is Coming!", :status => "active")
Email.create(:name => "Product Tracking", :email_type => "tracking", :subject => "[FanTees] Your Postal Tracking ID", :status => "active")
Email.create(:name => "Product Arrived", :email_type => "arrived", :subject => "[FanTees] Have You Got It?", :status => "active")