namespace :myloaders do
  desc "TODO"
  task :quantity => :environment do
    allOrders =  Order.order(:id => "DESC")
    allOrders.each do |order|
       qty = order.ordered_products.sum(:quantity)
       order.quantity = qty
       order.save
    end
  end

  desc "delete ordered_products with no parent order"
  task :task_name => [:dependent, :tasks] do
    orderedProducts  = OrderedProduct.group("order_id")
    orderedProducts.each do |op|
      order = Order.find(op.order_id)
      # if order.nil?
      #   del_ordered_product = OrderedProduct.where(:order_id => op.order_id)
      #   del_ordered_product.each do |d|
      #     puts "deleted", d.id
      #   end
      #   del_ordered_product.delete_all
      # end
    end
  end

end
