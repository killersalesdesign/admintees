namespace :abandons do
  desc "Set recovered field values"
  task :set_recovered => :environment do
  	orders = UnfinishedOrder.all()
  	orders.each do |order|
  		unless order.recovered == true
  			puts "updating order: #{order.tracking_id}"
  			order.recovered = false
  			order.save!
  		end
  	end
  end
end
