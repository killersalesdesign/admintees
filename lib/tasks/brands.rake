namespace :seed do

	desc "seed for brands"
	task :brands => :environment do
		puts 'creating brands...'
		Brand.create([
			{:name => "AS Colour Stencil Hood", :active => "true"},
			{:name => "AS Colour Chalk Hood", :active => "true"},
			{:name => "Anvil Lightweight Tee", :active => "true"},
			{:name => "Anvil Midweight Tee", :active => "true"}
		])
		puts 'brands created'
	end

	desc "seed for item colors"
	task :item_colors => :environment do
		puts 'creating item colors...'
		ItemColor.create([
			{:name => "Black", :active => "true"},
			{:name => "White", :active => "true"}
		])
		puts 'item colors created'
	end
end