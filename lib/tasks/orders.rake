namespace :orders do
  desc "Pick a random user as the winner"
  task :update_ordered_product => :environment do
    orders = Order.all
    orders.each do |o|
      o.ordered_products.each do |op|
        op.product_id = o.product_id
        if op.save
          puts "Order Product: #{op.inspect}"
        end
      end
    end
  end
end